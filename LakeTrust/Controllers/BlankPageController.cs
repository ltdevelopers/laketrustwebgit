﻿using LakeTrust.Models.Pages;
using LakeTrust.Models.ViewModels;
using System;
using System.Linq;
using System.Web.Mvc;

namespace LakeTrust.Controllers
{
    public class BlankPageController : PageControllerBase<BlankPage>
    {
        public ActionResult Index(BlankPage currentPage)
        {
            var model = PageViewModel.Create(currentPage);

            return View(model);
        }
    }
}