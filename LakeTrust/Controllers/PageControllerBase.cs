﻿using EPiServer;
using EPiServer.Globalization;
using EPiServer.Web;
using EPiServer.Web.Mvc;
using EPiServer.Web.Mvc.XForms;
using EPiServer.XForms.Util;
using LakeTrust.Business;
using LakeTrust.Models.Pages;
using LakeTrust.Models.ViewModels;
using System;
using System.Net.Mail;
using System.Text;
using System.Web.Mvc;
using System.Web.Security;

namespace LakeTrust.Controllers
{
    /// <summary>
    /// All controllers that renders pages should inherit from this class so that we can apply
    /// action filters, such as for output caching site wide, should we want to.
    /// </summary>
    public abstract class PageControllerBase<T> : PageController<T>, IModifyLayout
        where T : LakeTrustBaseModel
    {
        private readonly XFormPageUnknownActionHandler xformHandler;

        private string contentId;

        public PageControllerBase()
        {
            this.xformHandler = new XFormPageUnknownActionHandler();
            this.contentId = string.Empty;
        }

        /// <summary>
        /// Signs out the current user and redirects to the Index action of the same controller.
        /// </summary>
        /// <remarks>
        /// There's a log out link in the footer which should redirect the user to the same page. As
        /// we don't have a specific user/account/login controller but rely on the login URL for
        /// forms authentication for login functionality we add an action for logging out to all
        /// controllers inheriting from this class.
        /// </remarks>
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index");
        }

        public virtual void ModifyLayout(LayoutModel layoutModel)
        {
            var page = PageContext.Page as SitePageData;
            if (page != null)
            {
                layoutModel.HideHeader = page.HideSiteHeader;
                layoutModel.HideFooter = page.HideSiteFooter;
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public virtual ActionResult Success(XFormPostedData xFormPostedData)
        {
            if (xFormPostedData.XForm.PageGuidAfterPost != Guid.Empty)
            {
                PermanentContentLinkMap pageMap = PermanentLinkMapStore.Find(xFormPostedData.XForm.PageGuidAfterPost) as PermanentContentLinkMap;
                if (pageMap != null)
                {
                    string internalUrl = pageMap.MappedUrl.ToString();
                    internalUrl = UriSupport.AddLanguageSelection(internalUrl, ContentLanguage.PreferredCulture.Name);
                    UrlBuilder urlBuilder = new UrlBuilder(internalUrl);
                    Global.UrlRewriteProvider.ConvertToExternal(urlBuilder, null, Encoding.UTF8);
                    Response.Redirect(urlBuilder.ToString(), true);
                }
            }

            return RedirectToAction("Index");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public virtual ActionResult Failed(XFormPostedData xFormPostedData)
        {
            return RedirectToAction("Index");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public virtual ActionResult XFormPost(XFormPostedData xFormPostedData)
        {
            if (Request.Form["email"] != null)
            {
                if (IsValid(this.Request.Form["email"]))
                {
                    if (xFormPostedData != null)
                    {
                        if (this.Request.Params["XFormId"] != null &&
                        !string.IsNullOrEmpty(this.Request.Params["XFormId"]))
                        {
                            var formData = new XFormPageHelper().GetXFormData(this, xFormPostedData);
                            formData.MailFrom = this.Request.Form["email"];

                            try
                            {
                                if (XFormActionHelper.DoAction(formData, xFormPostedData, true))
                                {
                                    return Success(xFormPostedData);
                                }
                                else
                                {
                                    return Failed(xFormPostedData);
                                }
                            }
                            catch (Exception ex)
                            {
                                this.ModelState.AddModelError("ProcessXFormAction", ex.Message);
                            }
                        }
                    }
                }
            }

            return this.xformHandler.HandleAction(this);
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (!string.IsNullOrEmpty(this.contentId))
            {
                if (TempData[ViewDataKey] != null)
                {
                    ViewData = (ViewDataDictionary)TempData[ViewDataKey];
                }
            }

            base.OnActionExecuting(filterContext);
        }

        protected override void OnResultExecuting(ResultExecutingContext filterContext)
        {
            if (!string.IsNullOrEmpty(this.contentId))
            {
                TempData[ViewDataKey] = ViewData;
            }

            base.OnResultExecuting(filterContext);
        }

        public bool IsValid(string emailaddress)
        {
            try
            {
                MailAddress m = new MailAddress(emailaddress);
                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }

        private readonly string _viewDataKeyFormat = "ViewData_{0}";

        private string ViewDataKey
        {
            get
            {
                return string.Format(_viewDataKeyFormat, this.contentId);
            }
        }
    }
}