﻿using EPiServer.Web;
using EPiServer.Web.Mvc;
using LakeTrust.Models.Pages;
using LakeTrust.Models.ViewModels;
using System;
using System.Linq;
using System.Web.Mvc;

namespace LakeTrust.Controllers
{
    public class StartPageController : PageControllerBase<StartPage>
    {
        public ActionResult Index(StartPage currentPage)
        {
            var model = new StartPageViewModel(currentPage);

            if (SiteDefinition.Current.StartPage.CompareToIgnoreWorkID(currentPage.ContentLink)) // Check if it is the StartPage or just a page of the StartPage type.
            {
                //Connect the view models logotype property to the start page's to make it editable
                var editHints = ViewData.GetEditHints<StartPageViewModel, StartPage>();
                editHints.AddConnection(m => m.PageHeader.HeaderNavigation, p => p.HeaderNavigation);
                editHints.AddConnection(m => m.PageHeader.MyCreditCardLink, p => p.MyCreditCard);
                editHints.AddConnection(m => m.PageHeader.MyCreditCartText, p => p.MyCreditCardText);
                editHints.AddConnection(m => m.PageHeader.SearchPageContentLink, p => p.SearchResultsPage);

                editHints.AddConnection(m => m.PageFooter.FooterAddress, p => p.FooterAddress);
                editHints.AddConnection(m => m.PageFooter.FooterNavigation, p => p.FooterNavigation);
                editHints.AddConnection(m => m.PageFooter.FooterText, p => p.FooterText);
                editHints.AddConnection(m => m.PageFooter.SocialContentArea, p => p.SocialContentArea);
            }

            return View(model);
        }
    }
}