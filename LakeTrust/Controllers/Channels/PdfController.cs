﻿using EPiServer.Core;
using EPiServer.Framework.DataAnnotations;
using EPiServer.Web.Mvc;
using EPiServer.Web.Routing;
using Rotativa;
using Rotativa.Options;
using System;
using System.Linq;
using System.Web.Mvc;

namespace LakeTrust.Controllers
{
    [TemplateDescriptor(Inherited = true, Tags = new[] { "pdf" })]
    public class PdfController : PageController<PageData>
    {
        private readonly UrlResolver urlResolver;

        public PdfController(UrlResolver urlResolver)
        {
            this.urlResolver = urlResolver;
        }

        public ActionResult Index(PageData currentPage)
        {
            var url = this.urlResolver.GetUrl(currentPage.ContentLink);

            return new UrlAsPdf(url)
            {
                PageMargins = new Margins(3, 3, 3, 3),
                FormsAuthenticationCookieName = ".EPiServerLogin",
                CustomSwitches = "--print-media-type"
            };
        }
    }
}