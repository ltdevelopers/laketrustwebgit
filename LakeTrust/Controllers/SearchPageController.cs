﻿using EPiServer.Core;
using EPiServer.Framework.DataAnnotations;
using EPiServer.Web.Routing;
using LakeTrust.Business.Services;
using LakeTrust.Models.Pages;
using LakeTrust.Models.ViewModels;
using SolrNet;
using System;
using System.Linq;
using System.Web.Mvc;

namespace LakeTrust.Controllers
{
    [TemplateDescriptor(Default = true)]
    public partial class SearchPageController : PageControllerBase<SearchResult>
    {
        private readonly UrlResolver urlResolver;
        private readonly int PageSize = 10;
        private readonly SearchService serviceService;

        public SearchPageController(UrlResolver urlResolver, SearchService searchService)
        {
            this.urlResolver = urlResolver;
            this.serviceService = searchService;
        }

        public virtual ViewResult Index(SearchResult currentPage, int p = 1, string q = "")
        {
            var model = new ListingViewModel<SearchResult, SearchHit>(currentPage)
            {
                PageWeight = currentPage.NumberOfItemsToShow == 0 ? this.PageSize : currentPage.NumberOfItemsToShow,
                PageIndex = Request.QueryString["p"] != null ? Convert.ToInt32(Request.QueryString["p"]) : 1,
                TotalItems = 0,
                SearchTerm = string.Empty,
                Pages = Enumerable.Empty<SearchHit>()
            };

            if (!string.IsNullOrWhiteSpace(q))
            {
                var searchResult = this.serviceService.Search(q, new[] { ContentReference.StartPage, ContentReference.GlobalBlockFolder, ContentReference.SiteBlockFolder }, ControllerContext.HttpContext, currentPage.LanguageID);
                model.TotalItems = searchResult.TotalHits;
                model.Pages = searchResult.IndexResponseItems
                    .Skip((model.PageWeight * model.PageIndex) - model.PageWeight)
                    .Take(model.PageWeight)
                    .SelectMany(SearchHit.CreateHitModel);
                model.SearchTerm = q;
            }

            return View(model);
        }

        //[ValidateInput(false)]
        //public virtual ViewResult Paging(SearchResult currentPage, int pageIndex = 1, string searchTerm = "")
        //{
        //    var model = new ListingViewModel<SearchResult, SearchHit>(currentPage)
        //    {
        //        PageWeight = currentPage.NumberOfItemsToShow == 0 ? this.PageSize : currentPage.NumberOfItemsToShow,
        //        PageIndex = pageIndex
        //    };
        //    var searchResult = this.serviceService.Search(searchTerm, new[] { ContentReference.StartPage, ContentReference.GlobalBlockFolder, ContentReference.SiteBlockFolder }, ControllerContext.HttpContext, currentPage.LanguageID);
        //    model.TotalItems = searchResult.TotalHits;
        //    model.Pages = searchResult.IndexResponseItems
        //        .Skip((model.PageWeight * model.PageIndex) - model.PageWeight)
        //        .Take(model.PageWeight)
        //        .SelectMany(SearchHit.CreateHitModel);
        //    model.SearchTerm = searchTerm;

        //    return View("Index", model);
        //}
    }
}