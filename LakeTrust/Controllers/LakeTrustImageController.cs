﻿using LakeTrust.Business.Services;
using Simple.ImageResizer;
using Simple.ImageResizer.MvcExtensions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace LakeTrust.Controllers
{
    public class LakeTrustImageController : Controller
    {
        private readonly ImageServer imageService;

        public LakeTrustImageController(ImageServer imageService)
        {
            this.imageService = imageService;
        }

        [OutputCache(VaryByParam = "*", Duration = 900)]
        public ActionResult WebCamImage(int width = 555)
        {
            var info = this.imageService.MostRecentImage("TCC", "Lake-Trust", "Cam4");
            var webClient = new WebClient();
            byte[] buffer = webClient.DownloadData(info.ImageUrl);

            ViewBag.ImageInfo = info;

            ImageResizer resizer = new ImageResizer(buffer);
            var resizedBuffer = resizer.Resize(width, ImageEncoding.Jpg100);
            return File(resizedBuffer, "image/jpeg");
        }
    }
}