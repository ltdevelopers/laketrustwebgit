﻿using EPiServer;
using EPiServer.Core;
using EPiServer.Framework.DataAnnotations;
using EPiServer.Web;
using LakeTrust.Business.Services;
using LakeTrust.Models.Pages;
using LakeTrust.Models.ViewModels;
using System;
using System.Linq;
using System.Web.Mvc;

namespace LakeTrust.Controllers
{
    [TemplateDescriptor(Default = true)]
    public class BlogPageController : PageControllerBase<BlogPage>
    {
        private readonly IContentRepository contentRepository;
        private readonly IContentLoader contentLoader;
        private readonly BlogService blogService;
        private readonly TwitterFeedService twitterFeed;
        private readonly SiteDefinition siteDefinition;

        public BlogPageController(IContentLoader contentLoader, IContentRepository contentRepository, BlogService blogService, TwitterFeedService twitterFeed, SiteDefinition siteDefinition)
        {
            this.contentLoader = contentLoader;
            this.contentRepository = contentRepository;
            this.blogService = blogService;
            this.siteDefinition = siteDefinition;
            this.twitterFeed = twitterFeed;
        }

        public virtual ActionResult Index(BlogPage currentPage)
        {
            int totalItems = 0;
            var pages = this.blogService.GetBlogPosts(currentPage.ContentLink, out totalItems, "all", 1).Cast<BlogPostPage>();
            var model = new ListingViewModel<BlogPage, BlogPostPage>(currentPage)
            {
                TotalItems = totalItems,
                Pages = pages,
                PageIndex = 1,
                PageWeight = 10
            };

            return View(model);
        }

        public virtual ActionResult Paging(BlogPage currentPage, int pageIndex = 1, string searchTerm = "all")
        {
            int totalItems = 0;
            var pages = this.blogService.GetBlogPosts(currentPage.ContentLink, out totalItems, searchTerm, pageIndex).Cast<BlogPostPage>();

            var model = new ListingViewModel<BlogPage, BlogPostPage>(currentPage)
            {
                TotalItems = totalItems,
                Pages = pages,
                PageIndex = pageIndex,
                PageWeight = 10
            };

            return View("Index", model);
        }

        public virtual ActionResult PopularBlogPosts(LakeTrustBaseModel blogPage)
        {
            var page = this.contentLoader.Get<BlogPage>(blogPage.GetPropertyValue<PageReference>(Global.StringConstants.BlogStartReference));
            var posts = this.blogService.GetMostPopularBlogPost(page.ContentLink, blogPage.GetPropertyValue<int>("PopularPostCount", 3)).Cast<BlogPostPage>();
            return PartialView(posts);
        }

        public virtual ActionResult BlogCategories(LakeTrustBaseModel blogPage)
        {
            var page = this.contentLoader.Get<BlogPage>(blogPage.GetPropertyValue<PageReference>(Global.StringConstants.BlogStartReference));
            var categories = this.blogService.BlogCategories(page.ContentLink.ToPageReference());
            return PartialView(categories);
        }

        public virtual ActionResult TwitterFeed()
        {
            var start = this.contentLoader.Get<StartPage>(this.siteDefinition.StartPage);
            var tweets = this.twitterFeed.GetTweets(start.NumberOfTwitterItems, start.TwitterUserName);
            return PartialView(tweets);
        }
    }
}