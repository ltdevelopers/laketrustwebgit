﻿using EPiServer;
using EPiServer.Core;
using EPiServer.Framework.DataAnnotations;
using LakeTrust.Models.Pages;
using LakeTrust.Models.ViewModels;
using System.Web.Mvc;

namespace LakeTrust.Controllers
{
    [TemplateDescriptor(Default = true)]
    public class BlogPostPageController : PageControllerBase<BlogPostPage>
    {
        private readonly IContentLoader contentLoader;

        public BlogPostPageController(IContentLoader contentLoader)
        {
            this.contentLoader = contentLoader;
        }

        public virtual ActionResult Index(BlogPostPage currentPage)
        {
            var model = new PageViewModel<BlogPostPage>(currentPage);
            var blogRootPage = this.contentLoader.Get<BlogPage>(currentPage.GetPropertyValue<PageReference>(Global.StringConstants.BlogStartReference));
            if (!User.Identity.IsAuthenticated)
                LakeTrust.Business.Blog.BlogPostView.Increment(currentPage.ContentGuid, blogRootPage.ContentGuid);

            return View(model);
        }
    }
}