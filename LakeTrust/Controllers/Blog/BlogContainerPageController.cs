﻿using EPiServer;
using EPiServer.Framework.DataAnnotations;
using LakeTrust.Business.Services;
using LakeTrust.Models.Pages;
using LakeTrust.Models.ViewModels;
using System.Linq;
using System.Web.Mvc;

namespace LakeTrust.Controllers
{
    [TemplateDescriptor(Default = true)]
    public class BlogContainerPageController : PageControllerBase<BlogContainerPage>
    {
        private readonly IContentRepository contentRepository;
        private readonly IContentLoader contentLoader;
        private readonly BlogService blogService;

        public BlogContainerPageController(IContentLoader contentLoader, IContentRepository contentRepository, BlogService blogService)
        {
            this.contentLoader = contentLoader;
            this.contentRepository = contentRepository;
            this.blogService = blogService;
        }

        public virtual ActionResult Index(BlogContainerPage currentPage, int pageIndex = 1, string category = "all")
        {
            var model = new ListingViewModel<BlogContainerPage, BlogPostPage>(currentPage);
            int totalItems = 0;
            model.Pages = blogService.GetBlogPosts(currentPage.ContentLink, out totalItems, category, pageIndex).Cast<BlogPostPage>();

            model.TotalItems = totalItems;
            model.PageIndex = pageIndex;
            model.PageWeight = 10;
            return View(model);
        }

        public virtual ActionResult Paging(BlogContainerPage currentPage, int pageIndex = 1, string category = "all")
        {
            var model = new ListingViewModel<BlogContainerPage, BlogPostPage>(currentPage);
            int totalItems = 0;
            model.Pages = blogService.GetBlogPosts(currentPage.ContentLink, out totalItems, category, pageIndex).Cast<BlogPostPage>();

            model.TotalItems = totalItems;
            model.PageIndex = pageIndex;
            model.PageWeight = 10;
            return View("Index", model);
        }
    }
}