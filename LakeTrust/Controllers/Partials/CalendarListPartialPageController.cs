﻿using EPiServer;
using EPiServer.Core;
using EPiServer.Framework.DataAnnotations;
using EPiServer.Web.Mvc;
using LakeTrust.Business;
using LakeTrust.Models.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace LakeTrust.Controllers.Partials
{
    [TemplateDescriptor(AvailableWithoutTag = false, Tags = new[] { "LakeTrustU" }, ModelType = typeof(CalendarPage), Default = false, TemplateTypeCategory = EPiServer.Framework.Web.TemplateTypeCategories.MvcPartialController)]
    public class CalendarListPartialPageController : PageController<CalendarPage>
    {
        private readonly IContentRepository contentRepository;
        private readonly IContentLoader contentLoader;
        private readonly ContentLocator contentLocator;

        public CalendarListPartialPageController(IContentLoader contentLoader, IContentRepository contentRepository, ContentLocator contentLocator)
        {
            this.contentLoader = contentLoader;
            this.contentRepository = contentRepository;
            this.contentLocator = contentLocator;
        }

        public ActionResult Index(CalendarPage currentPage)
        {
            var filteredPages = GetEventItems(currentPage.ContentLink);

            return PartialView(filteredPages.Take(3));
        }

        private IEnumerable<CalendarEventPage> GetEventItems(ContentReference contentLink)
        {
            var list = new List<CalendarEventPage>();

            var eventItems = this.contentLocator.FindPagesByPageType<CalendarEventPage>(contentLink, true)
                .FilterForDisplay(true, true)
                .Cast<CalendarEventPage>();

            foreach (CalendarEventPage model in eventItems.OrderBy(x => x.EventDateTime))
            {
                if (model.HasEndDate)
                {
                    if (model.EventEndDateTime > DateTime.Now)
                    {
                        list.Add(model);
                        continue;
                    }
                }

                if (model.EventDateTime >= DateTime.Now)
                    list.Add(model);
            }

            return list;
        }
    }
}