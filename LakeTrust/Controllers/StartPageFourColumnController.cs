﻿using EPiServer;
using LakeTrust.Business;
using LakeTrust.Models.Pages;
using LakeTrust.Models.ViewModels;
using LinqToTwitter;
using System;
using System.Linq;
using System.Web.Mvc;

namespace LakeTrust.Controllers.Pages
{
    public class StartPageFourColumnController : PageControllerBase<LakeTrust.Models.Pages.StartPage>
    {
        private SingleUserAuthorizer auth = new SingleUserAuthorizer();
        private readonly IContentRepository contentRepository;
        private readonly IContentLoader contentLoader;
        private readonly ContentLocator contentLocator;

        public StartPageFourColumnController(IContentLoader contentLoader, IContentRepository contentRepository, ContentLocator contentLocator)
        {
            this.contentLoader = contentLoader;
            this.contentRepository = contentRepository;
            this.contentLocator = contentLocator;
        }

        public virtual ActionResult Index(StartPage currentPage)
        {
            var model = new StartPageViewModel(currentPage);

            return View(Global.Views.HomePage4Column, model);
        }
    }
}