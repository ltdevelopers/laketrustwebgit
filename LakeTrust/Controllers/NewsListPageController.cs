﻿using EPiServer;
using EPiServer.Core;
using EPiServer.Framework.DataAnnotations;
using LakeTrust.Business;
using LakeTrust.Models.Pages;
using LakeTrust.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace LakeTrust.Controllers
{
    [TemplateDescriptor(Default = true)]
    public class NewsListPageController : PageControllerBase<NewsPage>
    {
        private readonly IContentRepository contentRepository;
        private readonly IContentLoader contentLoader;
        private readonly ContentLocator contentLocator;

        public NewsListPageController(IContentLoader contentLoader, IContentRepository contentRepository, ContentLocator contentLocator)
        {
            this.contentLoader = contentLoader;
            this.contentRepository = contentRepository;
            this.contentLocator = contentLocator;
        }

        public virtual ActionResult Index(NewsPage currentPage)
        {
            var filteredPages = GetNewsItems(currentPage.ContentLink);

            var model = new ListingViewModel<NewsPage, NewsArticlePage>(currentPage)
            {
                PageIndex = 1,
                PageWeight = 10,
                TotalItems = filteredPages.Count(),
                Pages = filteredPages.Take(10)
            };

            return View(model);
        }

        public virtual ActionResult Paging(NewsPage currentPage, int pageIndex = 1)
        {
            var model = new ListingViewModel<NewsPage, NewsArticlePage>(currentPage);

            var filteredPages = GetNewsItems(currentPage.ContentLink);
            model.TotalItems = filteredPages.Count();
            model.PageIndex = pageIndex;
            model.PageWeight = 10;
            model.Pages = filteredPages.Skip((pageIndex - 1) * model.PageWeight).Take(model.PageWeight);

            return View("Index", model);
        }

        private IEnumerable<NewsArticlePage> GetNewsItems(ContentReference contentLink)
        {
            return this.contentLocator.FindPagesByPageType<NewsArticlePage>(contentLink, true)
                .FilterForDisplay(true, true)
                .OrderByDescending(x => x.StartPublish)
                .Cast<NewsArticlePage>();
        }
    }
}