﻿using EPiServer;
using EPiServer.Globalization;
using EPiServer.ServiceLocation;
using EPiServer.Web;
using EPiServer.Web.Mvc;
using EPiServer.Web.Mvc.XForms;
using EPiServer.Web.Routing;
using EPiServer.XForms.Util;
using LakeTrust.Models.Pages.HQ;
using System;
using System.Net.Mail;
using System.Text;
using System.Web.Mvc;

namespace LakeTrust.Controllers.HQ
{
    /// <summary>
    /// All controllers that renders pages should inherit from this class so that we can apply
    /// action filters, such as for output caching site wide, should we want to.
    /// </summary>
    public abstract class HQPageControllerBase<T> : PageController<T>
        where T : HQBasePage
    {
        private readonly XFormPageUnknownActionHandler xformHandler;
        private readonly IContentLoader contentLoader;
        private string contentId;

        public HQPageControllerBase()
        {
            this.xformHandler = new XFormPageUnknownActionHandler();
            this.contentId = string.Empty;
            this.contentLoader = ServiceLocator.Current.GetInstance<IContentLoader>();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public virtual ActionResult Success(XFormPostedData xFormPostedData)
        {
            var loader = ServiceLocator.Current.GetInstance<IContentLoader>();
            var urlResolver = ServiceLocator.Current.GetInstance<UrlResolver>();
            if (xFormPostedData.XForm.PageGuidAfterPost != Guid.Empty)
            {
                PermanentContentLinkMap pageMap = PermanentLinkMapStore.Find(xFormPostedData.XForm.PageGuidAfterPost) as PermanentContentLinkMap;
                if (pageMap != null)
                {
                    string internalUrl = pageMap.MappedUrl.ToString();
                    internalUrl = UriSupport.AddLanguageSelection(internalUrl, ContentLanguage.PreferredCulture.Name);
                    UrlBuilder urlBuilder = new UrlBuilder(internalUrl);
                    Global.UrlRewriteProvider.ConvertToExternal(urlBuilder, null, Encoding.UTF8);
                    Response.Redirect(urlBuilder.ToString(), true);
                }
            }
            var contentLink = HttpContext.Request.RequestContext.GetContentLink();
            var formId = xFormPostedData.XForm.Id.ToString();
            return Redirect(string.Format("{0}?action=success&formid={1}#{1}", UrlResolver.Current.GetUrl(contentLink), formId));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public virtual ActionResult Failed(XFormPostedData xFormPostedData)
        {
            return RedirectToAction("Index");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public virtual ActionResult XFormPost(XFormPostedData xFormPostedData)
        {
            if (Request.Form["email"] != null)
            {
                if (IsValid(this.Request.Form["email"]))
                {
                    if (xFormPostedData != null)
                    {
                        if (this.Request.Params["XFormId"] != null &&
                        !string.IsNullOrEmpty(this.Request.Params["XFormId"]))
                        {
                            var formData = new XFormPageHelper().GetXFormData(this, xFormPostedData);
                            formData.MailFrom = this.Request.Form["email"];

                            try
                            {
                                if (XFormActionHelper.DoAction(formData, xFormPostedData, true))
                                {
                                    return Success(xFormPostedData);
                                }
                                else
                                {
                                    return Failed(xFormPostedData);
                                }
                            }
                            catch (Exception ex)
                            {
                                this.ModelState.AddModelError("ProcessXFormAction", ex.Message);
                            }
                        }
                    }
                }
            }
            return this.xformHandler.HandleAction(this);
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (!string.IsNullOrEmpty(this.contentId))
            {
                if (TempData[ViewDataKey] != null)
                {
                    ViewData = (ViewDataDictionary)TempData[ViewDataKey];
                }
            }

            base.OnActionExecuting(filterContext);
        }

        protected override void OnResultExecuting(ResultExecutingContext filterContext)
        {
            if (!string.IsNullOrEmpty(this.contentId))
            {
                TempData[ViewDataKey] = ViewData;
            }

            base.OnResultExecuting(filterContext);
        }

        public bool IsValid(string emailaddress)
        {
            try
            {
                MailAddress m = new MailAddress(emailaddress);
                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }

        private readonly string _viewDataKeyFormat = "ViewData_{0}";

        private string ViewDataKey
        {
            get
            {
                return string.Format(_viewDataKeyFormat, this.contentId);
            }
        }
    }
}