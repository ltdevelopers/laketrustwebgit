﻿using EPiServer;
using EPiServer.Framework.DataAnnotations;
using EPiServer.Web.Mvc;
using LakeTrust.Models.Blocks.HQ;
using System;
using System.Linq;
using System.Web.Mvc;

namespace LakeTrust.Controllers.HQ
{
    [TemplateDescriptor(Inherited = true)]
    public class HQBlockController : BlockController<HQBaseBlockData>
    {
        public override ActionResult Index(HQBaseBlockData currentBlock)
        {
            return PartialView(string.Format("~/Views/Blocks/{0}/Index.cshtml", currentBlock.GetOriginalType().Name), currentBlock);
        }

        ///// <summary>
        ///// Creates a BlockViewModel where the type parameter is the type of the block.
        ///// </summary>
        ///// <remarks>
        ///// Used to create models of a specific type without the calling method having to know that type.
        ///// </remarks>
        //private static IBlockViewModel<SiteBlockData> CreateModel(SiteBlockData page)
        //{
        //    var type = typeof(BlockViewModel<>).MakeGenericType(page.GetOriginalType());
        //    return Activator.CreateInstance(type, page) as IBlockViewModel<SiteBlockData>;
        //}
    }
}