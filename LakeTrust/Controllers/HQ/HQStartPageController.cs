﻿using EPiServer;
using EPiServer.Core;
using EPiServer.SpecializedProperties;
using EPiServer.Web.Mvc;
using LakeTrust.Business;
using LakeTrust.Business.Interfaces;
using LakeTrust.Models.Pages.HQ;
using LakeTrust.Models.ViewModels;
using System;
using System.Linq;
using System.Web.Mvc;

namespace LakeTrust.Controllers.HQ
{
    public class HQStartPageController : HQPageControllerBase<HQStartPage>
    {
        private readonly IContentRepository contentRepository;
        private readonly IContentLoader contentLoader;
        private readonly ContentLocator contentLocator;

        public HQStartPageController(IContentLoader contentLoader, IContentRepository contentRepository, ContentLocator contentLocator)
        {
            this.contentLoader = contentLoader;
            this.contentRepository = contentRepository;
            this.contentLocator = contentLocator;
        }

        public virtual ActionResult Index(HQStartPage currentPage)
        {
            var model = new HQStartViewModel(currentPage);

            if (currentPage.MainContent != null)
            {
                foreach (var item in currentPage.MainContent.FilteredItems)
                {
                    var contentItem = item.GetContent();
                    if (contentItem is IHQDataItem)
                    {
                        if (((IHQDataItem)contentItem).IsNavigationPoint)
                        {
                            model.MainNavigation.Add(new LinkItem()
                            {
                                Href = string.Format("#{0}", contentItem.Name.ToLowerInvariant().Replace(" ", "-")),
                                Title = ((IHQDataItem)contentItem).ContentAreaCssClass,
                                Text = !string.IsNullOrEmpty(((IHQDataItem)contentItem).NavigationAnchorTitle) ? ((IHQDataItem)contentItem).NavigationAnchorTitle : contentItem.Name
                            });
                        }
                    }
                }
            }

            var editHints = ViewData.GetEditHints<HQStartViewModel, HQStartPage>();
            editHints.AddConnection(m => m.EmailAddress, p => p.EmailAddress);
            editHints.AddConnection(m => m.FacebookUrl, p => p.FacebookUrl);
            editHints.AddConnection(m => m.InstagramUrl, p => p.InstagramUrl);
            editHints.AddConnection(m => m.TwitterUrl, p => p.TwitterUrl);
            editHints.AddConnection(m => m.YouTubeUrl, p => p.YouTubeUrl);

            return View(model);
        }
    }
}