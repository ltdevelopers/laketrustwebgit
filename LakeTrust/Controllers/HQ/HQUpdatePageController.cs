﻿using EPiServer;
using EPiServer.Core;
using EPiServer.Editor;
using EPiServer.Framework.DataAnnotations;
using EPiServer.SpecializedProperties;
using EPiServer.Web.Routing;
using LakeTrust.Business;
using LakeTrust.Business.Interfaces;
using LakeTrust.Models.Pages.HQ;
using LakeTrust.Models.ViewModels;
using System;
using System.Linq;
using System.Web.Mvc;

namespace LakeTrust.Controllers.HQ
{
    [TemplateDescriptor(ModelType = typeof(HQUpdatePage), AvailableWithoutTag = true)]
    public class HQUpdatePageController : HQPageControllerBase<HQUpdatePage>
    {
        private readonly IContentRepository contentRepository;
        private readonly IContentLoader contentLoader;
        private readonly ContentLocator contentLocator;
        private readonly UrlResolver urlResolver;

        public HQUpdatePageController(IContentLoader contentLoader, IContentRepository contentRepository, ContentLocator contentLocator, UrlResolver urlResolver)
        {
            this.contentLoader = contentLoader;
            this.contentRepository = contentRepository;
            this.contentLocator = contentLocator;
            this.urlResolver = urlResolver;
        }

        public virtual ActionResult Index(HQUpdatePage currentPage)
        {
            var model = new HQUpdateViewModel(currentPage);
            model.MainNavigation = GetStartPageNavigation(currentPage);
            if (!PageEditing.PageIsInEditMode)
            {
                var children = this.contentLocator.FindPagesByPageType<HQUpdatePage>(currentPage.ParentLink, false);

                if (children.Count() > 1)
                {
                    var index = children.IndexOf(currentPage);
                    if (index == 0)
                        model.PreviousContent = null;
                    else
                        model.PreviousContent = children.ElementAt(index - 1);

                    if (index + 1 == children.Count())
                        model.NextContent = null;
                    else
                        model.NextContent = children.ElementAt(index + 1);
                }
            }

            return View(model);
        }

        public LinkItemCollection GetStartPageNavigation(HQUpdatePage currentPage)
        {
            var linkItemCollection = new LinkItemCollection();
            var startPage = this.contentLoader.Get<HQStartPage>(currentPage.ParentLink);
            if (startPage.MainContent != null)
            {
                foreach (var item in startPage.MainContent.FilteredItems)
                {
                    var contentItem = item.GetContent();
                    if (contentItem is IHQDataItem)
                    {
                        if (((IHQDataItem)contentItem).IsNavigationPoint)
                        {
                            linkItemCollection.Add(new LinkItem()
                            {
                                Href = string.Format("{0}#{1}", this.urlResolver.GetUrl(startPage.ContentLink), contentItem.Name.ToLowerInvariant().Replace(" ", "-")),
                                Title = ((IHQDataItem)contentItem).ContentAreaCssClass,
                                Text = contentItem.Name
                            });
                        }
                    }
                }
            }
            return linkItemCollection;
        }
    }
}