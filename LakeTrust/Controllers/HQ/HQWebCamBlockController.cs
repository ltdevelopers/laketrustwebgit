﻿using EPiServer.Framework.DataAnnotations;
using EPiServer.Framework.Web;
using EPiServer.Web.Mvc;
using LakeTrust.Business.Services;
using LakeTrust.Models.Blocks.HQ;
using Simple.ImageResizer;
using Simple.ImageResizer.MvcExtensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LakeTrust.Controllers.HQ
{
    [TemplateDescriptor(Inherited = true, ModelType = typeof(HQWebCamBlock), Default = true, AvailableWithoutTag = true, TemplateTypeCategory = TemplateTypeCategories.MvcPartialController)]
    public class HQWebCamBlockController : BlockController<HQWebCamBlock>
    {
        public override ActionResult Index(HQWebCamBlock currentBlock)
        {
            return PartialView(currentBlock);
        }
    }
}