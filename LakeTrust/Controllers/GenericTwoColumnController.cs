﻿using EPiServer.Core;
using EPiServer.Web.Mvc;
using LakeTrust.Models.ViewModels;
using System;
using System.Linq;
using System.Web.Mvc;

namespace LakeTrust.Controllers.Pages
{
    public class GenericTwoColumnController : PageController<PageData>
    {
        public virtual ActionResult Index(PageData currentPage)
        {
            var model = new GenericViewModel(currentPage);

            return View(model);
        }
    }
}