﻿using EPiServer;
using EPiServer.Core;
using EPiServer.Framework.DataAnnotations;
using EPiServer.Web;
using LakeTrust.Business;
using LakeTrust.Models.Pages;
using LakeTrust.Models.ViewModels;
using System;
using System.Linq;
using System.Web.Mvc;

namespace LakeTrust.Controllers
{
    [TemplateDescriptor(Default = true)]
    public class SiteMapPageController : PageControllerBase<SiteMapPage>
    {
        private readonly IContentRepository contentRepository;
        private readonly IContentLoader contentLoader;

        public SiteMapPageController(IContentLoader contentLoader, IContentRepository contentRepository)
        {
            this.contentLoader = contentLoader;
            this.contentRepository = contentRepository;
        }

        public virtual ActionResult Index(SiteMapPage currentPage)
        {
            var model = new SiteMapViewModel(currentPage);

            model.Children = this.contentLoader.GetChildren<PageData>(SiteDefinition.Current.StartPage)
                .FilterForDisplay(true, true);

            return View(model);
        }
    }
}