﻿using EPiServer;
using EPiServer.Core;
using EPiServer.Framework.DataAnnotations;
using LakeTrust.Business;
using LakeTrust.Models.Pages;
using LakeTrust.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace LakeTrust.Controllers
{
    [TemplateDescriptor(Default = true)]
    public class CalendarPageController : PageControllerBase<CalendarPage>
    {
        private readonly IContentRepository contentRepository;
        private readonly IContentLoader contentLoader;
        private readonly ContentLocator contentLocator;

        public CalendarPageController(IContentLoader contentLoader, ContentLocator contentLocator, IContentRepository contentRepository)
        {
            this.contentLoader = contentLoader;
            this.contentRepository = contentRepository;
            this.contentLocator = contentLocator;
        }

        public virtual ActionResult Index(CalendarPage currentPage)
        {
            var filteredPages = GetEventItems(currentPage.ContentLink);
            var model = new ListingViewModel<CalendarPage, CalendarEventPage>(currentPage)
            {
                TotalItems = filteredPages.Count(),
                PageIndex = 1,
                PageWeight = 10,
                Pages = filteredPages.Take(10)
            };

            return View(model);
        }

        public virtual ActionResult Paging(CalendarPage currentPage, int pageIndex = 1)
        {
            var filteredPages = GetEventItems(currentPage.ContentLink);
            var model = new ListingViewModel<CalendarPage, CalendarEventPage>(currentPage)
            {
                TotalItems = filteredPages.Count(),
                PageIndex = pageIndex,
                PageWeight = 10,
                Pages = filteredPages.Skip((10 * pageIndex) - 10).Take(10)
            };

            return View("Index", model);
        }

        private IEnumerable<CalendarEventPage> GetEventItems(ContentReference contentLink)
        {
            var list = new List<CalendarEventPage>();

            var eventItems = this.contentLocator.FindPagesByPageType<CalendarEventPage>(contentLink, true)
                .FilterForDisplay(true, true)
                .Cast<CalendarEventPage>();

            foreach (CalendarEventPage model in eventItems.OrderBy(x => x.EventDateTime))
            {
                if (model.HasEndDate)
                {
                    if (model.EventEndDateTime > DateTime.Now)
                    {
                        list.Add(model);
                        continue;
                    }
                }

                if (model.EventDateTime >= DateTime.Now)
                    list.Add(model);
            }

            return list;
        }
    }
}