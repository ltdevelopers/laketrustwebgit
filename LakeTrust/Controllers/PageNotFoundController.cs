﻿using BVNetwork.FileNotFound.Redirects;
using EPiServer;
using EPiServer.Data.Dynamic;
using LakeTrust.Business;
using LakeTrust.Models.Pages;
using LakeTrust.Models.ViewModels;
using System;
using System.Linq;
using System.Web.Mvc;

namespace LakeTrust.Controllers.Pages
{
    public class PageNotFoundController : PageControllerBase<PageNotFound>
    {
        private readonly IContentRepository contentRepository;
        private readonly IContentLoader contentLoader;
        private readonly ContentLocator contentLocator;

        public PageNotFoundController(IContentLoader contentLoader, IContentRepository contentRepository, ContentLocator contentLocator)
        {
            this.contentLoader = contentLoader;
            this.contentRepository = contentRepository;
            this.contentLocator = contentLocator;
        }

        public virtual ActionResult Index(PageNotFound currentPage)
        {
            var model = new PageNotFoundViewModel(currentPage);

            this.VerifyUrl();

            return View(model);
        }

        private void VerifyUrl()
        {
            var url = this.Request.Url.ToString().Trim();
            string location = string.Empty;
            if (url.Contains("404;"))
            {
                var trimmed = url.Replace("http://", string.Empty);
                var realUrl = trimmed.Substring(trimmed.LastIndexOf("404;")).Replace("404;", string.Empty);
                location = realUrl.Substring(realUrl.IndexOf("/"));
            }
            if (!string.IsNullOrWhiteSpace(location))
            {
                var store = typeof(CustomRedirect).GetStore();
                var urls = store.Items<CustomRedirect>()
                    .Where(x => x.OldUrl == location);

                if (urls.Count() > 0)
                {
                    HttpContext.Response.StatusCode = 301;
                    HttpContext.Response.RedirectPermanent(urls.FirstOrDefault().NewUrl, false);
                }
            }
        }
    }
}