﻿using EPiServer.Core;
using EPiServer.Web.Mvc;
using System;
using System.Linq;
using System.Web.Mvc;

namespace LakeTrust.Controllers.Pages
{
    public class GenericPageController : PageController<PageData>
    {
        public virtual ActionResult Index(PageData currentPage)
        {
            return View();
        }
    }
}