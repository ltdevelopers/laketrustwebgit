﻿using EPiServer.Web.Mvc;
using EPiServer.Web.Routing;
using LakeTrust.Models.Media;
using LakeTrust.Models.ViewModels;
using System.Web.Mvc;

namespace LakeTrust.Controllers
{
    /// <summary>
    /// Controller for the image file.
    /// </summary>
    public class ImageFileController : PartialContentController<ImageFile>
    {
        private readonly UrlResolver urlResolver;

        public ImageFileController(UrlResolver urlResolver)
        {
            this.urlResolver = urlResolver;
        }

        /// <summary>
        /// The index action for the image file. Creates the view model and renders the view.
        /// </summary>
        /// <param name="currentContent">The current image file.</param>
        public override ActionResult Index(ImageFile currentContent)
        {
            var model = new ImageViewModel
            {
                Url = this.urlResolver.GetUrl(currentContent.ContentLink),
                Name = currentContent.Name
            };

            return PartialView(model);
        }
    }
}