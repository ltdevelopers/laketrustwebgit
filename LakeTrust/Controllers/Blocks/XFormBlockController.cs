﻿using EPiServer;
using EPiServer.Core;
using EPiServer.Web;
using EPiServer.Web.Mvc;
using EPiServer.Web.Routing;
using LakeTrust.Models.Blocks;
using LakeTrust.Models.Pages;
using LakeTrust.Models.ViewModels;
using System;
using System.Linq;
using System.Web.Mvc;

namespace LakeTrust.Controllers.Blocks
{
    public class XFormBlockController : BlockController<FormBlock>
    {
        private readonly UrlResolver urlResolver;
        private readonly IContentLoader contentLoader;
        private readonly PageRouteHelper pageRouteHelper;
        private readonly SiteDefinition siteDefinition;

        public XFormBlockController(UrlResolver urlResolver, IContentLoader contentLoader, PageRouteHelper pageRouteHelper, SiteDefinition siteDefinition)
        {
            this.urlResolver = urlResolver;
            this.pageRouteHelper = pageRouteHelper;
            this.contentLoader = contentLoader;
            this.siteDefinition = siteDefinition;
        }

        public override ActionResult Index(FormBlock currentBlock)
        {
            var currentBlockID = (currentBlock as IContent).ContentLink.ID;
            var viewDataKey = string.Format("ViewData_{0}", currentBlockID);

            if (TempData[viewDataKey] != null)
            {
                ViewData = (ViewDataDictionary)TempData[viewDataKey];
            }

            PageData currentPage = pageRouteHelper.Page;

            var model = BlockViewModel.Create(currentBlock);
            // For block preview mode, we need to have a current page, but since preview isn't
            // really a page, we'll use the start page This won't matter anyway. If you try to
            // submit the form, the form selection window will pop up.
            if (currentPage == null)
                currentPage = contentLoader.Get<StartPage>(this.siteDefinition.StartPage);

            if (currentBlock.BlockForm != null && currentPage != null)
            {
                var pageUrl = this.urlResolver.GetUrl(currentPage.ContentLink);

                var actionUri = string.Format("{0}XFormPost/", pageUrl);
                actionUri = UriSupport.AddQueryString(actionUri, "XFormId", currentBlock.BlockForm.Id.ToString());
                actionUri = UriSupport.AddQueryString(actionUri, "failedAction", "Failed");
                actionUri = UriSupport.AddQueryString(actionUri, "successAction", "Success");
                actionUri = UriSupport.AddQueryString(actionUri, "contentId", currentBlockID.ToString());

                currentBlock.ActionUri = actionUri;
            }

            return PartialView(model);
        }
    }
}