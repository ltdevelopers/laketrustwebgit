﻿using EPiServer;
using EPiServer.Core;
using EPiServer.Framework.DataAnnotations;
using EPiServer.Web.Mvc;
using LakeTrust.Business;
using LakeTrust.Models.Blocks;
using LakeTrust.Models.Pages;
using LakeTrust.Models.ViewModels;
using System;
using System.Linq;
using System.Web.Mvc;

namespace LakeTrust.Controllers.Blocks
{
    [TemplateDescriptor(Default = true, ModelType = typeof(RotatorBlock))]
    public partial class RotatorBlockController : BlockController<RotatorBlock>
    {
        private ContentLocator contentLocator;
        private IContentLoader contentLoader;

        public RotatorBlockController(ContentLocator contentLocator, IContentLoader contentLoader)
        {
            this.contentLocator = contentLocator;
            this.contentLoader = contentLoader;
        }

        public override ActionResult Index(RotatorBlock currentBlock)
        {
            var model = new RotatorBlockViewModel();
            if (currentBlock.RotatorBlockItems != null && currentBlock.RotatorBlockItems.FilteredItems.Count() > 0)
            {
                foreach (ContentAreaItem item in currentBlock.RotatorBlockItems.FilteredItems)
                {
                    if (item.GetContent() is SlideItem)
                    {
                        model.RotatorItems.Add(contentLoader.Get<SlideItem>(item.ContentLink));
                    }
                }
            }
            return PartialView(model);
        }
    }
}