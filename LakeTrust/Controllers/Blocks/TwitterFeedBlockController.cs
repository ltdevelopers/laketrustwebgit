﻿using EPiServer;
using EPiServer.Framework.DataAnnotations;
using EPiServer.Web.Mvc;
using LakeTrust.Business;
using LakeTrust.Business.Services;
using LakeTrust.Models.Blocks;
using LakeTrust.Models.ViewModels;
using LinqToTwitter;
using System;
using System.Linq;
using System.Web.Mvc;

namespace LakeTrust.Controllers.Blocks
{
    [TemplateDescriptor(Default = true, AvailableWithoutTag = true, ModelType = typeof(TwitterFeedBlock))]
    public class TwitterFeedBlockController : BlockController<TwitterFeedBlock>
    {
        private readonly ContentLocator contentLocator;
        private readonly IContentLoader contentLoader;
        private readonly CacheHelper cacheHelper;
        private readonly TwitterFeedService twitterFeed;

        public TwitterFeedBlockController(ContentLocator contentLocator, IContentLoader contentLoader, TwitterFeedService twitterFeed)
        {
            this.contentLocator = contentLocator;
            this.contentLoader = contentLoader;
            this.twitterFeed = twitterFeed;
            this.cacheHelper = new CacheHelper();
        }

        public override ActionResult Index(TwitterFeedBlock currentBlock)
        {
            var model = new BlockListViewModel<TwitterFeedBlock, Status>(currentBlock);
            var editHints = ViewData.GetEditHints<BlockListViewModel<TwitterFeedBlock, Status>, TwitterFeedBlock>();
            editHints.AddConnection(m => m.CurrentBlock.BlockTitle, p => p.BlockTitle);

            model.Items = this.twitterFeed.GetTweets(currentBlock.NumberOfTwitterItems, currentBlock.TwitterUserName);

            return PartialView(model);
        }
    }
}