﻿using EPiServer;
using EPiServer.Core;
using EPiServer.Data.Dynamic;
using EPiServer.Framework.DataAnnotations;
using EPiServer.Web;
using EPiServer.Web.Mvc;
using EPiServer.Web.Routing;
using EPiServer.XForms;
using LakeTrust.Business;
using LakeTrust.Models.Blocks;
using LakeTrust.Models.Pages;
using LakeTrust.Models.ViewModels;
using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LakeTrust.Controllers.Blocks
{
    [TemplateDescriptor(Default = true, ModelType = typeof(PollResultBlock), AvailableWithoutTag = true)]
    public class PollResultBlockController : BlockController<PollResultBlock>
    {
        private readonly UrlResolver urlResolver;
        private readonly IContentLoader contentLoader;
        private readonly PageRouteHelper pageRouteHelper;
        private readonly SiteDefinition siteDefinition;
        private readonly XFormDataFactory xFormDataFactory;

        public PollResultBlockController(UrlResolver urlResolver, IContentLoader contentLoader, PageRouteHelper pageRouteHelper, SiteDefinition siteDefinition, XFormDataFactory xFormDataFactory)
        {
            this.urlResolver = urlResolver;
            this.pageRouteHelper = pageRouteHelper;
            this.contentLoader = contentLoader;
            this.siteDefinition = siteDefinition;
            this.xFormDataFactory = xFormDataFactory;
        }

        public override ActionResult Index(PollResultBlock currentBlock)
        {
            var model = new PollResultViewModel(currentBlock);

            var currentBlockID = (currentBlock as IContent).ContentLink.ID;
            var viewDataKey = string.Format("ViewData_{0}", currentBlockID);

            if (TempData[viewDataKey] != null)
            {
                ViewData = (ViewDataDictionary)TempData[viewDataKey];
            }

            PageData currentPage = pageRouteHelper.Page;

            // For block preview mode, we need to have a current page, but since preview isn't
            // really a page, we'll use the start page This won't matter anyway. If you try to
            // submit the form, the form selection window will pop up.
            if (currentPage == null)
                currentPage = contentLoader.Get<StartPage>(this.siteDefinition.StartPage);

            if (currentBlock.FormPage != null && currentPage != null)
            {
                var pageUrl = this.urlResolver.GetUrl(currentPage.ContentLink);

                var actionUri = string.Format("{0}XFormPost/", pageUrl);
                actionUri = UriSupport.AddQueryString(actionUri, "XFormId", currentBlock.FormPage.Id.ToString());
                actionUri = UriSupport.AddQueryString(actionUri, "failedAction", "Failed");
                actionUri = UriSupport.AddQueryString(actionUri, "successAction", "Success");
                actionUri = UriSupport.AddQueryString(actionUri, "contentId", currentBlockID.ToString());

                currentBlock.ActionUri = actionUri;

                var form = XForm.CreateInstance(new Guid(currentBlock.FormPage.Id.ToString()));
                var data = form.CreateFormData();
                bool hasSubmitted = HasAlreadyPosted(data, this.Request.Cookies);

                model.DisplayResults = hasSubmitted;
                if (hasSubmitted)
                {
                    model.PollResult = this.xFormDataFactory.GetPollResults(form);
                }
            }

            return PartialView(model);
        }

        public bool HasAlreadyPosted(XFormData data, HttpCookieCollection cookieCollection)
        {
            if ((ChannelOptions.Database != ChannelOptions.Database) || string.IsNullOrEmpty(HttpContext.User.Identity.Name))
            {
                return CheckCookieForPostedForm(data.FormId, cookieCollection);
            }
            if (Guid.Empty.Equals(data.FormId))
            {
                throw new InvalidOperationException("Cannot read the XFormData before the FormName property has been set");
            }
            return ((from post in GetStore(data.FormId).ItemsAsPropertyBag()
                     where (((Guid)post["Meta_FormId"]) == data.FormId) && (((string)post["Meta_UserName"]) == HttpContext.User.Identity.Name)
                     select post).Count<PropertyBag>() > 0);
        }

        public static DynamicDataStore GetStore(Guid formId)
        {
            return DynamicDataStoreFactory.Instance.GetStore(GetStoreName(formId));
        }

        public static string GetStoreName(Guid formId)
        {
            return string.Format("XFormData_{0}", formId.ToString().Replace("-", "").ToUpperInvariant());
        }

        private static bool CheckCookieForPostedForm(Guid formId, HttpCookieCollection cookieCollection)
        {
            HttpCookie cookie = cookieCollection["FormCookie"];
            if (cookie != null)
            {
                foreach (string str in cookie.Values.Keys)
                {
                    if (str.Equals(formId.ToString()))
                    {
                        return true;
                    }
                }
            }
            return false;
        }
    }
}