﻿using EPiServer;
using EPiServer.Core;
using EPiServer.Framework.DataAnnotations;
using EPiServer.Web.Mvc;
using LakeTrust.Business;
using LakeTrust.Models.Blocks;
using LakeTrust.Models.ViewModels;
using System.Web.Mvc;

namespace LakeTrust.Controllers.Blocks
{
    [TemplateDescriptor(Default = true, ModelType = typeof(ChildListBlock), AvailableWithoutTag = true)]
    public class ChildListBlockController : BlockController<ChildListBlock>
    {
        private ContentLocator contentLocator;
        private IContentLoader contentLoader;

        public ChildListBlockController(ContentLocator contentLocator, IContentLoader contentLoader)
        {
            this.contentLocator = contentLocator;
            this.contentLoader = contentLoader;
        }

        public override ActionResult Index(ChildListBlock currentBlock)
        {
            var model = new BlockListViewModel<ChildListBlock, PageData>(currentBlock);

            if (currentBlock.ParentPageReference != null && currentBlock.ParentPageReference != ContentReference.EmptyReference)
            {
                model.Items = this.contentLoader.GetChildren<PageData>(currentBlock.ParentPageReference).FilterForDisplay(false, false);
            }

            return PartialView(model);
        }
    }
}