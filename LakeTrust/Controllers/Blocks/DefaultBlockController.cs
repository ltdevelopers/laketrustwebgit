﻿using EPiServer;
using EPiServer.Framework.DataAnnotations;
using EPiServer.Web.Mvc;
using LakeTrust.Models.Blocks;
using System;
using System.Linq;
using System.Web.Mvc;

namespace LakeTrust.Controllers.Blocks
{
    [TemplateDescriptor(Inherited = true)]
    public class DefaultBlockController : BlockController<SiteBlockData>
    {
        public override ActionResult Index(SiteBlockData currentBlock)
        {
            return PartialView(string.Format("~/Views/Blocks/{0}.cshtml", currentBlock.GetOriginalType().Name), currentBlock);
        }

        ///// <summary>
        ///// Creates a BlockViewModel where the type parameter is the type of the block.
        ///// </summary>
        ///// <remarks>
        ///// Used to create models of a specific type without the calling method having to know that type.
        ///// </remarks>
        //private static IBlockViewModel<SiteBlockData> CreateModel(SiteBlockData page)
        //{
        //    var type = typeof(BlockViewModel<>).MakeGenericType(page.GetOriginalType());
        //    return Activator.CreateInstance(type, page) as IBlockViewModel<SiteBlockData>;
        //}
    }
}