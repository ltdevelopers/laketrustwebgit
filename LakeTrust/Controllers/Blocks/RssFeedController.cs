﻿using EPiServer.Framework.DataAnnotations;
using EPiServer.Web.Mvc;
using LakeTrust.Business;
using LakeTrust.Models.Blocks;
using LakeTrust.Models.ViewModels;
using RssFeedImporter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace LakeTrust.Controllers.Blocks
{
    [TemplateDescriptor(ModelType = typeof(RssFeedBlock), Default = true, AvailableWithoutTag = true)]
    public class RssFeedBlockController : BlockController<RssFeedBlock>
    {
        private readonly CacheHelper cacheHelper;

        public RssFeedBlockController(CacheHelper cacheHelper)
        {
            this.cacheHelper = cacheHelper;
        }

        public override ActionResult Index(RssFeedBlock currentBlock)
        {
            var model = new BlockListViewModel<RssFeedBlock, RssItem>(currentBlock);
            var rssItems = new List<RssItem>();

            if (currentBlock.RssUrl != null)
            {
                if (!this.cacheHelper.Get<List<RssItem>>(currentBlock.RssUrl.ToString(), out rssItems))
                {
                    var rssFeed = new RssFeedImporter.RssFeed(currentBlock.RssUrl.ToString());
                    rssItems = rssFeed.Items.Take(currentBlock.NumberOfItemsToShow).ToList();
                    this.cacheHelper.Add<List<RssItem>>(currentBlock.RssUrl.ToString(), rssItems);
                }
            }
            model.Items = rssItems;
            return PartialView(model);
        }
    }
}