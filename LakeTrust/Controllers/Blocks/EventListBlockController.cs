﻿using EPiServer.Framework.DataAnnotations;
using EPiServer.Web;
using EPiServer.Web.Mvc;
using LakeTrust.Business;
using LakeTrust.Models.Blocks;
using LakeTrust.Models.Pages;
using LakeTrust.Models.ViewModels;
using System;
using System.Linq;
using System.Web.Mvc;

namespace LakeTrust.Controllers.Blocks
{
    [TemplateDescriptor(Default = true, ModelType = typeof(EventListBlock), AvailableWithoutTag = true)]
    public class EventListBlockController : BlockController<EventListBlock>
    {
        private ContentLocator contentLocator;
        private readonly SiteDefinition siteDefinition;

        public EventListBlockController(ContentLocator contentLocator, SiteDefinition siteDefinition)
        {
            this.contentLocator = contentLocator;
            this.siteDefinition = siteDefinition;
        }

        public override ActionResult Index(EventListBlock currentBlock)
        {
            var model = new BlockListViewModel<EventListBlock, CalendarEventPage>(currentBlock);
            model.Items = this.contentLocator.FindPagesByPageType<CalendarEventPage>(siteDefinition.StartPage, true)
                .FilterForDisplay()
               .Where(x => x.EventDateTime >= DateTime.Today)
               .OrderBy(x => x.EventDateTime)
               .Take(currentBlock.NumberOfItemsToShow == 0 ? 5 : currentBlock.NumberOfItemsToShow);

            return PartialView(model);
        }
    }
}