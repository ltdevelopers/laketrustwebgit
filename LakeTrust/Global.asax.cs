﻿using EPiServer.Core;
using EPiServer.ServiceLocation;
using EPiServer.Web.Routing;
using EPiServer.Web.Routing.Segments;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace LakeTrust
{
    public class Global : EPiServer.Global
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
        }

        protected void Application_BeginRequest(Object sender, EventArgs e)
        {
            if (HttpContext.Current.Request.IsSecureConnection.Equals(false) && !Request.IsLocal)
            {
                Response.Redirect("https://" + Request.ServerVariables["HTTP_HOST"] + HttpContext.Current.Request.RawUrl);
            }
        }

        protected override void RegisterRoutes(RouteCollection routes)
        {
            if (!ContentReference.IsNullOrEmpty(ContentReference.StartPage))
            {
                IUrlSegmentRouter instance = ServiceLocator.Current.GetInstance<IUrlSegmentRouter>();
                instance.RootResolver = (s) => s.StartPage;
                MapContentRouteParameters parameters = new MapContentRouteParameters
                {
                    UrlSegmentRouter = instance,
                    BasePathResolver = new Func<RequestContext, RouteValueDictionary, string>(EPiServer.Web.Routing.RouteCollectionExtensions.ResolveBasePath),
                    Direction = SupportedDirection.Incoming
                };

                routes.MapContentRoute(
                    "PaginationRoute",
                    "{language}/{node}/{action}/{pageIndex}/{searchTerm}",
                    new
                    {
                        controller = "BlogPage",
                        action = "paging",
                        pageIndex = UrlParameter.Optional,
                        searchTerm = UrlParameter.Optional
                    }, parameters);
            }

            base.RegisterRoutes(routes);
            routes.MapRoute("default", "{controller}/{action}", new { action = "index" });
        }

        public static class GroupNames
        {
            public const string Blocks = "Blocks";
            public const string Navigation = "Navigation";
            public const string Footer = "Footer";
            public const string SEO = "SEO";
            public const string Images = "Images";
            public const string DisplaySettings = "Display Settings";
            public const string TwitterSettings = "Twitter Settings";
            public const string Forms = "Forms";
            public const string LinkSettings = "Link Settings";
            public const string Blog = "Blog";
            public const string Default = "Default";
            public const string HQ = "HQ";
            public const string Calendar = "Calendar";
            public const string Specialized = "Specialized";
            public const string News = "News";
            public const string Product = "Product";
            public const string LakeTrustU = "Lake Trust U";
        }

        public static class ContentAreaTags
        {
            public const string FullWidth = "span12";
            public const string TwoThirdsWidth = "span8";
            public const string HalfWidth = "span6";
            public const string OneThirdWidth = "span4";
            public const string OneQuarterWidth = "span3";
            public const string OneSixthWidth = "span2";
            public const string HomePage = "HomePage";
            public const string NoRenderer = "norenderer";
        }

        public static class ContentAreaWidths
        {
            public const int FullWidth = 12;
            public const int TwoThirdsWidth = 8;
            public const int HalfWidth = 6;
            public const int OneThirdWidth = 4;
            public const int OneQuarterWidth = 3;
            public const int OneSixthWidth = 2;
            public const int HomePage = 3;
        }

        public static class BootstrapTags
        {
            public const string FullWidth = "col-sm-12";
            public const string TwoThirdWidth = "col-sm-8";
            public const string HalfWidth = "col-sm-6";
            public const string OneThirdWidth = "col-sm-4";
        }

        public static Dictionary<string, int> BootstrapContentAreaTagWidths = new Dictionary<string, int>
        {
            { BootstrapTags.FullWidth, ContentAreaWidths.FullWidth },
            { BootstrapTags.TwoThirdWidth, ContentAreaWidths.TwoThirdsWidth },
            { BootstrapTags.HalfWidth, ContentAreaWidths.HalfWidth },
            { BootstrapTags.OneThirdWidth, ContentAreaWidths.OneThirdWidth }
        };

        public static class SiteUIHints
        {
            public const string Strings = "StringList";
            public const string CustomTag = "CustomTag";
            public const string ColorClass = "ColorClass";
            public const string HQColorClass = "HQColorClass";
            public const string Tags = "Tags";
            public const string BackgroundPlacementMultiple = "BackgroundPlacementMultiple";
        }

        public static class StringConstants
        {
            public const string BlogCategoryRoot = "Blog Root";
            public const string BlogStartReference = "BlogStartReference";
            public const string StaticGraphicsFolderPath = "~/Content/Images/GroupImages/";
        }

        public static class Views
        {
            public const string HomePage4Column = "~/Views/Pages/StartPage/HomePage4Column.cshtml";
        }
    }
}