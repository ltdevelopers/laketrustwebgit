﻿using System;
using System.Linq;

namespace LakeTrust.Business.Interfaces
{
    public interface IHQDataItem
    {
        string ContentAreaCssClass { get; }

        bool IsNavigationPoint { get; }

        string NavigationAnchorTitle { get; set; }
    }
}