﻿using System;
using System.Linq;

namespace LakeTrust.Business.Rendering
{
    internal interface ICustomCssInContentArea
    {
        string ContentAreaCssClass { get; }
    }
}