﻿using BlendSolrManager.Interfaces;
using BlendSolrManager.Services;
using EPiServer;
using EPiServer.Core;
using EPiServer.Framework;
using EPiServer.ServiceLocation;
using EPiServer.Web;
using LakeTrust.Business.Initialization;
using log4net;
using SolrNet;
using System;
using System.Configuration;

namespace LakeTrust.Business.SolrIntegration
{
    [InitializableModule, ModuleDependency(typeof(InitializationModule))]
    public class SolrSearchModule : IInitializableModule
    {
        private static ILog iLog = LogManager.GetLogger(typeof(SolrSearchModule));
        private static ISolrIndexer blendSolrService;
        private static IContentRepository contentRepository;
        private static ISolrOperations<SearchDocument> solrFactory;

        public void Initialize(EPiServer.Framework.Initialization.InitializationEngine context)
        {
            contentRepository = ServiceLocator.Current.GetInstance<IContentRepository>();
            solrFactory = ServiceLocator.Current.GetInstance<ISolrOperations<SearchDocument>>();
            blendSolrService = new SolrIndexer<SearchDocument>();

            DataFactory.Instance.PublishedPage += new PageEventHandler(SolrSearchModule.DataFactory_PublishedPage);
            DataFactory.Instance.MovedPage += new PageEventHandler(SolrSearchModule.DataFactory_MovedPage);
            DataFactory.Instance.DeletingPage += new PageEventHandler(SolrSearchModule.DataFactory_DeletingPage);

            DataFactory.Instance.PublishedContent += new EventHandler<ContentEventArgs>(SolrSearchModule.Instance_PublishedContent);
            DataFactory.Instance.DeletedContent += new EventHandler<DeleteContentEventArgs>(SolrSearchModule.Instance_DeletedContent);
        }

        public void Uninitialize(EPiServer.Framework.Initialization.InitializationEngine context)
        {
            DataFactory.Instance.PublishedPage -= new PageEventHandler(SolrSearchModule.DataFactory_PublishedPage);
            DataFactory.Instance.MovedPage -= new PageEventHandler(SolrSearchModule.DataFactory_MovedPage);
            DataFactory.Instance.DeletingPage -= new PageEventHandler(SolrSearchModule.DataFactory_DeletingPage);

            DataFactory.Instance.PublishedContent -= new EventHandler<ContentEventArgs>(SolrSearchModule.Instance_PublishedContent);
            DataFactory.Instance.DeletedContent -= new EventHandler<DeleteContentEventArgs>(SolrSearchModule.Instance_DeletedContent);
        }

        private static void DataFactory_DeletingPage(object sender, PageEventArgs e)
        {
            try
            {
                var pageData = contentRepository.Get<PageData>(e.ContentLink);
                blendSolrService.Delete(pageData);
            }
            catch (Exception ex)
            {
                iLog.Error(ex);
            }
        }

        private static void DataFactory_MovedPage(object sender, PageEventArgs e)
        {
            var content = contentRepository.Get<IContent>(e.ContentLink);
            blendSolrService.IndexPageTree(content);
        }

        private static void DataFactory_PublishedPage(object sender, PageEventArgs e)
        {
            try
            {
                var page = contentRepository.Get<PageData>(e.ContentLink);
            }
            catch (Exception ex)
            {
                iLog.Error(ex);
            }

            blendSolrService.Index(contentRepository.Get<PageData>(e.ContentLink), true, true);
        }

        private static void Instance_PublishedContent(object sender, ContentEventArgs e)
        {
            if (e.Content == null)
                return;

            if (e.Content is BlockData)
            {
            }
        }

        private static void Instance_DeletedContent(object sender, DeleteContentEventArgs e)
        {
        }

        public void Preload(string[] parameters)
        {
            throw new NotImplementedException();
        }
    }
}