﻿using BlendSolrManager.Services;
using EPiServer;
using EPiServer.BaseLibrary.Scheduling;
using EPiServer.Core;
using EPiServer.PlugIn;
using EPiServer.ServiceLocation;
using System.Linq;

namespace LakeTrust.Business.SolrIntegration
{
    [ScheduledPlugIn(DisplayName = "Solr Index - Solr Re-Index")]
    public class SolrSearchScheduleJob : JobBase
    {
        public SolrSearchScheduleJob()
        {
        }

        public override string Execute()
        {
            var contentRepo = ServiceLocator.Current.GetInstance<IContentRepository>();
            var blendSolr = new SolrIndexer<SearchDocument>();
            blendSolr.Clear();

            var descendants = contentRepo.GetDescendents(ContentReference.StartPage);
            var iContentItems = contentRepo.GetItems(descendants, LanguageSelector.AutoDetect()).ToList();
            blendSolr.Index(iContentItems, true, 50);
            return string.Format("Indexing {0} Content Items", iContentItems.Count);
        }
    }
}