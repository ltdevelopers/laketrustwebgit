﻿using BlendSolrManager.Models;
using SolrNet.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LakeTrust.Business.SolrIntegration
{
    public class SearchDocument : EPiServerBaseSolrDocument
    {
        public SearchDocument()
        {
            this.Keywords = new List<string>();
            this.Tags = new List<string>();
        }

        [SolrField("Description")]
        public string Description { get; set; }

        [SolrField("Keywords")]
        public List<string> Keywords { get; set; }

        [SolrField("Tags")]
        public List<string> Tags { get; set; }
    }
}