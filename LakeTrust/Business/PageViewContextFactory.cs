using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.Globalization;
using EPiServer.Web;
using EPiServer.Web.Routing;
using LakeTrust.Models.Pages;
using LakeTrust.Models.ViewModels;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;

namespace LakeTrust.Business
{
    public class PageViewContextFactory
    {
        private readonly IContentLoader contentLoader;
        private readonly UrlResolver urlResolver;
        private readonly SiteDefinition siteDefinition;
        private readonly IContentTypeRepository contentTypeRepository;
        private StartPage startPage;

        public PageViewContextFactory(IContentLoader contentLoader, UrlResolver urlResolver, SiteDefinition siteDefinition, IContentTypeRepository contentTypeRepository)
        {
            this.contentLoader = contentLoader;
            this.urlResolver = urlResolver;
            this.siteDefinition = siteDefinition;
            this.startPage = this.contentLoader.Get<StartPage>(this.siteDefinition.StartPage);
            this.contentTypeRepository = contentTypeRepository;
        }

        public virtual LayoutModel CreateLayoutModel(ContentReference currentContentLink, RequestContext requestContext)
        {
            var model = new LayoutModel
            {
                LoginUrl = new MvcHtmlString(GetLoginUrl(currentContentLink)),
                CustomHeaderScripts = new MvcHtmlString(startPage.GetPropertyValue<string>("CustomHeadScripts", string.Empty)),
                LoggedIn = HttpContext.Current.User.Identity.IsAuthenticated,
                HideScreenReaderLink = false
            };

            var content = this.contentLoader.Get<IContent>(currentContentLink);
            if (content is PageData)
            {
                var currentPage = contentLoader.Get<PageData>(currentContentLink);
                model.BodyClasses = GetBodyClasses(currentPage);
            }

            return model;
        }

        public virtual PageHeaderModel CreatePageHeaderModel(ContentReference currentContentLink)
        {
            var startPage = this.contentLoader.Get<StartPage>(this.siteDefinition.StartPage);
            var model = new PageHeaderModel
            {
                SearchPageContentLink = startPage.SearchResultsPage,
                MyCreditCartText = startPage.MyCreditCardText,
                MyCreditCardLink = startPage.MyCreditCard,
                HeaderNavigation = startPage.HeaderNavigation,
                MainNavigation = this.GetMainNavigation(),
                LoginContent = startPage.LoginContent
            };

            if (!ContentReference.IsNullOrEmpty(startPage.DesktopBankingLoginPage))
                model.DesktopBankingLoginPage = this.contentLoader.Get<IContent>(startPage.DesktopBankingLoginPage);

            if (!ContentReference.IsNullOrEmpty(startPage.MobileBankingLoginPage))
                model.MobileBankingLoginPage = this.contentLoader.Get<IContent>(startPage.MobileBankingLoginPage);

            var content = this.contentLoader.Get<IContent>(currentContentLink);
            if (content != null && content is LakeTrustBaseModel)
                model.IsLandingPage = content.ContentTypeID == this.contentTypeRepository.Load<LandingPage>().ID;

            return model;
        }

        public virtual PageFooterModel CreatePageFooterModel(ContentReference currentContentLink)
        {
            var startPage = this.contentLoader.Get<StartPage>(this.siteDefinition.StartPage);
            return new PageFooterModel
            {
                FooterText = startPage.FooterText,
                FooterNavigation = startPage.FooterNavigation,
                FooterAddress = startPage.FooterAddress,
                SocialContentArea = startPage.SocialContentArea
            };
        }

        public string GetBodyClasses(PageData currentPage)
        {
            var list = new List<string>();
            if (currentPage != null)
            {
                list.Add(currentPage.PageTypeName.ToLower().Replace("page", ""));
                if (currentPage is StandardPage)
                {
                    if (currentPage["RightFixedBlockArea"] != null)
                        list.Add("has-right-column");
                }
            }
            return string.Join(" ", list);
        }

        private string GetLoginUrl(ContentReference returnToContentLink)
        {
            return string.Format("{0}?ReturnUrl={1}", FormsAuthentication.LoginUrl, urlResolver.GetUrl(returnToContentLink));
        }

        public virtual IContent GetSection(ContentReference contentLink)
        {
            var currentContent = contentLoader.Get<IContent>(contentLink);
            if (currentContent.ParentLink != null && currentContent.ParentLink.CompareToIgnoreWorkID(this.siteDefinition.StartPage))
            {
                return currentContent;
            }

            return contentLoader.GetAncestors(contentLink)
                .OfType<PageData>()
                .SkipWhile(x => x.ParentLink == null || (!x.ParentLink.CompareToIgnoreWorkID(this.siteDefinition.StartPage) && !x.ParentLink.CompareToIgnoreWorkID(this.siteDefinition.RootPage)))
                .FirstOrDefault();
        }

        public RouteValueDictionary GetPageRoute(RequestContext requestContext, PageReference pageLink)
        {
            var values = new RouteValueDictionary();
            values[RoutingConstants.NodeKey] = pageLink;
            values[RoutingConstants.LanguageKey] = ContentLanguage.PreferredCulture.Name;
            return values;
        }

        private IEnumerable<PageData> GetMainNavigation()
        {
            return contentLoader.GetChildren<PageData>(this.startPage.ContentLink)
                .FilterForDisplay();
        }
    }
}