﻿using EPiServer.XForms;
using EPiServer.XForms.Util;
using EPiServer.XForms.WebControls;
using LakeTrust.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Xml.Linq;

namespace LakeTrust.Business
{
    public class XFormDataFactory
    {
        public XFormDataFactory()
        { }

        public IList<XFormData> GetXFormData(Guid formId)
        {
            var form = XForm.CreateInstance(formId);
            return form.GetPostedData();
        }

        public PollResult GetPollResults(XFormControl xFormControl, XForm form)
        {
            IList<XFormData> data = form.GetPostedData();
            PollResult result = new PollResult();
            ControlCollection controls = xFormControl.Controls;
            result.TotalItems = form.GetPostedDataCount();
            foreach (var item in controls)
            {
                if (item is Select1)
                {
                    var select = item as Select1;
                    if (string.IsNullOrEmpty(result.Question))
                        result.Question = select.Label;
                    var options = select.Choices.Items;
                    if (options.Count > 0)
                    {
                        foreach (var option in options)
                        {
                            var value = data.Where(p => p.Data.InnerText == option.Value).Count();
                            var resultItem = new Result();
                            resultItem.Name = option.Value;
                            resultItem.Value = value;
                            resultItem.Percentage = Math.Round(((double)value / (double)result.TotalItems) * 100, 1);
                            result.Results.Add(resultItem);
                        }
                    }
                    break;
                }
            }

            return result;
        }

        public PollResult GetPollResults(XForm form)
        {
            IList<XFormData> data = form.GetPostedData();
            PollResult result = new PollResult();

            result.TotalItems = form.GetPostedDataCount();
            var question = string.Empty;
            var resultItems = ParseXmlDocumentFromForm(form.Document, out question);
            foreach (var item in resultItems)
            {
                var value = data.Where(p => p.Data.InnerText == item.XFormValue).Count();
                var resultItem = new Result();
                resultItem.Name = item.Name;
                resultItem.Value = value;
                resultItem.Percentage = Math.Round(((double)value / (double)result.TotalItems) * 100, 1);
                result.Results.Add(resultItem);
            }
            result.Question = question;
            return result;
        }

        private List<Result> ParseXmlDocumentFromForm(SerializableXmlDocument document, out string question)
        {
            var results = new List<Result>();
            XDocument doc = XDocument.Parse(document.OuterXml);
            var nodes = from p in doc.Descendants()
                        where p.Name.LocalName == "item"
                        select p;

            foreach (var node in nodes)
            {
                XNamespace xns = node.Name.Namespace;
                var result = new Result();
                result.Name = node.Elements(xns + "label").First().Value;
                result.XFormValue = node.Elements(xns + "value").First().Value;
                results.Add(result);
            }
            question = (from x in doc.Descendants()
                        where x.Name.LocalName == "label"
                        select x).First().Value;
            if (string.IsNullOrWhiteSpace(question))
                question = string.Empty;

            return results;
        }
    }
}