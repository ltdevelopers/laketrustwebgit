﻿using System;
using System.Collections;
using System.Web;

namespace LakeTrust.Business
{
    public class CacheHelper
    {
        public void Add<T>(string key, T obj)
        {
            HttpContext.Current.Cache.Insert(
                key,
                obj,
                null,
                DateTime.Now.AddMinutes(60),
                System.Web.Caching.Cache.NoSlidingExpiration);
        }

        public void Add<T>(string key, T obj, int Minutes)
        {
            HttpContext.Current.Cache.Insert(
                key,
                obj,
                null,
                DateTime.Now.AddMinutes(Minutes),
                System.Web.Caching.Cache.NoSlidingExpiration);
        }

        public void Clear(string key)
        {
            //TODO remove this and use the episerver cache
            if (Exists(key))
            {
                HttpContext.Current.Cache.Remove(key);
            }
        }

        public bool Exists(string key)
        {
            if (HttpContext.Current != null)
            {
                if (HttpContext.Current.Cache != null)
                {
                    return HttpContext.Current.Cache[key] != null;
                }
            }

            return false;
        }

        public bool Get<T>(string key, out T value)
        {
            try
            {
                if (!Exists(key))
                {
                    value = default(T);
                    return false;
                }

                value = (T)HttpContext.Current.Cache[key];
            }
            catch
            {
                value = default(T);
                return false;
            }

            return true;
        }

        public void ClearAllCache()
        {
            foreach (DictionaryEntry dEntry in HttpContext.Current.Cache)
            {
                HttpContext.Current.Cache.Remove(dEntry.Key.ToString());
            }
        }
    }
}