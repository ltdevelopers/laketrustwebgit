﻿using EPiServer.Editor.TinyMCE;

namespace LakeTrust.Business.Plugins
{
    [TinyMCEPluginNonVisual(AlwaysEnabled = true, PlugInName = "LakeTrustPlugins", EditorInitConfigurationOptions = "{ valid_elements: '*[*]', browser_spellcheck : true, gecko_spellcheck: true}")]
    public class ExtendedValidElements
    {
    }
}