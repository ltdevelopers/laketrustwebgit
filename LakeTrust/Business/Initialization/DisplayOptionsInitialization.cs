﻿using EPiServer.Framework;
using EPiServer.Framework.Initialization;
using EPiServer.ServiceLocation;
using EPiServer.Web;
using System;
using System.Linq;

namespace LakeTrust.Business.Initialization
{
    [InitializableModule]
    [ModuleDependency(typeof(EPiServer.Web.InitializationModule))]
    public class DisplayOptionsInitialization : IInitializableModule
    {
        public void Initialize(InitializationEngine context)
        {
            var options = ServiceLocator.Current.GetInstance<DisplayOptions>();
            options
                .Add("full", "Full Width", Global.BootstrapTags.FullWidth, "", "epi-icon__layout--full")
                .Add("twothird", "Two Thirds Width", Global.BootstrapTags.TwoThirdWidth, "", "epi-icon__layout--two-thirds")
                .Add("half", "Half Width", Global.BootstrapTags.HalfWidth, "", "epi-icon__layout--half")
                .Add("onethird", "One Third Width", Global.BootstrapTags.OneThirdWidth, "", "epi-icon__layout--one-third");
        }

        public void Preload(string[] parameters)
        {
        }

        public void Uninitialize(InitializationEngine context)
        {
            //Add uninitialization logic
        }
    }
}