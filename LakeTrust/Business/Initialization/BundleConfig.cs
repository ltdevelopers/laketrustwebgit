﻿using EPiServer.Framework;
using EPiServer.Framework.Initialization;
using System.Web.Optimization;

namespace LakeTrust.Business.Initialization
{
    [InitializableModule]
    public class BundleConfig : IInitializableModule
    {
        public void Initialize(InitializationEngine context)
        {
            if (context.HostType == HostType.WebApplication)
            {
                RegisterBundles(BundleTable.Bundles);
            }
        }

        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.IgnoreList.Clear();

            bundles.Add(new ScriptBundle("~/Scripts/jquery", "https://ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js")
                .Include("~/Scripts/libs/jquery-1.8.0.min.js"));

            bundles.Add(new ScriptBundle("~/Scripts/Modernizr")
                .Include("~/Scripts/modernizr-1.7.min.js"));

            #region LakeTrust Styles and Scripts

            bundles.Add(new ScriptBundle("~/Scripts/IE")
                .Include("~/Scripts/html5shiv.js")
                .Include("~/Scripts/html5shiv-printshiv.js")
                .Include("~/Scripts/respond.js")
                .Include("~/Scripts/matchmedia.polyfill.js"));

            bundles.Add(new ScriptBundle("~/Scripts/LakeTrust")
                .Include("~/Scripts/libs/bootstrap3/bootstrap.js")
                .Include("~/Scripts/libs/jquery.hoverIntent.minified.js")
                .Include("~/Scripts/libs/jquery.dcmegamenu.1.3.3.js")
                .Include("~/Scripts/libs/responsiveslides.js")
                .Include("~/Scripts/plugins.js")
                .Include("~/Scripts/script.js"));

            bundles.Add(new StyleBundle("~/Content/LakeTrust")
                .Include("~/Content/bootstrap3/bootstrap.css")
                .Include("~/Content/laketrust.css"));

            #endregion LakeTrust Styles and Scripts

            #region HQ Styles and Scripts

            bundles.Add(new StyleBundle("~/Content/LakeTrustHQ")
                .Include("~/Content/HQ/bootstrap3/css/bootstrap.css")
                .Include("~/Content/HQ/bootstrap3/css/bootstrap-theme.css")
                .Include("~/Content/hq.css"));

            bundles.Add(new ScriptBundle("~/Scripts/LakeTrustHQ")
                .Include("~/Content/HQ/bootstrap3/js/bootstrap.js")
                .Include("~/Scripts/libs/jquery.equal-heights.js")
                .Include("~/Scripts/libs/responsiveslides.js")
                .Include("~/Scripts/hq.js"));

            #endregion HQ Styles and Scripts
        }

        public void Uninitialize(InitializationEngine context)
        {
        }

        public void Preload(string[] parameters)
        {
        }
    }
}