﻿using EPiServer;
using EPiServer.Core;
using EPiServer.Data;
using EPiServer.Framework;
using EPiServer.Framework.Initialization;
using EPiServer.ServiceLocation;
using EPiServer.Web;
using EPiServer.Web.Routing;
using System;
using System.Linq;
using System.Web;
using System.Web.Caching;
using System.Web.Routing;

namespace LakeTrust.Business.Initialization
{
    /// <summary>
    /// Automatically redirects old VPP paths registered by the migration tool to new media paths
    /// </summary>
    [ModuleDependency(typeof(EPiServer.Web.InitializationModule))]
    public class VppToContentRedirect : RouteBase, IInitializableModule
    {
        private string[] RootPaths = new string[] { "~/Global/", "~/PageFiles/", "~/Documents/" };

        public override RouteData GetRouteData(HttpContextBase httpContext)
        {
            if (httpContext == null || !VirtualPathUtilityEx.IsValidVirtualPath(httpContext.Request.Path))
            {
                return null;
            }
            var appRelativePath = VirtualPathUtility.ToAppRelative(httpContext.Request.Path);
            if (!RootPaths.Any(p => appRelativePath.StartsWith(p, StringComparison.OrdinalIgnoreCase)))
            {
                return null;
            }
            return new RouteData(this, new VppRedirectRouteHandler());
        }

        public override VirtualPathData GetVirtualPath(RequestContext requestContext, RouteValueDictionary values)
        {
            return null;
        }

        private class VppRedirectRouteHandler : IRouteHandler
        {
            public IHttpHandler GetHttpHandler(RequestContext requestContext)
            {
                return new VppRedirectHttpHandler();
            }
        }

        private class VppRedirectHttpHandler : IHttpHandler
        {
            private const string CacheKeyPrefix = "VppToContentRedirect-";
            private const string MigrationToolLookupSql = "SELECT ContentLink FROM [dbo].[_MigratedVPPFiles] WHERE VirtualPath=@p0";

            public bool IsReusable
            {
                get { return true; }
            }

            public void ProcessRequest(HttpContext context)
            {
                var vppPath = context.Request.Path;

                var cacheKey = CacheKeyPrefix + vppPath;
                var newUrl = context.Cache[cacheKey] as string;
                if (newUrl == null)
                {
                    var db = ServiceLocator.Current.GetInstance<IDatabaseHandler>();
                    var contentLinkString = db.Execute(() =>
                    {
                        using (var cmd = db.CreateCommand(MigrationToolLookupSql, System.Data.CommandType.Text, vppPath))
                        {
                            return cmd.ExecuteScalar() as string;
                        }
                    });

                    if (String.IsNullOrEmpty(contentLinkString))
                    {
                        throw new HttpException(404, "Not Found");
                    }

                    ContentReference contentLink = new ContentReference(contentLinkString);
                    try
                    {
                        newUrl = ServiceLocator.Current.GetInstance<UrlResolver>().GetUrl(contentLink);
                    }
                    catch (ContentNotFoundException)
                    {
                    }

                    if (String.IsNullOrEmpty(newUrl))
                    {
                        throw new HttpException(404, "Not Found");
                    }
                    context.Cache.Insert(cacheKey, newUrl, DataFactoryCache.CreateDependency(contentLink), Cache.NoAbsoluteExpiration, TimeSpan.FromHours(1));
                }

                context.Response.RedirectPermanent(newUrl);
            }
        }

        public void Initialize(InitializationEngine context)
        {
            if (context.HostType == HostType.WebApplication)
            {
                RouteTable.Routes.Add(this);
            }
        }

        public void Preload(string[] parameters)
        { }

        public void Uninitialize(InitializationEngine context)
        { }
    }
}