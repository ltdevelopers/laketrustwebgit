﻿using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.Filters;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace LakeTrust.Business
{
    public class ContentLocator
    {
        private readonly IContentLoader contentLoader;
        private readonly IContentProviderManager providerManager;
        private readonly IPageCriteriaQueryService pageCriteriaQueryService;
        private readonly IContentTypeRepository contentTypeRepository;

        public ContentLocator(IContentLoader contentLoader, IContentProviderManager providerManager, IPageCriteriaQueryService pageCriteriaQueryService, IContentTypeRepository contentTypeRepository)
        {
            this.contentLoader = contentLoader;
            this.providerManager = providerManager;
            this.pageCriteriaQueryService = pageCriteriaQueryService;
            this.contentTypeRepository = contentTypeRepository;
        }

        public virtual IEnumerable<T> GetAll<T>(ContentReference rootLink) where T : PageData
        {
            var children = contentLoader.GetChildren<PageData>(rootLink);
            foreach (var child in children)
            {
                var childOfRequestedTyped = child as T;
                if (childOfRequestedTyped != null)
                {
                    yield return childOfRequestedTyped;
                }
                foreach (var descendant in GetAll<T>(child.ContentLink))
                {
                    yield return descendant;
                }
            }
        }

        /// <summary>
        /// Returns pages of a specific page type
        /// </summary>
        /// <param name="pageLink"></param>
        /// <param name="recursive"></param>
        /// <param name="pageTypeId">ID of the page type to filter by</param>
        /// <returns></returns>
        public IEnumerable<PageData> FindPagesByPageType(PageReference pageLink, bool recursive, int pageTypeId)
        {
            if (PageReference.IsNullOrEmpty(pageLink))
            {
                throw new ArgumentNullException("pageLink", "No page link specified, unable to find pages");
            }

            var pages = recursive
                        ? FindPagesByPageTypeRecursively(pageLink, pageTypeId)
                        : contentLoader.GetChildren<PageData>(pageLink);

            return pages;
        }

        // Type specified through page type ID
        private IEnumerable<PageData> FindPagesByPageTypeRecursively(PageReference pageLink, int pageTypeId)
        {
            var criteria = new PropertyCriteriaCollection
                               {
                                    new PropertyCriteria
                                    {
                                        Name = "PageTypeID",
                                        Type = PropertyDataType.PageType,
                                        Condition = CompareCondition.Equal,
                                        Value = pageTypeId.ToString(CultureInfo.InvariantCulture)
                                    }
                               };

            // Include content providers serving content beneath the page link specified for the search
            if (providerManager.ProviderMap.CustomProvidersExist)
            {
                var contentProvider = providerManager.ProviderMap.GetProvider(pageLink);

                if (contentProvider.HasCapability(ContentProviderCapabilities.Search))
                {
                    criteria.Add(new PropertyCriteria
                    {
                        Name = "EPI:MultipleSearch",
                        Value = contentProvider.ProviderKey
                    });
                }
            }

            return pageCriteriaQueryService.FindPagesWithCriteria(pageLink, criteria);
        }

        public IEnumerable<T> FindPagesByPageType<T>(ContentReference contentLink, bool recursive = false) where T : PageData
        {
            int contentTypeId = this.contentTypeRepository.Load(typeof(T)).ID;
            return this.FindPagesByPageType(contentLink.ToPageReference(), recursive, contentTypeId).Cast<T>();
        }
    }
}