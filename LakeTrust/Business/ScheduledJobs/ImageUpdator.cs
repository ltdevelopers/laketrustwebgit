﻿using EPiServer;
using EPiServer.BaseLibrary.Scheduling;
using EPiServer.Core;
using EPiServer.PlugIn;
using EPiServer.ServiceLocation;
using EPiServer.Web;
using EPiServer.Web.Routing;
using LakeTrust.Models.Pages;
using System;
using System.Linq;

namespace LakeTrust.Business.ScheduledJobs
{
    [ScheduledPlugIn(DisplayName = "Image Updator", SortIndex = 1)]
    public class ImageUpdator : JobBase
    {
        private bool _stopSignaled;
        private readonly IContentRepository contentRepository;
        private readonly IContentLoader contentLoader;
        private readonly ContentLocator contentLocator;
        private readonly UrlResolver urlResolver;
        private readonly SiteDefinition siteDefinition;

        public ImageUpdator()
        {
            IsStoppable = true;
            var current = ServiceLocator.Current;
            this.contentLoader = current.GetInstance<IContentLoader>();
            this.contentRepository = current.GetInstance<IContentRepository>();
            this.contentLocator = current.GetInstance<ContentLocator>();
            this.urlResolver = current.GetInstance<UrlResolver>();
            this.siteDefinition = current.GetInstance<SiteDefinition>();
        }

        /// <summary>
        /// Called when a user clicks on Stop for a manually started job, or when ASP.NET shuts down.
        /// </summary>
        public override void Stop()
        {
            _stopSignaled = true;
        }

        /// <summary>
        /// Called when a scheduled job executes
        /// </summary>
        /// <returns>A status message to be stored in the database log and visible from admin mode</returns>
        public override string Execute()
        {
            //Call OnStatusChanged to periodically notify progress of job for manually started jobs
            OnStatusChanged(String.Format("Starting execution of {0}", this.GetType()));

            var blogPages = this.contentLocator.FindPagesByPageType<BlogPostPage>(siteDefinition.StartPage, true);
            foreach (var blogPage in blogPages)
            {
                if (blogPage.BlogPostImage == null)
                    continue;

                UrlBuilder builder = new UrlBuilder(blogPage.BlogPostImage);
                var imageContentLink = PermanentLinkUtility.GetContentReference(builder);
                if (imageContentLink != null && !ContentReference.IsNullOrEmpty(imageContentLink))
                {
                    var clone = blogPage.CreateWritableClone() as BlogPostPage;
                    clone.PageImage = imageContentLink;
                    this.contentRepository.Save(clone, EPiServer.DataAccess.SaveAction.Publish);
                }
            }

            //For long running jobs periodically check if stop is signaled and if so stop execution
            if (_stopSignaled)
            {
                return "Stop of job was called";
            }

            return "Change to message that describes outcome of execution";
        }
    }
}