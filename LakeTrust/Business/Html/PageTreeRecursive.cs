﻿using EPiServer;
using EPiServer.Core;
using EPiServer.ServiceLocation;
using EPiServer.Web.Mvc.Html;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace LakeTrust.Business.Html
{
    public static class PageTreeRecursiveHelper
    {
        public static MvcHtmlString PageTreeRecursive(this HtmlHelper htmlHelper, ContentReference pageLink, string className = "", int maxDepth = 3)
        {
            return htmlHelper.PageTreeRecursive(pageLink.ToPageReference(), className, maxDepth);
        }

        public static MvcHtmlString PageTreeRecursive(this HtmlHelper htmlHelper, PageReference contentLink, string className = "", int maxDepth = 3)
        {
            StringBuilder html = new StringBuilder();

            var children = htmlHelper.GetContext().GetService<IContentLoader>().GetChildren<PageData>(contentLink).FilterForDisplay();
            html = PopulateChildren(htmlHelper, html, children, 1, className, maxDepth);

            return new MvcHtmlString(html.ToString());
        }

        private static StringBuilder PopulateChildren(HtmlHelper htmlHelper, StringBuilder html, IEnumerable<PageData> children, int depth, string className, int maxDepth)
        {
            if (!string.IsNullOrEmpty(className))
                html.AppendFormat("<ul class=\"depth-{0} {1}\">", depth, className);
            else
                html.AppendFormat("<ul class=\"depth-{0}\">", depth);

            foreach (PageData child in children)
            {
                if (depth <= maxDepth)
                {
                    html.Append("<li>");

                    html.Append(htmlHelper.PageLink(child));
                    var childsChildren = htmlHelper.GetContext().GetService<IContentLoader>().GetChildren<PageData>(child.ContentLink).FilterForDisplay();

                    if (childsChildren.Any())
                    {
                        PopulateChildren(htmlHelper, html, childsChildren, depth + 1, className, maxDepth);
                    }

                    html.Append("</li>");
                }
            }
            html.Append("</ul>");

            return html;
        }

        public static HttpContextBase GetContext(this HtmlHelper htmlHelper)
        {
            return htmlHelper.ViewContext.HttpContext;
        }

        public static T GetService<T>(this HttpContextBase httpContext, string key = null)
        {
            IServiceLocator serviceLocator = httpContext.GetServiceLocator();
            if (key == null)
            {
                return serviceLocator.GetInstance<T>();
            }
            return serviceLocator.GetInstance<T>(key);
        }

        public static IServiceLocator GetServiceLocator(this HttpContextBase httpContext)
        {
            IServiceLocator locator = null;
            if (httpContext != null)
            {
                locator = httpContext.Items["serviceLocator"] as IServiceLocator;
            }
            return (locator ?? ServiceLocator.Current);
        }
    }
}