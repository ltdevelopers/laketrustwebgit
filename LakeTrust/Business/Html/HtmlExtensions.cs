﻿using EPiServer.Editor;
using LakeTrust.Models.Blocks;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web.Mvc;

namespace LakeTrust.Business.Html
{
    public static class HtmlExtensions
    {
        public static ContentWrapper BeginContentWrapper(this HtmlHelper htmlHelper, string tag, object htmlAttributes = null)
        {
            var tagBuilder = new TagBuilder(tag);

            foreach (var htmlAttribute in GetHtmlAttributes(htmlAttributes))
                tagBuilder.Attributes.Add(htmlAttribute);

            htmlHelper.ViewContext.Writer.Write(tagBuilder.ToString(TagRenderMode.StartTag));

            return new ContentWrapper(htmlHelper.ViewContext, tagBuilder.TagName);
        }

        public static MvcHtmlString RenderBlockTitle(this HtmlHelper helper, SiteBlockData block)
        {
            if (string.IsNullOrWhiteSpace(block.BlockTitle))
                return MvcHtmlString.Empty;

            var customTag = !string.IsNullOrWhiteSpace(block.BlockTitleCustomTag) ? block.BlockTitleCustomTag : "h3";
            var tag = new TagBuilder(customTag);
            if (!string.IsNullOrEmpty(block.BlockTitleColor))
                tag.MergeAttribute("class", block.BlockTitleColor);
            if (PageEditing.PageIsInEditMode)
                tag.Attributes.Add("data-epi-property-name", "BlockTitle");
            tag.InnerHtml = block.BlockTitle;

            return new MvcHtmlString(tag.ToString());
        }

        public static ContentWrapper BlockTitle(this HtmlHelper htmlHelper, SiteBlockData block, object htmlAttributes = null)
        {
            if (string.IsNullOrWhiteSpace(block.BlockTitle))
                return new ContentWrapper();

            var attributes = GetHtmlAttributes(htmlAttributes);

            var tagBuilder = new TagBuilder(!string.IsNullOrWhiteSpace(block.BlockTitleCustomTag) ? block.BlockTitleCustomTag : "h3");

            if (!string.IsNullOrEmpty(block.BlockTitleColor))
                attributes.Add("class", block.BlockTitleColor);

            if (PageEditing.PageIsInEditMode)
                attributes.Add("data-epi-property-name", "BlockTitle");

            if (attributes.Count > 0)
            {
                foreach (var htmlAttribute in attributes)
                    tagBuilder.Attributes.Add(htmlAttribute);
            }

            htmlHelper.ViewContext.Writer.Write(tagBuilder.ToString(TagRenderMode.StartTag));
            return new ContentWrapper(htmlHelper.ViewContext, tagBuilder.TagName);
        }

        public static ContentWrapper BeginBlockWrapper(this HtmlHelper htmlHelper, SiteBlockData block, object htmlAttributes = null, string cssClasses = "")
        {
            List<string> classes = new List<string>();
            var tagBuilder = new TagBuilder("div");

            var attributes = GetHtmlAttributes(htmlAttributes);
            if (block.DisplayWithBorder)
                classes.Add("sb");

            if (block.DisplayWithFadeBackground)
                classes.Add("wb");

            if (!string.IsNullOrWhiteSpace(cssClasses))
            {
                foreach (var css in cssClasses.Split(' '))
                    classes.Add(css.Trim());
            }

            if (classes.Count > 0)
            {
                if (attributes.ContainsKey("class"))
                {
                    var classOne = attributes["class"];
                    foreach (var s in classOne.Split(' '))
                        classes.Add(s.Trim());

                    attributes["class"] = string.Join(" ", classes);
                }
                else
                    attributes.Add("class", string.Join(" ", classes));
            }
            foreach (var htmlAttribute in attributes)
                tagBuilder.Attributes.Add(htmlAttribute);

            htmlHelper.ViewContext.Writer.Write(tagBuilder.ToString(TagRenderMode.StartTag));

            return new ContentWrapper(htmlHelper.ViewContext, tagBuilder.TagName);
        }

        private static Dictionary<string, string> GetHtmlAttributes(object htmlAttributes)
        {
            Dictionary<string, string> entries = new Dictionary<string, string>();
            if (htmlAttributes != null)
            {
                foreach (PropertyDescriptor property in TypeDescriptor.GetProperties(htmlAttributes))
                {
                    var key = property.Name.Replace('_', '-');
                    var value = property.GetValue(htmlAttributes).ToString();
                    if (!entries.ContainsKey(key))
                        entries.Add(key, value);
                }
            }
            return entries;
        }
    }
}