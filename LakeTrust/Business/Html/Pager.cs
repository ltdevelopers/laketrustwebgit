﻿using EPiServer;
using EPiServer.Core;
using EPiServer.ServiceLocation;
using EPiServer.Web.Routing;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace LakeTrust.Business.Html
{
    public static class HtmlPager
    {
        // @Html.ContentLink(Request.RequestContext.GetContentLink(), new { pageIndex = 2,
        // action="paging", searchTerm = "joshua" },null)
        public static MvcHtmlString Pager(this HtmlHelper helper, ContentReference contentLink, int pageIndex, int totalPages, int pageWeight, string actionName = "paging")
        {
            var urlResolver = UrlResolver.Current;
            StringBuilder sb = new StringBuilder();

            var searchTerm = helper.ViewContext.RouteData.Values["Searchterm"];
            var pager = new CustomPager(actionName);
            var pagerPages = pager.CreatePages(pageWeight, totalPages, pageIndex);

            if (pagerPages.Where(x => x.IsPageNumber).Count() > 1)
            {
                sb.Append("<div class=\"pagination pagination-small\">");
                sb.Append("<ul class=\"pagination\">");
                foreach (var page in pagerPages)
                {
                    if (pageIndex == 1)
                    {
                        if (page.PageNum == "1")
                        {
                            sb.AppendFormat("<li class=\"disabled\"><span>{0}</span></li>", page.Title);
                            continue;
                        }
                    }
                    if (pageIndex == pager.TotalPages)
                    {
                        if (page.PageNum == pager.TotalPages.ToString())
                        {
                            sb.AppendFormat("<li class=\"disabled\"><span>{0}</span></li>", page.Title);
                            continue;
                        }
                    }

                    if (!string.IsNullOrEmpty(page.ActiveClass))
                        sb.AppendFormat("<li class=\"active\"><span>{0}</span></li>", page.PageNum);
                    else
                    {
                        sb.AppendFormat("<li><a href=\"{0}{1}/{2}{3}\">{4}</a></li>", urlResolver.GetUrl(contentLink), pager.ActionName.ToLowerInvariant(), page.PageNum, searchTerm != null ? "/" + searchTerm.ToString() : string.Empty, page.Title);
                    }
                }
                sb.Append("</ul>");
                sb.Append("</div>");
                return new MvcHtmlString(sb.ToString());
            }
            return MvcHtmlString.Empty;
        }

        public static MvcHtmlString SearchPager(this HtmlHelper helper, ContentReference contentLink, int pageIndex, int totalPages, int pageWeight)
        {
            UrlResolver urlResolver = ServiceLocator.Current.GetInstance<UrlResolver>();

            StringBuilder sb = new StringBuilder();

            var searchTerm = helper.ViewContext.RouteData.Values["Searchterm"];

            var pagerPages = new CustomPager().CreatePages(pageWeight, totalPages, pageIndex);
            if (pagerPages.Where(x => x.IsPageNumber).Count() > 1)
            {
                sb.Append("<div class=\"pagination pagination-small\">");
                sb.Append("<ul>");
                foreach (var page in pagerPages)
                {
                    if (!string.IsNullOrEmpty(page.ActiveClass))
                        sb.AppendFormat("<li class=\"active\"><a href=\"{0}{1}/{2}\">{3}</a></li>", urlResolver.GetUrl(contentLink), page.PageNum, searchTerm, page.Title);
                    else
                        sb.AppendFormat("<li><a href=\"{0}{1}/{2}\">{3}</a></li>", urlResolver.GetUrl(contentLink), page.PageNum, searchTerm, page.Title);
                }
                sb.Append("</ul>");
                sb.Append("</div>");
                return new MvcHtmlString(sb.ToString());
            }
            return MvcHtmlString.Empty;
        }

        public static IHtmlString BlendPager(this HtmlHelper helper, int pageSize = 10, int totalItems = 0, int pageIndex = 1, int visiblePages = 10, string activeClass = "active")
        {
            if (totalItems <= pageSize)
                return MvcHtmlString.Empty;

            var pager = new LakeTrust.Models.ViewModels.BlendPager();
            var pages = pager.CreatedPages(pageSize, totalItems, pageIndex, visiblePages);

            var currentContentLink = helper.ViewContext.RequestContext.GetContentLink();
            var url = UrlResolver.Current.GetUrl(currentContentLink);

            NameValueCollection collection = helper.ViewContext.HttpContext.Request.QueryString;
            foreach (string key in collection)
            {
                if (key != "p")
                    url = UriSupport.AddQueryString(url, key, Url.Encode(collection[key]));
            }

            StringBuilder builder = new StringBuilder("<ol class=\"pagination\">");

            foreach (LakeTrust.Models.ViewModels.PagerPage page in pages)
            {
                if (page.PageNum != 1)
                    url = UriSupport.AddQueryString(url, "p", page.PageNum.ToString());

                if (page.IsDisabled)
                {
                    builder.AppendFormat("<li class=\"disabled\"><span><span aria-hidden=\"true\">{0}</span></span></li>", page.Title);
                }
                else
                {
                    if (page.CurrentPage)
                    {
                        builder.AppendFormat("<li class=\"active\"><span>{0} <span class=\"sr-only\">(current)</span></span></li>", page.Title);
                    }
                    else
                    {
                        builder.AppendFormat("<li><a href=\"{0}\">{1}</a></li>", url, page.Title);
                    }
                }
            }
            builder.Append("</ol>");

            return new MvcHtmlString(builder.ToString());
        }

        private class CustomPager
        {
            public CustomPager()
            {
                this.ActionName = "paging";
                this.TotalPages = 1;
            }

            public CustomPager(string actionName)
                : this()
            {
                this.ActionName = actionName;
            }

            /// <summary>
            /// Creates a list of page numbers to be enumerated in a paging control.
            /// </summary>
            /// <remarks>Paging is 1-based, meaning that the first page is called page 1.</remarks>
            /// <param name="pageSize">Size of the page.</param>
            /// <param name="totalItems">The total items.</param>
            /// <param name="currentPage">The current page.</param>
            /// <returns></returns>
            public IList<PagerPage> CreatePages(int pageSize, int totalItems, int currentPage)
            {
                List<PagerPage> pages = new List<PagerPage>();
                this.TotalPages = (totalItems + pageSize - 1) / pageSize;
                int startIndex = 0;
                int endIndex = this.totalPages;

                if (this.totalPages > 10)
                {
                    startIndex = currentPage - 5;
                    endIndex = currentPage + 5;
                    if (startIndex < 0)
                    {
                        startIndex = 0;
                        endIndex = startIndex + 10;
                    }
                    if (endIndex > this.totalPages)
                    {
                        endIndex = this.totalPages;
                        startIndex = this.totalPages - 10;
                    }
                }

                pages.Add(new PagerPage { Title = "first", PageNum = "1", CurrentPage = false });

                if (currentPage == 1)
                    pages.Add(new PagerPage { Title = "prev", PageNum = (currentPage).ToString(), CurrentPage = false, IsPageNumber = false });
                else
                    pages.Add(new PagerPage { Title = "prev", PageNum = (currentPage - 1).ToString(), CurrentPage = false, IsPageNumber = false });

                for (int i = startIndex; i < endIndex; i++)
                {
                    pages.Add(new PagerPage
                    {
                        Title = (i + 1).ToString(),
                        PageNum = (i + 1).ToString(),
                        CurrentPage = i == (currentPage - 1),
                        ActiveClass = i == (currentPage - 1) ? "active" : string.Empty,
                        IsPageNumber = true
                    });
                }

                if (currentPage == this.totalPages)
                    pages.Add(new PagerPage { Title = "next", PageNum = (currentPage).ToString(), CurrentPage = false, IsPageNumber = false });
                else
                    pages.Add(new PagerPage { Title = "next", PageNum = (currentPage + 1).ToString(), CurrentPage = false, IsPageNumber = false });

                pages.Add(new PagerPage { Title = "last", PageNum = this.totalPages.ToString(), CurrentPage = false, IsPageNumber = false });

                return pages;
            }

            public string ActionName { get; set; }

            private int totalPages = 1;

            public int TotalPages
            {
                get { return totalPages; }
                private set { totalPages = value; }
            }
        }

        /// <summary>
        /// Page class containing the information used to create a paging control
        /// </summary>
        private class PagerPage
        {
            /// <summary>
            /// Gets or sets the title.
            /// </summary>
            /// <value>The title.</value>
            public string Title { get; set; }

            /// <summary>
            /// Gets or sets the page number.
            /// </summary>
            /// <value>The page num.</value>
            public string PageNum { get; set; }

            /// <summary>
            /// Gets or sets a value indicating whether this page is the current page.
            /// </summary>
            /// <value><c>true</c> if [current page]; otherwise, <c>false</c>.</value>
            public bool CurrentPage { get; set; }

            public string ActiveClass { get; set; }

            public bool IsPageNumber { get; set; }
        }
    }
}