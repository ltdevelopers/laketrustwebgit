﻿using EPiServer;
using EPiServer.Core;
using SpreadsheetGear;
using SpreadsheetGear.Data;
using System.Data;
using System.IO;
using System.Text;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.HtmlControls;

namespace LakeTrust.Business.Html
{
    public static class ExcelToTable
    {
        public static MvcHtmlString RenderTable(this HtmlHelper helper, ContentReference contentLink, string tableCaption = "")
        {
            var file = DataFactory.Instance.Get<MediaData>(contentLink);

            if (file.RouteSegment.Contains(".xls") || file.RouteSegment.Contains(".xlsx"))
            {
                IWorkbook workbook = Factory.GetWorkbook(contentLink.MediaAbsolutePath());
                DataSet dataSet = workbook.GetDataSet(GetDataFlags.FormattedText);
                if (dataSet.Tables.Count > 0)
                {
                    StringBuilder sb = new StringBuilder("<table class=\"table table-striped table-bordered\">");

                    var dt = dataSet.Tables[0];
                    if (!string.IsNullOrEmpty(tableCaption))
                    {
                        sb.AppendFormat("<caption>{0}</caption>", tableCaption);
                    }

                    if (dt.Columns.Count > 0)
                    {
                        sb.Append("<thead>");
                        foreach (DataColumn column in dt.Columns)
                        {
                            sb.AppendFormat("<th>{0}</th>", column.ColumnName);
                        }
                        sb.Append("</thead>");
                    }
                    if (dt.Rows.Count > 0)
                    {
                        sb.Append("<tbody>");
                        foreach (DataRow row in dt.Rows)
                        {
                            WriteRow(sb, row);
                        }
                        sb.Append("</tbody>");
                    }

                    sb.Append("</table>");

                    return new MvcHtmlString(sb.ToString());
                }
            }
            return MvcHtmlString.Empty;
        }

        private static void WriteRow(StringBuilder sb, DataRow row)
        {
            int spanCount = 0;
            sb.Append("<tr>");

            if (IsSpannedRow(row, out spanCount))
                sb.AppendFormat("<td colspan=\"{0}\">{1}</td>", spanCount + 1, row[0].ToString());
            else
                foreach (DataColumn column in row.Table.Columns)
                    sb.AppendFormat("<td>{0}</td>", row[column.ColumnName].ToString());

            sb.Append("</tr>");
        }

        private static bool IsSpannedRow(DataRow row, out int spanCount)
        {
            int columnCount = row.Table.Columns.Count;
            spanCount = 0;
            for (int i = 0; i <= columnCount - 1; i++)
            {
                var value = row[i].ToString();
                if (!string.IsNullOrEmpty(value))
                {
                    if (i != 0)
                        return false;
                }
                else
                {
                    if (i > 0)
                        spanCount++;
                }
            }
            return spanCount == columnCount - 1;
        }

        private static string WriteToHTML(HtmlTable table)
        {
            var sw = new StringWriter(new StringBuilder());
            var htw = new HtmlTextWriter(sw);

            table.RenderControl(htw);
            return sw.ToString();
        }
    }
}