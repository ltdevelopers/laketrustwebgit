﻿using System;
using System.IO;
using System.Linq;
using System.Web.Mvc;

namespace LakeTrust.Business.Html
{
    public class ContentWrapper : IDisposable
    {
        private readonly TextWriter writer;
        private string tag;

        public ContentWrapper()
        {
        }

        public ContentWrapper(ViewContext viewContext)
        {
            this.writer = viewContext.Writer;
        }

        public ContentWrapper(ViewContext viewContext, string tag)
            : this(viewContext)
        {
            this.tag = tag;
        }

        public void Dispose()
        {
            if (!string.IsNullOrWhiteSpace(this.tag))
                this.writer.Write(string.Format("</{0}>", this.tag));
        }
    }
}