﻿using EPiServer;
using EPiServer.Core;
using EPiServer.Filters;
using EPiServer.ServiceLocation;
using EPiServer.Web.Mvc.Html;
using EPiServer.Web.Routing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace LakeTrust.Business.Html
{
    public static class PageTreeHelper
    {
        public static MvcHtmlString PageTree(this HtmlHelper htmlHelper, ContentReference pageLink)
        {
            return htmlHelper.PageTree(pageLink.ToPageReference());
        }

        public static MvcHtmlString PageTree(this HtmlHelper htmlHelper, PageReference contentLink)
        {
            List<ContentReference> pageTreeTrail = new List<ContentReference>();
            var activeTrail = htmlHelper.GetActiveTrail();
            StringBuilder html = new StringBuilder();

            if (!activeTrail.Contains(contentLink))
                pageTreeTrail.Add(contentLink);
            else
                pageTreeTrail.AddRange(activeTrail.Skip(activeTrail.IndexOf(contentLink)));

            var children = new PageDataCollection(htmlHelper.GetContext().GetService<IContentLoader>().GetChildren<PageData>(pageTreeTrail.First()));
            children = new PageDataCollection(FilterForVisitor.Filter(children).Where(p => p.VisibleInMenu));
            if (children.Count > 0)
                html = PopulateChildren(htmlHelper, html, children, pageTreeTrail, 1);

            return new MvcHtmlString(html.ToString());
        }

        internal static StringBuilder PopulateChildren(HtmlHelper htmlHelper, StringBuilder html, IEnumerable<PageData> children, IEnumerable<ContentReference> references, int depth)
        {
            html.AppendFormat("<ul class=\"depth-{0}\">", depth);
            foreach (PageData child in children)
            {
                html.Append("<li>");
                if (references.Contains(child.ContentLink))
                {
                    bool isLast = references.Last() == child.ContentLink;
                    if (isLast)
                    {
                        if (depth > 1)
                        {
                            html.AppendFormat("<span class\"active expanded\">{0}</span>", child.Name);
                        }
                        else
                        {
                            html.Append(htmlHelper.PageLink(child.Name, child.ContentLink.ToPageReference(), null, new { @class = "active expanded" }));
                        }
                    }
                    else
                    {
                        html.Append(htmlHelper.PageLink(child.Name, child.ContentLink.ToPageReference(), null, new { @class = "parent" }));
                    }
                    var childsChildren = new PageDataCollection(htmlHelper.GetContext().GetService<IContentLoader>().GetChildren<PageData>(child.ContentLink));
                    childsChildren = new PageDataCollection(FilterForVisitor.Filter(childsChildren).Where(p => p.VisibleInMenu));

                    if (childsChildren.Any())
                        PopulateChildren(htmlHelper, html, childsChildren, references, depth + 1);
                }
                else
                    html.Append(htmlHelper.PageLink(child.GetPropertyValue<string>("Title", child.Name), child.ContentLink.ToPageReference()));

                html.Append("</li>");
            }
            html.Append("</ul>");

            return html;
        }

        internal static IList<ContentReference> GetActiveTrail(this HtmlHelper htmlHelper)
        {
            var currentContentReference = htmlHelper.GetContext().Request.RequestContext.GetContentLink();
            var contentLoader = ServiceLocator.Current.GetInstance<IContentLoader>();
            var items = contentLoader.GetAncestors(currentContentReference)
                .Select(p => p.ContentLink)
                .Where(p => !p.CompareToIgnoreWorkID(PageReference.RootPage))
                .Reverse()
                .ToList();
            items.Add(currentContentReference);
            return items;
        }

        public static IEnumerable<IContent> GetSelfAndAncestors(this IContentLoader structure, IContent rootContent, IContent content, string cultureName = "en")
        {
            if (content == null || rootContent == null)
                yield break;

            while (true)
            {
                yield return content;
                if (ContentReference.IsNullOrEmpty(content.ParentLink) || rootContent.ContentLink.CompareToIgnoreWorkID(content.ContentLink))
                {
                    yield break;
                }
                content = structure.Get<IContent>(content.ParentLink, LanguageSelector.Fallback(cultureName, true));
            }
        }
    }
}