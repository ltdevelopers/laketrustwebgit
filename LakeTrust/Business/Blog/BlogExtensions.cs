﻿using EPiServer;
using EPiServer.Core;
using EPiServer.Personalization;
using EPiServer.ServiceLocation;
using EPiServer.Web.Routing;
using LakeTrust.Business.Services;
using LakeTrust.Models.Pages;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LakeTrust.Business.Blog
{
    public static class BlogExtensions
    {
        public static string Author(this string username)
        {
            var profile = EPiServerProfile.GetProfiles(username).FirstOrDefault();
            if (profile != null)
            {
                var name = string.Format("{0} {1}", profile.FirstName, profile.LastName);
                if (!string.IsNullOrWhiteSpace(name.Trim()))
                    return name;
                return username;
            }
            return username;
        }

        public static string TagLinks(this BlogPostPage post)
        {
            var blogService = ServiceLocator.Current.GetInstance<BlogService>();
            var urlResolver = ServiceLocator.Current.GetInstance<UrlResolver>();
            var blogPage = DataFactory.Instance.Get<BlogPage>(post.GetPropertyValue<PageReference>(Global.StringConstants.BlogStartReference));
            if (string.IsNullOrWhiteSpace(post.Tags))
                return string.Empty;

            var tags = post.Tags.Split(',');
            var list = new List<string>();
            foreach (var tag in tags)
                list.Add(string.Format("<a href=\"{0}paging/1/{1}\">{1}</a>", urlResolver.GetUrl(blogPage.ContentLink), blogService.ToTitleCase(tag).Trim()));

            return string.Join(", ", list);
        }
    }
}