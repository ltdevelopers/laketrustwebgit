﻿using EPiServer;
using EPiServer.Core;
using EPiServer.ServiceLocation;
using EPiServer.Web;
using EPiServer.Web.Routing;
using LakeTrust.Business.Services;
using LakeTrust.Models.Pages;
using System;
using System.Linq;
using System.Text;
using System.Web;
using System.Xml;

namespace LakeTrust.Business.Blog.Handlers
{
    public class BlogRssFeed : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            var blogService = ServiceLocator.Current.GetInstance<BlogService>();
            var urlResolver = ServiceLocator.Current.GetInstance<UrlResolver>();
            // Clear any previous output from the buffer
            context.Response.Clear();
            context.Response.ContentEncoding = Encoding.UTF8;
            context.Response.ContentType = "text/xml";
            XmlTextWriter feedWriter = new XmlTextWriter(context.Response.OutputStream, Encoding.UTF8);

            feedWriter.WriteStartDocument();

            // These are RSS Tags
            feedWriter.WriteStartElement("rss");
            feedWriter.WriteAttributeString("version", "2.0");

            feedWriter.WriteStartElement("channel");
            feedWriter.WriteElementString("title", "Lake Trust Credit Union");
            feedWriter.WriteElementString("link", SiteDefinition.Current.SiteUrl.ToString());
            feedWriter.WriteElementString("description", "The CU Scoop");
            feedWriter.WriteElementString("copyright", string.Format("Copyright {0} Lake Trust Credit Union. All rights reserved.", DateTime.Now.Year));

            var blogRoll = blogService.GetBlogPosts(PageReference.StartPage).Take(20);
            foreach (BlogPostPage post in blogRoll)
            {
                feedWriter.WriteStartElement("item");
                feedWriter.WriteElementString("title", post.Title);
                feedWriter.WriteElementString("description", post.Teaser.ToHtmlString());

                UrlBuilder ub = new UrlBuilder(UriSupport.Combine(UriSupport.SiteUrl.ToString(), urlResolver.GetUrl(post.ContentLink)));
                EPiServer.Global.UrlRewriteProvider.ConvertToExternal(ub, post.PageLink, System.Text.UTF8Encoding.UTF8);

                feedWriter.WriteElementString("link", ub.ToString());
                feedWriter.WriteElementString("pubDate", post.StartPublish.ToString("o"));
                feedWriter.WriteElementString("guid", post.ContentGuid.ToString());
                feedWriter.WriteEndElement();
            }

            // Close all open tags tags
            feedWriter.WriteEndElement();
            feedWriter.WriteEndElement();
            feedWriter.WriteEndDocument();
            feedWriter.Flush();
            feedWriter.Close();
            context.Response.End();
        }

        public bool IsReusable { get { throw new NotImplementedException(); } }
    }
}