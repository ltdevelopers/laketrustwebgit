﻿using EPiServer;
using EPiServer.Web;
using System.Text;
using System.Text.RegularExpressions;

namespace LakeTrust.Business.Blog
{
    public class BlogUrlRewriter : HierarchicalUrlRewriteProvider
    {
        // The regexp to match a paged url
        private string _regexpPaging = @"(.+)/p/([0-9]+)/([^/]*)/";

        public override bool TryConvertToInternal(UrlBuilder url, out System.Globalization.CultureInfo preferredCulture, out object internalObject)
        {
            if (url == null)
            {
                internalObject = null;
                preferredCulture = System.Globalization.CultureInfo.CurrentCulture;
                return false;
            }
            Match match = Regex.Match(url.Path, _regexpPaging);

            if (match.Length > 0)
            {
                url.Path = match.Groups[1].Value + "/";
                url.QueryCollection["pageIndex"] = match.Groups[2].Value;
                url.QueryCollection["searchTerm"] = match.Groups[3] == null ? "0" : match.Groups[3].Value;

                return base.TryConvertToInternal(url, out  preferredCulture, out internalObject);
            }

            return base.TryConvertToInternal(url, out preferredCulture, out internalObject);
        }

        protected override bool ConvertToExternalInternal(UrlBuilder url, object internalObject, Encoding toEncoding)
        {
            // First let EPiServer convert the internal URL to an external. This will give us a URL
            // like: /Products/?dqcPagingId=5 (if it is a paged page)
            bool isRewritten = base.ConvertToExternalInternal(url, internalObject, toEncoding);

            // Check if the URLs query string contains dqcPagingId If it does we add /page/{Id} to
            // the URL and removes the query string
            if (url.Query.Contains("pageIndex"))
            {
                url.Path = string.Concat(url.Path, "p/", url.QueryCollection["pageIndex"], "/");
                url.QueryCollection.Remove("pageIndex");
            }

            if (url.Query.Contains("searchTerm"))
            {
                string categoryId = url.QueryCollection["searchTerm"];
                if (!string.IsNullOrEmpty(categoryId))
                    url.Path = string.Concat(url.Path, categoryId, "/");
                else
                    url.Path = string.Concat(url.Path, "all/");

                url.QueryCollection.Remove("searchTerm");
            }

            return isRewritten;
        }

        public override bool ConvertToInternal(UrlBuilder url, out object internalObject)
        {
            // If the URL end on /page/{id}/, bypass cache by calling ConvertToInternalInternal A
            // more optimal solution would be to perform some kind of caching here
            if (Regex.IsMatch(url.Path, _regexpPaging))
            {
                internalObject = null;
                System.Globalization.CultureInfo info;
                return this.TryConvertToInternal(url, out info, out internalObject);
            }

            // Else, it is ok to use the cached result
            return base.ConvertToInternal(url, out internalObject);
        }
    }
}