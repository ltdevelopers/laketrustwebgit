﻿using EPiServer;
using EPiServer.Core;
using EPiServer.Core.Html;
using EPiServer.DataAbstraction;
using EPiServer.DataAccess;
using EPiServer.Filters;
using EPiServer.PlugIn;
using EPiServer.Security;
using EPiServer.ServiceLocation;
using EPiServer.Web;
using HtmlAgilityPack;
using LakeTrust.Models.Pages;
using System;
using System.Linq;
using System.Web;

namespace LakeTrust.Business.Plugins
{
    [PagePlugIn(Description = "Used to place pages under the blog and organize them in a month / year calendar", DisplayName = "Blog Post Organizer")]
    public class BlogPostOrganizer
    {
        #region Properties

        private static readonly string ContainerPageType = "BlogContainerPage";
        private static readonly string ValidPageType = "BlogPostPage";

        #endregion Properties

        private static PageReference CreateContainerPage(PageReference parentPage, DateTime startPublish, bool yearContainer)
        {
            BlogContainerPage model = contentRepository.Service.GetDefault<BlogContainerPage>(parentPage, contentTypeRepository.Service.Load(ContainerPageType).ID);
            model.Name = yearContainer ? startPublish.ToString("yyyy") : startPublish.ToString("MMMM yyyy");
            model.Title = model.Name;
            model.StartPublish = yearContainer ? new DateTime(startPublish.Year, 1, 1) : new DateTime(startPublish.Year, startPublish.Month, 1);
            model.SetValue("PageChildOrderRule", (int)FilterSortOrder.PublishedDescending);
            model.URLSegment = UrlSegment.CreateUrlSegment(model);
            return contentRepository.Service.Save(model, SaveAction.Publish, AccessLevel.NoAccess).ToPageReference();
        }

        private static PageReference FindContainerPage(PageReference reference, DateTime date, bool yearContainer)
        {
            var children = DataFactory.Instance.GetChildren(reference);
            string name = string.Empty;
            foreach (PageData data in children)
            {
                if (yearContainer)
                {
                    if (data.StartPublish.Year == date.Year)
                        return data.PageLink;
                }
                else
                {
                    if (data.StartPublish.Year == date.Year && data.StartPublish.Month == date.Month)
                        return data.PageLink;
                }
            }

            return CreateContainerPage(reference, date, yearContainer);
        }

        private static bool IsValidPage(PageData page)
        {
            return page != null && page.PageTypeName == ValidPageType;
        }

        private static void ProcessFile(PageData data)
        {
            try
            {
                var parent = DataFactory.Instance.GetPage(data.ParentLink);
                while (parent.PageTypeName != "BlogPage" && parent.ContentLink != PageReference.StartPage)
                {
                    parent = DataFactory.Instance.GetPage(parent.ParentLink);
                }

                var yearReference = FindContainerPage(parent.ContentLink.ToPageReference(), data.StartPublish, true);
                var monthReference = FindContainerPage(yearReference, data.StartPublish, false);

                if (data.PageLink != PageReference.EmptyReference)
                    DataFactory.Instance.Move(data.PageLink, monthReference);
                else
                    data.ParentLink = monthReference;
            }
            catch (Exception)
            {
            }
        }

        private static void CheckTagRoot()
        {
            if (Category.Find(Global.StringConstants.BlogCategoryRoot) == null)
            {
                Category root = Category.GetRoot();
                Category category = new Category(Global.StringConstants.BlogCategoryRoot, Global.StringConstants.BlogCategoryRoot)
                {
                    Available = true,
                    Selectable = false
                };
                root.Categories.Add(category);
                category.Save();
            }
        }

        #region IInitializableModule Members

        public static void Initialize(int optionFlag)
        {
            if (HttpContext.Current != null)
            {
                DataFactory.Instance.PublishingPage += PublishingPage;
                DataFactory.Instance.CreatedPage += Instance_CreatedPage;
            }
        }

        private static void Instance_CreatedPage(object sender, PageEventArgs e)
        {
            if (e.Page.PageTypeName == contentTypeRepository.Service.Load(typeof(BlogPage)).Name)
            {
                DynamicProperty dynamicProperty = DynamicProperty.Load(e.PageLink, Global.StringConstants.BlogStartReference);
                dynamicProperty.PropertyValue.Value = e.PageLink;
                dynamicProperty.Save();
            }
        }

        private static void PublishingPage(object sender, PageEventArgs e)
        {
            if (!(IsValidPage(e.Page)))
                return;

            // CheckTagRoot();

            ProcessFile(e.Page);

            if (e.Page["MainBody"] != null)
            {
                var html = new HtmlDocument();
                var internalHtml = e.Page.GetPropertyValue<XhtmlString>("MainBody", new XhtmlString()).ToHtmlString();
                html.LoadHtml(internalHtml);
                var images = html.DocumentNode.Descendants("img")
                                .Select(x => x.GetAttributeValue("src", null))
                                .Where(s => !String.IsNullOrEmpty(s));
                var image = html.DocumentNode.Descendants("img").FirstOrDefault();

                if (image != null)
                {
                    var imageSrc = image.GetAttributeValue("src", null);
                    if (!string.IsNullOrEmpty(imageSrc))
                    {
                        e.Page.SetValue("BlogPostImage", new Url(imageSrc));
                        if (image.GetAttributeValue("alt", null) != null)
                        {
                            e.Page.SetValue("BlogPostImageAltText", image.GetAttributeValue("alt", string.Empty));
                        }
                    }
                }
                else
                {
                    e.Page.SetValue("BlogPostImageAltText", null);
                    e.Page.SetValue("BlogPostImage", null);
                }
                if (e.Page["Teaser"] == null)
                    e.Page.SetValue("Teaser", new XhtmlString(TextIndexer.StripHtml(internalHtml, 250)));
            }
        }

        #endregion IInitializableModule Members

        private static Injected<IContentRepository> contentRepository { get; set; }

        private static Injected<IContentTypeRepository> contentTypeRepository { get; set; }
    }
}