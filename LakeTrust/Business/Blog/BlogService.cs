﻿using EPiServer;
using EPiServer.Core;
using EPiServer.Filters;
using EPiServer.Web;
using EPiServer.Web.Routing;
using LakeTrust.Business.Blog;
using LakeTrust.Models.Pages;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace LakeTrust.Business.Services
{
    public class BlogService
    {
        private readonly IContentLoader contentLoader;
        private readonly ContentLocator contentLocator;
        private readonly UrlResolver urlResolver;
        private readonly SiteDefinition siteDefinition;
        private readonly CacheHelper cacheHelper;

        public BlogService(IContentLoader contentLoader, ContentLocator contentLocator, UrlResolver urlResolver, SiteDefinition siteDefinition, CacheHelper cacheHelper)
        {
            this.contentLoader = contentLoader;
            this.contentLocator = contentLocator;
            this.urlResolver = urlResolver;
            this.siteDefinition = siteDefinition;
            this.cacheHelper = cacheHelper;
        }

        /// <summary>
        /// Returns all the pages under the current page of type blogpostmodel
        /// </summary>
        /// <param name="contentReference">
        /// The page you would like to retrieve descendants of type type blogpostmodel
        /// </param>
        /// <returns>EPiServer.Core.PageDataCollection</returns>
        public IEnumerable<BlogPostPage> GetBlogPosts(ContentReference contentLink)
        {
            return this.GetBlogPages(contentLink);
        }

        public PageDataCollection GetBlogPosts(ContentReference contentLink, out int totalItems, string filterCategory = "all", int currentPageNumber = 1, int pageWeight = 10)
        {
            totalItems = 0;
            var categories = EPiServer.DataAbstraction.Category.Find(Global.StringConstants.BlogCategoryRoot).Categories;
            var blogPosts = new PageDataCollection(this.GetBlogPages(contentLink));

            if (filterCategory != "all")
            {
                new FilterValueInArray("Tags", filterCategory).Filter(blogPosts);
            }

            totalItems = blogPosts.Count();

            new FilterSkipCount((currentPageNumber * pageWeight) - pageWeight).Filter(blogPosts);
            new FilterCount(pageWeight).Filter(blogPosts);
            return blogPosts;
        }

        public List<EPiServer.DataAbstraction.Category> GetBlogPageCategory(BlogPostPage model)
        {
            var categories = new List<EPiServer.DataAbstraction.Category>();
            foreach (int categoryId in model.Category)
            {
                var category = EPiServer.DataAbstraction.Category.Find(categoryId);
                if (category != null)
                    categories.Add(category);
            }
            return categories;
        }

        public IEnumerable<string> GetBlogPageCategoryNames(BlogPostPage model)
        {
            foreach (EPiServer.DataAbstraction.Category category in GetBlogPageCategory(model))
                yield return this.ToTitleCase(category.Name);
        }

        public IEnumerable<PageData> GetMostPopularBlogPost(ContentReference blogPage, int takeCount = 3)
        {
            var views = BlogPostView.GetBlogPosts(this.contentLoader.Get<PageData>(blogPage).ContentGuid);
            var mostPopulars = views.OrderByDescending(x => x.Views).Take(takeCount);
            foreach (var popular in mostPopulars)
                yield return this.contentLoader.Get<PageData>(popular.PageId);
        }

        public IEnumerable<string> BlogCategories(PageReference contentLink)
        {
            var blogPosts = this.GetBlogPosts(contentLink);
            var list = new List<string>();
            foreach (var item in blogPosts)
            {
                if (item["Tags"] != null)
                {
                    var tags = item.GetPropertyValue<string>("Tags").Split(',');
                    foreach (var tag in tags)
                        if (!list.Contains(this.ToTitleCase(tag).Trim()))
                            list.Add(this.ToTitleCase(tag).Trim());
                }
            }
            return list;
        }

        private IEnumerable<BlogPostPage> GetBlogPages(ContentReference contentLink)
        {
            return this.contentLocator.FindPagesByPageType<BlogPostPage>(contentLink, true)
                   .FilterForDisplay(true, true)
                   .OrderByDescending(x => x.StartPublish)
                   .Cast<BlogPostPage>();
        }

        public string ToTitleCase(string str)
        {
            return CultureInfo.CurrentCulture.TextInfo.ToTitleCase(str.ToLower());
        }
    }
}