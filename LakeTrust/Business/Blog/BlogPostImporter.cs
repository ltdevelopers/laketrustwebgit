﻿using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.PlugIn;
using EPiServer.Security;
using EPiServer.ServiceLocation;
using EPiServer.Web;
using LakeTrust.Business.ScheduledJobs;
using LakeTrust.Models.Pages;
using System;
using System.Globalization;
using System.Linq;

namespace LakeTrust.Business.Blog
{
    [ScheduledPlugIn(DisplayName = "Blog Post Importer")]
    public class BlogPostImporter : ScheduledJobBase
    {
        public BlogPostImporter()
        {
        }

        /// <summary>
        /// Starts the job
        /// </summary>
        /// <returns>A status message that will be logged</returns>
        public override string Execute()
        {
            this.InitializeCounters("Blog Post Count");
            var posts = Blog.GetBlogPosts();
            var startPage = contentRepository.Service.Get<StartPage>(SiteDefinition.Current.StartPage);
            var blogRoot = startPage.GetPropertyValue<ContentReference>("BlogStartReference");

            var blogCategories = Category.Find("Blog Root").Categories;
            foreach (Blog.BlogPost blogPost in posts)
            {
                switch (blogPost.Author)
                {
                    case "laketrust":
                        PrincipalInfo.CurrentPrincipal = PrincipalInfo.CreatePrincipal("bwoods");
                        break;

                    case "loricies":
                        PrincipalInfo.CurrentPrincipal = PrincipalInfo.CreatePrincipal("loricies");
                        break;

                    case "admin":
                        PrincipalInfo.CurrentPrincipal = PrincipalInfo.CreatePrincipal("laketrust");
                        break;
                }

                var categories = new CategoryList();
                BlogPostModel model = contentRepository.Service.GetDefault<BlogPostModel>(blogRoot, contentTypeRepository.Service.Load(typeof(BlogPostModel)).ID);
                string title = string.IsNullOrEmpty(blogPost.Title) ? "Untitled" : blogPost.Title;
                if (title.Length > 255)
                {
                    title = title.Substring(0, 254);
                }
                model.Name = title;
                model.Title = title;
                model.StartPublish = blogPost.PostDate;
                model.Created = blogPost.PostDate;
                model.MainBody = new XhtmlString(blogPost.Content);
                model.Teaser = new XhtmlString(blogPost.Content.CleanBody(300, true) + "...");

                model.CommentsClosed = true;
                model.ImportPostId = blogPost.PostId;

                foreach (string s in blogPost.Categories)
                    foreach (Category category in blogCategories)
                        if (category.Name == CultureInfo.CurrentCulture.TextInfo.ToTitleCase(s))
                            categories.Add(category.ID);

                if (categories.Count > 0)
                    model.Category = categories;

                var blogContentReference = this.contentRepository.Service.Save(model, EPiServer.DataAccess.SaveAction.Publish);
                this.Increment("Blog Post Count");
                this.OnStatusChanged(this.CounterReport());
            }
            return this.CounterReport();
        }

        private Injected<IContentRepository> contentRepository { get; set; }

        private Injected<IContentTypeRepository> contentTypeRepository { get; set; }
    }
}