﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Xml.Linq;

namespace LakeTrust.Business.Blog
{
    public class Blog
    {
        public static List<BlogPost> GetBlogPosts()
        {
            var path = string.Format("{0}laketrustposts.xml", ConfigurationManager.AppSettings["BlogImportUrl"]);
            XNamespace content = "http://purl.org/rss/1.0/modules/content/";
            XNamespace dc = "http://purl.org/dc/elements/1.1/";
            XNamespace wp = "http://wordpress.org/export/1.1/";
            XDocument doc = XDocument.Load(path);
            var posts = new List<BlogPost>();
            var els = doc.Root.Element("channel").Elements("item");
            foreach (XElement element in els.Where(p => p.Element(wp + "post_type").Value == "post"))
            {
                posts.Add(new BlogPost(element, content, dc, wp));
            }

            return posts;
        }

        public class BlogPost
        {
            public BlogPost(XElement element, XNamespace content, XNamespace dc, XNamespace wp)
            {
                this.PostId = int.Parse(element.Element(wp + "post_id").Value);
                this.Categories = new List<string>();
                this.PostDate = Convert.ToDateTime(element.Element("pubDate").Value);
                this.Title = element.Element("title").Value;
                this.Author = element.Element(dc + "creator").Value;
                this.Content = element.Element(content + "encoded").Value;
                this.ImageUrl = element.Element(wp + "attachment_url") != null ? element.Element(wp + "attachment_url").Value : string.Empty;
                this.PostType = element.Element(wp + "post_type").Value;
                if (element.Elements("category") != null)
                {
                    this.Categories.AddRange(element.Elements("category").Select(p => p.Value).Where(p => p != "Uncategorized"));
                }
            }

            public int PostId { get; set; }

            public string PostType { get; set; }

            public DateTime PostDate { get; set; }

            public string Title { get; set; }

            public string Author { get; set; }

            public string Content { get; set; }

            public string Permalink { get; set; }

            public string ImageUrl { get; set; }

            public List<string> Categories { get; set; }
        }
    }
}