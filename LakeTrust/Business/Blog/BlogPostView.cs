﻿using EPiServer.Data;
using EPiServer.Data.Dynamic;
using System;
using System.Linq;

namespace LakeTrust.Business.Blog
{
    [EPiServerDataStore(StoreName = "BlogPostViews", AutomaticallyCreateStore = true, AutomaticallyRemapStore = true)]
    public class BlogPostView : IDynamicData
    {
        private static BlogPostView Get(Guid pageId)
        {
            var store = typeof(BlogPostView).GetStore();
            return store.Items<BlogPostView>()
                .Where(p => p.PageId == pageId)
                .FirstOrDefault();
        }

        public static IQueryable<BlogPostView> GetBlogPosts(Guid blogId)
        {
            var store = typeof(BlogPostView).GetStore();
            return store.Items<BlogPostView>()
                .Where(p => p.BlogId == blogId);
        }

        public static void Increment(Guid pageId, Guid blogId)
        {
            var store = typeof(BlogPostView).GetStore();
            var item = Get(pageId);
            if (item == null)
            {
                item = new BlogPostView();
                item.Views = 1;
                item.BlogId = blogId;
                item.PageId = pageId;
                store.Save(item);
            }
            else
            {
                item.Views = item.Views + 1;
                store.Save(item);
            }
        }

        public Identity Id { get; set; }

        public Guid PageId { get; set; }

        public Guid BlogId { get; set; }

        public int Views { get; set; }
    }
}