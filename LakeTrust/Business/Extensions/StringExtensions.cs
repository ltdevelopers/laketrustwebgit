﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace LakeTrust.Business
{
    public static class StringExtensions
    {
        public static string CleanBody(this string content)
        {
            string inputText = content;

            inputText = inputText.StripTags();
            inputText = inputText.Replace("&nbsp;", " ");
            inputText = inputText.CollapseWhitespace();
            return inputText;
        }

        public static string CleanBody(this string content, int maxLength)
        {
            return CleanBody(content, maxLength, true);
        }

        public static string CleanBody(this string content, int maxLength, bool atWordBreak, string truncateSuffix = "")
        {
            string inputText = content;

            inputText = inputText.StripTags();
            inputText = inputText.Replace("&nbsp;", " ");
            inputText = inputText.Truncate(maxLength, atWordBreak, truncateSuffix);
            inputText = inputText.CollapseWhitespace();
            return inputText;
        }

        public static string CollapseWhitespace(this string inputstring)
        {
            return Regex.Replace(inputstring, " {2-1000}", "");
        }

        public static string PrependUrl(this string mystring)
        {
            return HttpContext.Current.Request.Url.OriginalString.IndexOf(HttpContext.Current.Request.Url.PathAndQuery) + "/" + mystring;
        }

        public static string StripTags(this string inputstring)
        {
            if (!string.IsNullOrEmpty(inputstring))
            {
                return Regex.Replace(inputstring, "<.*?>", string.Empty);
            }
            return string.Empty;
        }

        public static string Truncate(this string text, int charLimit, bool breakAtWord, string truncationSuffix = "")
        {
            if (text.Length <= charLimit)
            {
                return text;
            }
            else
            {
                if (!breakAtWord || (!text.Substring(0, charLimit).Contains(" ")))
                {
                    return string.Concat(text.Substring(0, charLimit), truncationSuffix);
                }
                else
                {
                    return string.Concat(
                        text.Substring(0, text.Substring(0, charLimit).LastIndexOf(" ")), truncationSuffix);
                }
            }
        }

        public static string ToSimpleHtml(this string text)
        {
            return text.Replace("\r\n", "<br>").Replace("\r", "<br>").Replace("\n", "<br>");
        }

        public static string RemoveTrailingSlash(this string text)
        {
            int lastSlash = text.LastIndexOf('/');
            return (lastSlash > -1) ? text.Substring(0, lastSlash) : text;
        }

        #region Twitter

        public static string Link(this string s, string url)
        {
            return string.Format("<a href=\"{0}\" target=\"_blank\">{1}</a>", url, s);
        }

        public static string ParseURL(this string s)
        {
            return Regex.Replace(s, @"(http|ftp|https):\/\/[\w\-_]+(\.[\w\-_]+)+([\w\-\.,@?^=%&:/~\+#]*[\w\-\@?^=%&/~\+#])?", new MatchEvaluator(StringExtensions.URL));
        }

        public static string ParseUsername(this string s)
        {
            return Regex.Replace(s, "(@)((?:[A-Za-z0-9-_]*))", new MatchEvaluator(StringExtensions.Username));
        }

        public static string ParseHashtag(this string s)
        {
            return Regex.Replace(s, "(#)((?:[A-Za-z0-9-_]*))", new MatchEvaluator(StringExtensions.Hashtag));
        }

        private static string Hashtag(Match m)
        {
            string x = m.ToString();
            string tag = x.Replace("#", "%23");
            return x.Link(string.Format("https://twitter.com/search?q={0}&src=hash", tag));
        }

        private static string Username(Match m)
        {
            string x = m.ToString();
            string username = x.Replace("@", "");
            return x.Link("http://twitter.com/" + username);
        }

        private static string URL(Match m)
        {
            string x = m.ToString();
            return x.Link(x);
        }

        #endregion Twitter
    }
}