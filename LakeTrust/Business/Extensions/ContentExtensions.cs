﻿using EPiServer;
using EPiServer.Core;
using EPiServer.Filters;
using EPiServer.Framework.Web;
using EPiServer.ServiceLocation;
using EPiServer.Web;
using EPiServer.Web.Mvc.Html;
using LakeTrust.Business.Interfaces;
using LakeTrust.Business.Rendering;
using LakeTrust.Models.Blocks.HQ;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace LakeTrust.Business
{
    /// <summary>
    /// Extension methods for content
    /// </summary>
    public static class ContentExtensions
    {
        /// <summary>
        /// Shorthand for DataFactory.Instance.Get
        /// </summary>
        /// <typeparam name="TContent"></typeparam>
        /// <param name="contentLink"></param>
        /// <returns></returns>
        public static TContent Get<TContent>(this ContentReference contentLink) where TContent : IContent
        {
            return ServiceLocator.Current.GetInstance<IContentLoader>().Get<TContent>(contentLink);
        }

        public static IEnumerable<TContent> GetChildren<TContent>(this ContentReference contentLink) where TContent : IContent
        {
            return ServiceLocator.Current.GetInstance<IContentLoader>().GetChildren<TContent>(contentLink);
        }

        /// <summary>
        /// Filters content which should not be visible to the user.
        /// </summary>
        public static IEnumerable<T> FilterForDisplay<T>(this IEnumerable<T> contents, bool requirePageTemplate = true, bool requireVisibleInMenu = true)
            where T : IContent
        {
            var accessFilter = new FilterAccess();
            var publishedFilter = new FilterPublished(PagePublishedStatus.Published);
            contents = contents.Where(x => !publishedFilter.ShouldFilter(x) && !accessFilter.ShouldFilter(x));
            if (requirePageTemplate)
            {
                var templateFilter = ServiceLocator.Current.GetInstance<FilterTemplate>();
                templateFilter.TemplateTypeCategories = TemplateTypeCategories.Page;
                contents = contents.Where(x => !templateFilter.ShouldFilter(x));
            }
            if (requireVisibleInMenu)
            {
                contents = contents.Where(x => VisibleInMenu(x));
            }
            return contents;
        }

        public static ContentReference GetParentBelowStartPage(this ContentReference contentLink)
        {
            var content = Get<IContent>(contentLink);
            if (content is PageData)
            {
                var currentPage = Get<IContent>(contentLink);
                if (currentPage.ParentLink == null)
                    return currentPage.ContentLink;

                while (currentPage.ParentLink != SiteDefinition.Current.StartPage && currentPage.ParentLink != SiteDefinition.Current.RootPage)
                {
                    if (PageReference.IsNullOrEmpty(currentPage.ParentLink))
                        break;

                    currentPage = Get<IContent>(currentPage.ParentLink);
                }
                return currentPage.ContentLink;
            }
            return ContentReference.EmptyReference;
        }

        public static bool HasItems(this ContentArea contentArea)
        {
            if (contentArea != null)
                return (contentArea.FilteredItems != null && contentArea.FilteredItems.Count() > 0);

            return false;
        }

        private static bool VisibleInMenu(IContent content)
        {
            var page = content as PageData;
            if (page == null)
            {
                return true;
            }
            return page.VisibleInMenu;
        }

        public static string GetTeaser(this PageData data, string alternateProperty, int charCount, bool trimTags)
        {
            var returnstring = string.Empty;
            if (data == null)
            {
                return returnstring;
            }

            if (data[alternateProperty] != null)
            {
                returnstring = data[alternateProperty].ToString();
            }
            else
            {
                if (data["MainBody"] != null)
                {
                    returnstring = data.GetPropertyValue<XhtmlString>("MainBody").ToHtmlString().StripTags();
                }
                else
                {
                    return string.Empty;
                }
            }

            if (trimTags)
            {
                returnstring = returnstring.StripTags();
            }

            if (returnstring.Length > charCount)
            {
                returnstring = returnstring.Truncate(charCount, true);
            }

            return returnstring;
        }

        public static string GetTeaser(this PageData currentPage, string summaryProperty, string fallBackProperty, int maxCharacters, bool atWordBreak)
        {
            if (currentPage[summaryProperty] != null)
            {
                return currentPage[summaryProperty].ToString().CleanBody(maxCharacters, atWordBreak);
            }

            return currentPage[fallBackProperty] != null ? currentPage[fallBackProperty].ToString().CleanBody(maxCharacters, true) : string.Empty;
        }

        public static void RenderRowBalancerForContentArea(this HtmlHelper helper, ContentArea contentArea)
        {
            var viewData = helper.ViewContext.ViewData;
            var renderingTag = viewData["tag"] as string;
            if (renderingTag == null || !Global.BootstrapContentAreaTagWidths.ContainsKey(renderingTag))
            {
                //Without any tag we don't know the width of the content area and therefor use the standard content area rendering
                helper.RenderContentArea(contentArea);
                return;
            }

            if (helper.ViewContext.RequestContext.RouteData.Values["controller"].ToString() == "HQStartPage")
            {
                var renderer = ServiceLocator.Current.GetInstance<HQBalancedContentAreaRenderer>();
                renderer.RenderBalancedContentArea(helper, contentArea, renderingTag);
            }
            else
            {
                var renderer = ServiceLocator.Current.GetInstance<LakeTrustBalancedContentAreaRenderer>();
                renderer.RenderBalancedContentArea(helper, contentArea, renderingTag);
            }
        }

        public static string GetLakeTrustCssClasses(this HQBaseBlockData data)
        {
            return string.Empty;
        }

        public static string GetHQCssClasses(this HQBaseBlockData data)
        {
            var list = new List<string>() { "metro" };

            list.Add(data.GetOriginalType().Name.ToLowerInvariant().Replace("hq", string.Empty).Replace("block", string.Empty));

            if (data is ICustomCssInContentArea)
            {
                var customClassContent = data as IHQDataItem;
                if (customClassContent != null && !string.IsNullOrWhiteSpace(customClassContent.ContentAreaCssClass))
                    list.Add(customClassContent.ContentAreaCssClass);
            }

            if (list.Count > 0)
                return string.Join(" ", list);

            return string.Empty;
        }
    }
}