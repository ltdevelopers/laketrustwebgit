﻿using EPiServer;
using EPiServer.Core;
using EPiServer.Framework.Blobs;
using EPiServer.ServiceLocation;
using EPiServer.Web.Routing;
using System;
using System.Linq;

namespace LakeTrust.Business
{
    public static class MediaExtensions
    {
        public static string MediaAbsolutePath(this ContentReference contentLink)
        {
            var file = ServiceLocator.Current.GetInstance<IContentLoader>().Get<MediaData>(contentLink).BinaryData;
            if (file is FileBlob)
                return ((FileBlob)file).FilePath;
            return string.Empty;
        }

        public static IContent GetItem(this Url url)
        {
            if (url != null)
            {
                var urlResolver = ServiceLocator.Current.GetInstance<UrlResolver>();
                return urlResolver.Route(new UrlBuilder(url));
            }
            return null;
        }
    }
}