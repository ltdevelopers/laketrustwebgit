﻿using EPiServer;
using EPiServer.Core;
using EPiServer.Web.Routing;
using LakeTrust.Models.Pages;
using LakeTrust.Models.ViewModels;
using System.Web.Mvc;

namespace LakeTrust.Business
{
    /// <summary>
    /// Intercepts actions with view models of type IPageViewModel and populates the view models
    /// Layout and Section properties.
    /// </summary>
    /// <remarks>
    /// This filter frees controllers for pages from having to care about common context needed by
    /// layouts and other page framework components allowing the controllers to focus on the
    /// specifics for the page types and actions that they handle.
    /// </remarks>
    public class PageContextActionFilter : IResultFilter
    {
        private readonly PageViewContextFactory contextFactory;
        private readonly IContentLoader contentLoader;

        public PageContextActionFilter(PageViewContextFactory contextFactory, IContentLoader contentLoader)
        {
            this.contextFactory = contextFactory;
            this.contentLoader = contentLoader;
        }

        public void OnResultExecuting(ResultExecutingContext filterContext)
        {
            var viewModel = filterContext.Controller.ViewData.Model;

            var model = viewModel as IPageViewModel<LakeTrustBaseModel>;
            if (model != null)
            {
                var currentContentLink = filterContext.RequestContext.GetContentLink();

                var layoutModel = model.Layout ?? this.contextFactory.CreateLayoutModel(currentContentLink, filterContext.RequestContext);

                var layoutController = filterContext.Controller as IModifyLayout;
                if (layoutController != null)
                {
                    layoutController.ModifyLayout(layoutModel);
                    model.PageHeader = model.PageHeader ?? this.contextFactory.CreatePageHeaderModel(currentContentLink);
                    model.PageFooter = model.PageFooter ?? this.contextFactory.CreatePageFooterModel(currentContentLink);
                    model.ParentBelowStartPage = model.ParentBelowStartPage ?? currentContentLink.GetParentBelowStartPage();
                }

                model.Layout = layoutModel;

                if (model.Section == null)
                {
                    model.Section = this.contextFactory.GetSection(currentContentLink);
                }
                var content = this.contentLoader.Get<IContent>(currentContentLink);

                if (content is SitePageData)
                {
                    var siteData = content as SitePageData;
                    model.PageFooter.JavascriptIncludes = siteData.JavascriptIncludes;
                    model.PageHeader.HeaderJavascriptIncludes = siteData.HeaderJavascriptIncludes;
                }
            }
        }

        public void OnResultExecuted(ResultExecutedContext filterContext)
        {
        }
    }
}