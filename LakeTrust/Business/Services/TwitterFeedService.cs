﻿using EPiServer;
using EPiServer.Web;
using LakeTrust.Models.Pages;
using LinqToTwitter;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace LakeTrust.Business.Services
{
    public class TwitterFeedService
    {
        private readonly SiteDefinition siteDefinition;
        private readonly IContentLoader contentLoader;
        private readonly CacheHelper cacheHelper;

        public TwitterFeedService(SiteDefinition siteDefinition, IContentLoader contentLoader)
        {
            this.siteDefinition = siteDefinition;
            this.contentLoader = contentLoader;
            cacheHelper = new CacheHelper();
        }

        public List<Status> GetTweets(int takeCount, string username = "")
        {
            var start = this.contentLoader.Get<StartPage>(this.siteDefinition.StartPage);
            var cacheKey = string.Format("twitterstatic-cache-items-{0}-{1}-{2}", username + takeCount.ToString(), username, takeCount);

            var list = new List<Status>();
            if (string.IsNullOrEmpty(username))
            {
                username = start.TwitterUserName;
            }
            if (!string.IsNullOrWhiteSpace(username))
            {
                SingleUserAuthorizer auth = new SingleUserAuthorizer
                {
                    Credentials = new InMemoryCredentials
                    {
                        ConsumerKey = ConfigurationManager.AppSettings["twitterConsumerKey"],
                        ConsumerSecret = ConfigurationManager.AppSettings["twitterConsumerSecret"],
                        OAuthToken = ConfigurationManager.AppSettings["twitterOAuthToken"],
                        AccessToken = ConfigurationManager.AppSettings["twitterAccessToken"]
                    }
                };
                if (!this.cacheHelper.Get<List<Status>>(cacheKey, out list))
                {
                    using (var context = new TwitterContext(auth))
                    {
                        list = (from tweet in context.Status
                                where tweet.Type == StatusType.User &&
                                      tweet.ScreenName == username &&
                                      tweet.IncludeRetweets == true
                                      && tweet.Count == takeCount
                                select tweet).ToList();
                    }
                    this.cacheHelper.Add<List<Status>>(cacheKey, list, 15);
                }
            }
            return list;
        }
    }
}