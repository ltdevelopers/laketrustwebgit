﻿#pragma warning disable 1591

namespace LakeTrust.Business.Services
{
    using System;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Web.Services;
    using System.Web.Services.Protocols;
    using System.Xml.Serialization;

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.33440")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Web.Services.WebServiceBindingAttribute(Name = "BasicHttpBinding_IImageServer", Namespace = "http://tempuri.org/")]
    public partial class ImageServer : System.Web.Services.Protocols.SoapHttpClientProtocol
    {
        private System.Threading.SendOrPostCallback MostRecentImageOperationCompleted;

        private System.Threading.SendOrPostCallback PreviousImageOperationCompleted;

        private System.Threading.SendOrPostCallback NextImageOperationCompleted;

        private System.Threading.SendOrPostCallback NextDayOperationCompleted;

        private System.Threading.SendOrPostCallback PreviousDayOperationCompleted;

        private System.Threading.SendOrPostCallback GetAllCustomerInfoOperationCompleted;

        private System.Threading.SendOrPostCallback GetCustomerInfoOperationCompleted;

        private System.Threading.SendOrPostCallback GetAllProjectInfoOperationCompleted;

        private System.Threading.SendOrPostCallback GetProjectInfoOperationCompleted;

        private System.Threading.SendOrPostCallback GetAllWebCamInfoOperationCompleted;

        private System.Threading.SendOrPostCallback GetDaysWithImagesOperationCompleted;

        private System.Threading.SendOrPostCallback GetImageNearestOperationCompleted;

        private System.Threading.SendOrPostCallback GetImageTimesOperationCompleted;

        private bool useDefaultCredentialsSetExplicitly;

        /// <remarks/>
        public ImageServer()
        {
            this.Url = "http://webcams.christmanco.com/ImageServer/ImageServer.svc";
            if ((this.IsLocalFileSystemWebService(this.Url) == true))
            {
                this.UseDefaultCredentials = true;
                this.useDefaultCredentialsSetExplicitly = false;
            }
            else
            {
                this.useDefaultCredentialsSetExplicitly = true;
            }
        }

        public new string Url
        {
            get
            {
                return base.Url;
            }
            set
            {
                if ((((this.IsLocalFileSystemWebService(base.Url) == true)
                            && (this.useDefaultCredentialsSetExplicitly == false))
                            && (this.IsLocalFileSystemWebService(value) == false)))
                {
                    base.UseDefaultCredentials = false;
                }
                base.Url = value;
            }
        }

        public new bool UseDefaultCredentials
        {
            get
            {
                return base.UseDefaultCredentials;
            }
            set
            {
                base.UseDefaultCredentials = value;
                this.useDefaultCredentialsSetExplicitly = true;
            }
        }

        /// <remarks/>
        public event MostRecentImageCompletedEventHandler MostRecentImageCompleted;

        /// <remarks/>
        public event PreviousImageCompletedEventHandler PreviousImageCompleted;

        /// <remarks/>
        public event NextImageCompletedEventHandler NextImageCompleted;

        /// <remarks/>
        public event NextDayCompletedEventHandler NextDayCompleted;

        /// <remarks/>
        public event PreviousDayCompletedEventHandler PreviousDayCompleted;

        /// <remarks/>
        public event GetAllCustomerInfoCompletedEventHandler GetAllCustomerInfoCompleted;

        /// <remarks/>
        public event GetCustomerInfoCompletedEventHandler GetCustomerInfoCompleted;

        /// <remarks/>
        public event GetAllProjectInfoCompletedEventHandler GetAllProjectInfoCompleted;

        /// <remarks/>
        public event GetProjectInfoCompletedEventHandler GetProjectInfoCompleted;

        /// <remarks/>
        public event GetAllWebCamInfoCompletedEventHandler GetAllWebCamInfoCompleted;

        /// <remarks/>
        public event GetDaysWithImagesCompletedEventHandler GetDaysWithImagesCompleted;

        /// <remarks/>
        public event GetImageNearestCompletedEventHandler GetImageNearestCompleted;

        /// <remarks/>
        public event GetImageTimesCompletedEventHandler GetImageTimesCompleted;

        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/IImageServer/MostRecentImage", RequestNamespace = "http://tempuri.org/", ResponseNamespace = "http://tempuri.org/", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        [return: System.Xml.Serialization.XmlElementAttribute(IsNullable = true)]
        public ImageInfo MostRecentImage([System.Xml.Serialization.XmlElementAttribute(IsNullable = true)] string customerName, [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)] string projectName, [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)] string webCamName)
        {
            object[] results = this.Invoke("MostRecentImage", new object[] {
                        customerName,
                        projectName,
                        webCamName});
            return ((ImageInfo)(results[0]));
        }

        /// <remarks/>
        public void MostRecentImageAsync(string customerName, string projectName, string webCamName)
        {
            this.MostRecentImageAsync(customerName, projectName, webCamName, null);
        }

        /// <remarks/>
        public void MostRecentImageAsync(string customerName, string projectName, string webCamName, object userState)
        {
            if ((this.MostRecentImageOperationCompleted == null))
            {
                this.MostRecentImageOperationCompleted = new System.Threading.SendOrPostCallback(this.OnMostRecentImageOperationCompleted);
            }
            this.InvokeAsync("MostRecentImage", new object[] {
                        customerName,
                        projectName,
                        webCamName}, this.MostRecentImageOperationCompleted, userState);
        }

        private void OnMostRecentImageOperationCompleted(object arg)
        {
            if ((this.MostRecentImageCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.MostRecentImageCompleted(this, new MostRecentImageCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/IImageServer/PreviousImage", RequestNamespace = "http://tempuri.org/", ResponseNamespace = "http://tempuri.org/", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        [return: System.Xml.Serialization.XmlElementAttribute(IsNullable = true)]
        public ImageInfo PreviousImage([System.Xml.Serialization.XmlElementAttribute(IsNullable = true)] string customerName, [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)] string projectName, [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)] string webCamName, System.DateTime currentImageTime, [System.Xml.Serialization.XmlIgnoreAttribute()] bool currentImageTimeSpecified)
        {
            object[] results = this.Invoke("PreviousImage", new object[] {
                        customerName,
                        projectName,
                        webCamName,
                        currentImageTime,
                        currentImageTimeSpecified});
            return ((ImageInfo)(results[0]));
        }

        /// <remarks/>
        public void PreviousImageAsync(string customerName, string projectName, string webCamName, System.DateTime currentImageTime, bool currentImageTimeSpecified)
        {
            this.PreviousImageAsync(customerName, projectName, webCamName, currentImageTime, currentImageTimeSpecified, null);
        }

        /// <remarks/>
        public void PreviousImageAsync(string customerName, string projectName, string webCamName, System.DateTime currentImageTime, bool currentImageTimeSpecified, object userState)
        {
            if ((this.PreviousImageOperationCompleted == null))
            {
                this.PreviousImageOperationCompleted = new System.Threading.SendOrPostCallback(this.OnPreviousImageOperationCompleted);
            }
            this.InvokeAsync("PreviousImage", new object[] {
                        customerName,
                        projectName,
                        webCamName,
                        currentImageTime,
                        currentImageTimeSpecified}, this.PreviousImageOperationCompleted, userState);
        }

        private void OnPreviousImageOperationCompleted(object arg)
        {
            if ((this.PreviousImageCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.PreviousImageCompleted(this, new PreviousImageCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/IImageServer/NextImage", RequestNamespace = "http://tempuri.org/", ResponseNamespace = "http://tempuri.org/", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        [return: System.Xml.Serialization.XmlElementAttribute(IsNullable = true)]
        public ImageInfo NextImage([System.Xml.Serialization.XmlElementAttribute(IsNullable = true)] string customerName, [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)] string projectName, [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)] string webCamName, System.DateTime currentImageTime, [System.Xml.Serialization.XmlIgnoreAttribute()] bool currentImageTimeSpecified)
        {
            object[] results = this.Invoke("NextImage", new object[] {
                        customerName,
                        projectName,
                        webCamName,
                        currentImageTime,
                        currentImageTimeSpecified});
            return ((ImageInfo)(results[0]));
        }

        /// <remarks/>
        public void NextImageAsync(string customerName, string projectName, string webCamName, System.DateTime currentImageTime, bool currentImageTimeSpecified)
        {
            this.NextImageAsync(customerName, projectName, webCamName, currentImageTime, currentImageTimeSpecified, null);
        }

        /// <remarks/>
        public void NextImageAsync(string customerName, string projectName, string webCamName, System.DateTime currentImageTime, bool currentImageTimeSpecified, object userState)
        {
            if ((this.NextImageOperationCompleted == null))
            {
                this.NextImageOperationCompleted = new System.Threading.SendOrPostCallback(this.OnNextImageOperationCompleted);
            }
            this.InvokeAsync("NextImage", new object[] {
                        customerName,
                        projectName,
                        webCamName,
                        currentImageTime,
                        currentImageTimeSpecified}, this.NextImageOperationCompleted, userState);
        }

        private void OnNextImageOperationCompleted(object arg)
        {
            if ((this.NextImageCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.NextImageCompleted(this, new NextImageCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/IImageServer/NextDay", RequestNamespace = "http://tempuri.org/", ResponseNamespace = "http://tempuri.org/", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        [return: System.Xml.Serialization.XmlElementAttribute(IsNullable = true)]
        public ImageInfo NextDay([System.Xml.Serialization.XmlElementAttribute(IsNullable = true)] string customerName, [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)] string projectName, [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)] string webCamName, System.DateTime currentImageTime, [System.Xml.Serialization.XmlIgnoreAttribute()] bool currentImageTimeSpecified)
        {
            object[] results = this.Invoke("NextDay", new object[] {
                        customerName,
                        projectName,
                        webCamName,
                        currentImageTime,
                        currentImageTimeSpecified});
            return ((ImageInfo)(results[0]));
        }

        /// <remarks/>
        public void NextDayAsync(string customerName, string projectName, string webCamName, System.DateTime currentImageTime, bool currentImageTimeSpecified)
        {
            this.NextDayAsync(customerName, projectName, webCamName, currentImageTime, currentImageTimeSpecified, null);
        }

        /// <remarks/>
        public void NextDayAsync(string customerName, string projectName, string webCamName, System.DateTime currentImageTime, bool currentImageTimeSpecified, object userState)
        {
            if ((this.NextDayOperationCompleted == null))
            {
                this.NextDayOperationCompleted = new System.Threading.SendOrPostCallback(this.OnNextDayOperationCompleted);
            }
            this.InvokeAsync("NextDay", new object[] {
                        customerName,
                        projectName,
                        webCamName,
                        currentImageTime,
                        currentImageTimeSpecified}, this.NextDayOperationCompleted, userState);
        }

        private void OnNextDayOperationCompleted(object arg)
        {
            if ((this.NextDayCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.NextDayCompleted(this, new NextDayCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/IImageServer/PreviousDay", RequestNamespace = "http://tempuri.org/", ResponseNamespace = "http://tempuri.org/", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        [return: System.Xml.Serialization.XmlElementAttribute(IsNullable = true)]
        public ImageInfo PreviousDay([System.Xml.Serialization.XmlElementAttribute(IsNullable = true)] string customerName, [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)] string projectName, [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)] string webCamName, System.DateTime currentImageTime, [System.Xml.Serialization.XmlIgnoreAttribute()] bool currentImageTimeSpecified)
        {
            object[] results = this.Invoke("PreviousDay", new object[] {
                        customerName,
                        projectName,
                        webCamName,
                        currentImageTime,
                        currentImageTimeSpecified});
            return ((ImageInfo)(results[0]));
        }

        /// <remarks/>
        public void PreviousDayAsync(string customerName, string projectName, string webCamName, System.DateTime currentImageTime, bool currentImageTimeSpecified)
        {
            this.PreviousDayAsync(customerName, projectName, webCamName, currentImageTime, currentImageTimeSpecified, null);
        }

        /// <remarks/>
        public void PreviousDayAsync(string customerName, string projectName, string webCamName, System.DateTime currentImageTime, bool currentImageTimeSpecified, object userState)
        {
            if ((this.PreviousDayOperationCompleted == null))
            {
                this.PreviousDayOperationCompleted = new System.Threading.SendOrPostCallback(this.OnPreviousDayOperationCompleted);
            }
            this.InvokeAsync("PreviousDay", new object[] {
                        customerName,
                        projectName,
                        webCamName,
                        currentImageTime,
                        currentImageTimeSpecified}, this.PreviousDayOperationCompleted, userState);
        }

        private void OnPreviousDayOperationCompleted(object arg)
        {
            if ((this.PreviousDayCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.PreviousDayCompleted(this, new PreviousDayCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/IImageServer/GetAllCustomerInfo", RequestNamespace = "http://tempuri.org/", ResponseNamespace = "http://tempuri.org/", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        [return: System.Xml.Serialization.XmlArrayAttribute(IsNullable = true)]
        [return: System.Xml.Serialization.XmlArrayItemAttribute(Namespace = "http://schemas.datacontract.org/2004/07/Christman")]
        public CustomerInfo[] GetAllCustomerInfo()
        {
            object[] results = this.Invoke("GetAllCustomerInfo", new object[0]);
            return ((CustomerInfo[])(results[0]));
        }

        /// <remarks/>
        public void GetAllCustomerInfoAsync()
        {
            this.GetAllCustomerInfoAsync(null);
        }

        /// <remarks/>
        public void GetAllCustomerInfoAsync(object userState)
        {
            if ((this.GetAllCustomerInfoOperationCompleted == null))
            {
                this.GetAllCustomerInfoOperationCompleted = new System.Threading.SendOrPostCallback(this.OnGetAllCustomerInfoOperationCompleted);
            }
            this.InvokeAsync("GetAllCustomerInfo", new object[0], this.GetAllCustomerInfoOperationCompleted, userState);
        }

        private void OnGetAllCustomerInfoOperationCompleted(object arg)
        {
            if ((this.GetAllCustomerInfoCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.GetAllCustomerInfoCompleted(this, new GetAllCustomerInfoCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/IImageServer/GetCustomerInfo", RequestNamespace = "http://tempuri.org/", ResponseNamespace = "http://tempuri.org/", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        [return: System.Xml.Serialization.XmlElementAttribute(IsNullable = true)]
        public CustomerInfo GetCustomerInfo([System.Xml.Serialization.XmlElementAttribute(IsNullable = true)] string name)
        {
            object[] results = this.Invoke("GetCustomerInfo", new object[] {
                        name});
            return ((CustomerInfo)(results[0]));
        }

        /// <remarks/>
        public void GetCustomerInfoAsync(string name)
        {
            this.GetCustomerInfoAsync(name, null);
        }

        /// <remarks/>
        public void GetCustomerInfoAsync(string name, object userState)
        {
            if ((this.GetCustomerInfoOperationCompleted == null))
            {
                this.GetCustomerInfoOperationCompleted = new System.Threading.SendOrPostCallback(this.OnGetCustomerInfoOperationCompleted);
            }
            this.InvokeAsync("GetCustomerInfo", new object[] {
                        name}, this.GetCustomerInfoOperationCompleted, userState);
        }

        private void OnGetCustomerInfoOperationCompleted(object arg)
        {
            if ((this.GetCustomerInfoCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.GetCustomerInfoCompleted(this, new GetCustomerInfoCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/IImageServer/GetAllProjectInfo", RequestNamespace = "http://tempuri.org/", ResponseNamespace = "http://tempuri.org/", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        [return: System.Xml.Serialization.XmlArrayAttribute(IsNullable = true)]
        [return: System.Xml.Serialization.XmlArrayItemAttribute(Namespace = "http://schemas.datacontract.org/2004/07/Christman")]
        public ProjectInfo[] GetAllProjectInfo([System.Xml.Serialization.XmlElementAttribute(IsNullable = true)] string customerName)
        {
            object[] results = this.Invoke("GetAllProjectInfo", new object[] {
                        customerName});
            return ((ProjectInfo[])(results[0]));
        }

        /// <remarks/>
        public void GetAllProjectInfoAsync(string customerName)
        {
            this.GetAllProjectInfoAsync(customerName, null);
        }

        /// <remarks/>
        public void GetAllProjectInfoAsync(string customerName, object userState)
        {
            if ((this.GetAllProjectInfoOperationCompleted == null))
            {
                this.GetAllProjectInfoOperationCompleted = new System.Threading.SendOrPostCallback(this.OnGetAllProjectInfoOperationCompleted);
            }
            this.InvokeAsync("GetAllProjectInfo", new object[] {
                        customerName}, this.GetAllProjectInfoOperationCompleted, userState);
        }

        private void OnGetAllProjectInfoOperationCompleted(object arg)
        {
            if ((this.GetAllProjectInfoCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.GetAllProjectInfoCompleted(this, new GetAllProjectInfoCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/IImageServer/GetProjectInfo", RequestNamespace = "http://tempuri.org/", ResponseNamespace = "http://tempuri.org/", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        [return: System.Xml.Serialization.XmlElementAttribute(IsNullable = true)]
        public ProjectInfo GetProjectInfo([System.Xml.Serialization.XmlElementAttribute(IsNullable = true)] string customerName, [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)] string name)
        {
            object[] results = this.Invoke("GetProjectInfo", new object[] {
                        customerName,
                        name});
            return ((ProjectInfo)(results[0]));
        }

        /// <remarks/>
        public void GetProjectInfoAsync(string customerName, string name)
        {
            this.GetProjectInfoAsync(customerName, name, null);
        }

        /// <remarks/>
        public void GetProjectInfoAsync(string customerName, string name, object userState)
        {
            if ((this.GetProjectInfoOperationCompleted == null))
            {
                this.GetProjectInfoOperationCompleted = new System.Threading.SendOrPostCallback(this.OnGetProjectInfoOperationCompleted);
            }
            this.InvokeAsync("GetProjectInfo", new object[] {
                        customerName,
                        name}, this.GetProjectInfoOperationCompleted, userState);
        }

        private void OnGetProjectInfoOperationCompleted(object arg)
        {
            if ((this.GetProjectInfoCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.GetProjectInfoCompleted(this, new GetProjectInfoCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/IImageServer/GetAllWebCamInfo", RequestNamespace = "http://tempuri.org/", ResponseNamespace = "http://tempuri.org/", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        [return: System.Xml.Serialization.XmlArrayAttribute(IsNullable = true)]
        [return: System.Xml.Serialization.XmlArrayItemAttribute(Namespace = "http://schemas.datacontract.org/2004/07/Christman")]
        public WebCamInfo[] GetAllWebCamInfo([System.Xml.Serialization.XmlElementAttribute(IsNullable = true)] string customerName, [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)] string projName)
        {
            object[] results = this.Invoke("GetAllWebCamInfo", new object[] {
                        customerName,
                        projName});
            return ((WebCamInfo[])(results[0]));
        }

        /// <remarks/>
        public void GetAllWebCamInfoAsync(string customerName, string projName)
        {
            this.GetAllWebCamInfoAsync(customerName, projName, null);
        }

        /// <remarks/>
        public void GetAllWebCamInfoAsync(string customerName, string projName, object userState)
        {
            if ((this.GetAllWebCamInfoOperationCompleted == null))
            {
                this.GetAllWebCamInfoOperationCompleted = new System.Threading.SendOrPostCallback(this.OnGetAllWebCamInfoOperationCompleted);
            }
            this.InvokeAsync("GetAllWebCamInfo", new object[] {
                        customerName,
                        projName}, this.GetAllWebCamInfoOperationCompleted, userState);
        }

        private void OnGetAllWebCamInfoOperationCompleted(object arg)
        {
            if ((this.GetAllWebCamInfoCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.GetAllWebCamInfoCompleted(this, new GetAllWebCamInfoCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/IImageServer/GetDaysWithImages", RequestNamespace = "http://tempuri.org/", ResponseNamespace = "http://tempuri.org/", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        [return: System.Xml.Serialization.XmlElementAttribute(IsNullable = true)]
        public DaysWithImages GetDaysWithImages([System.Xml.Serialization.XmlElementAttribute(IsNullable = true)] string customerName, [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)] string projectName, [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)] string webCamName)
        {
            object[] results = this.Invoke("GetDaysWithImages", new object[] {
                        customerName,
                        projectName,
                        webCamName});
            return ((DaysWithImages)(results[0]));
        }

        /// <remarks/>
        public void GetDaysWithImagesAsync(string customerName, string projectName, string webCamName)
        {
            this.GetDaysWithImagesAsync(customerName, projectName, webCamName, null);
        }

        /// <remarks/>
        public void GetDaysWithImagesAsync(string customerName, string projectName, string webCamName, object userState)
        {
            if ((this.GetDaysWithImagesOperationCompleted == null))
            {
                this.GetDaysWithImagesOperationCompleted = new System.Threading.SendOrPostCallback(this.OnGetDaysWithImagesOperationCompleted);
            }
            this.InvokeAsync("GetDaysWithImages", new object[] {
                        customerName,
                        projectName,
                        webCamName}, this.GetDaysWithImagesOperationCompleted, userState);
        }

        private void OnGetDaysWithImagesOperationCompleted(object arg)
        {
            if ((this.GetDaysWithImagesCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.GetDaysWithImagesCompleted(this, new GetDaysWithImagesCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/IImageServer/GetImageNearest", RequestNamespace = "http://tempuri.org/", ResponseNamespace = "http://tempuri.org/", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        [return: System.Xml.Serialization.XmlElementAttribute(IsNullable = true)]
        public ImageInfo GetImageNearest([System.Xml.Serialization.XmlElementAttribute(IsNullable = true)] string customerName, [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)] string projectName, [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)] string webCamName, System.DateTime imageDateTime, [System.Xml.Serialization.XmlIgnoreAttribute()] bool imageDateTimeSpecified)
        {
            object[] results = this.Invoke("GetImageNearest", new object[] {
                        customerName,
                        projectName,
                        webCamName,
                        imageDateTime,
                        imageDateTimeSpecified});
            return ((ImageInfo)(results[0]));
        }

        /// <remarks/>
        public void GetImageNearestAsync(string customerName, string projectName, string webCamName, System.DateTime imageDateTime, bool imageDateTimeSpecified)
        {
            this.GetImageNearestAsync(customerName, projectName, webCamName, imageDateTime, imageDateTimeSpecified, null);
        }

        /// <remarks/>
        public void GetImageNearestAsync(string customerName, string projectName, string webCamName, System.DateTime imageDateTime, bool imageDateTimeSpecified, object userState)
        {
            if ((this.GetImageNearestOperationCompleted == null))
            {
                this.GetImageNearestOperationCompleted = new System.Threading.SendOrPostCallback(this.OnGetImageNearestOperationCompleted);
            }
            this.InvokeAsync("GetImageNearest", new object[] {
                        customerName,
                        projectName,
                        webCamName,
                        imageDateTime,
                        imageDateTimeSpecified}, this.GetImageNearestOperationCompleted, userState);
        }

        private void OnGetImageNearestOperationCompleted(object arg)
        {
            if ((this.GetImageNearestCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.GetImageNearestCompleted(this, new GetImageNearestCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/IImageServer/GetImageTimes", RequestNamespace = "http://tempuri.org/", ResponseNamespace = "http://tempuri.org/", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        [return: System.Xml.Serialization.XmlElementAttribute(IsNullable = true)]
        public ImageTimes GetImageTimes([System.Xml.Serialization.XmlElementAttribute(IsNullable = true)] string customerName, [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)] string projectName, [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)] string webCamName, System.DateTime imageDate, [System.Xml.Serialization.XmlIgnoreAttribute()] bool imageDateSpecified)
        {
            object[] results = this.Invoke("GetImageTimes", new object[] {
                        customerName,
                        projectName,
                        webCamName,
                        imageDate,
                        imageDateSpecified});
            return ((ImageTimes)(results[0]));
        }

        /// <remarks/>
        public void GetImageTimesAsync(string customerName, string projectName, string webCamName, System.DateTime imageDate, bool imageDateSpecified)
        {
            this.GetImageTimesAsync(customerName, projectName, webCamName, imageDate, imageDateSpecified, null);
        }

        /// <remarks/>
        public void GetImageTimesAsync(string customerName, string projectName, string webCamName, System.DateTime imageDate, bool imageDateSpecified, object userState)
        {
            if ((this.GetImageTimesOperationCompleted == null))
            {
                this.GetImageTimesOperationCompleted = new System.Threading.SendOrPostCallback(this.OnGetImageTimesOperationCompleted);
            }
            this.InvokeAsync("GetImageTimes", new object[] {
                        customerName,
                        projectName,
                        webCamName,
                        imageDate,
                        imageDateSpecified}, this.GetImageTimesOperationCompleted, userState);
        }

        private void OnGetImageTimesOperationCompleted(object arg)
        {
            if ((this.GetImageTimesCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.GetImageTimesCompleted(this, new GetImageTimesCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        public new void CancelAsync(object userState)
        {
            base.CancelAsync(userState);
        }

        private bool IsLocalFileSystemWebService(string url)
        {
            if (((url == null)
                        || (url == string.Empty)))
            {
                return false;
            }
            System.Uri wsUri = new System.Uri(url);
            if (((wsUri.Port >= 1024)
                        && (string.Compare(wsUri.Host, "localHost", System.StringComparison.OrdinalIgnoreCase) == 0)))
            {
                return true;
            }
            return false;
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.33440")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://schemas.datacontract.org/2004/07/Christman")]
    public partial class ImageInfo
    {
        private string customerNameField;

        private System.DateTime firstImageDateField;

        private bool firstImageDateFieldSpecified;

        private byte[] imageDataField;

        private System.DateTime imageDateTimeField;

        private bool imageDateTimeFieldSpecified;

        private string imageUrlField;

        private bool isNextDayAvailableField;

        private bool isNextDayAvailableFieldSpecified;

        private bool isNextImageAvailableField;

        private bool isNextImageAvailableFieldSpecified;

        private bool isPreviousDayAvailableField;

        private bool isPreviousDayAvailableFieldSpecified;

        private bool isPreviousImageAvailableField;

        private bool isPreviousImageAvailableFieldSpecified;

        private System.DateTime lastImageDateField;

        private bool lastImageDateFieldSpecified;

        private string projectNameField;

        private string weatherUrlField;

        private string webCamNameField;

        private string zoomUrlField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)]
        public string CustomerName
        {
            get
            {
                return this.customerNameField;
            }
            set
            {
                this.customerNameField = value;
            }
        }

        /// <remarks/>
        public System.DateTime FirstImageDate
        {
            get
            {
                return this.firstImageDateField;
            }
            set
            {
                this.firstImageDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool FirstImageDateSpecified
        {
            get
            {
                return this.firstImageDateFieldSpecified;
            }
            set
            {
                this.firstImageDateFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "base64Binary", IsNullable = true)]
        public byte[] ImageData
        {
            get
            {
                return this.imageDataField;
            }
            set
            {
                this.imageDataField = value;
            }
        }

        /// <remarks/>
        public System.DateTime ImageDateTime
        {
            get
            {
                return this.imageDateTimeField;
            }
            set
            {
                this.imageDateTimeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ImageDateTimeSpecified
        {
            get
            {
                return this.imageDateTimeFieldSpecified;
            }
            set
            {
                this.imageDateTimeFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)]
        public string ImageUrl
        {
            get
            {
                return this.imageUrlField;
            }
            set
            {
                this.imageUrlField = value;
            }
        }

        /// <remarks/>
        public bool IsNextDayAvailable
        {
            get
            {
                return this.isNextDayAvailableField;
            }
            set
            {
                this.isNextDayAvailableField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IsNextDayAvailableSpecified
        {
            get
            {
                return this.isNextDayAvailableFieldSpecified;
            }
            set
            {
                this.isNextDayAvailableFieldSpecified = value;
            }
        }

        /// <remarks/>
        public bool IsNextImageAvailable
        {
            get
            {
                return this.isNextImageAvailableField;
            }
            set
            {
                this.isNextImageAvailableField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IsNextImageAvailableSpecified
        {
            get
            {
                return this.isNextImageAvailableFieldSpecified;
            }
            set
            {
                this.isNextImageAvailableFieldSpecified = value;
            }
        }

        /// <remarks/>
        public bool IsPreviousDayAvailable
        {
            get
            {
                return this.isPreviousDayAvailableField;
            }
            set
            {
                this.isPreviousDayAvailableField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IsPreviousDayAvailableSpecified
        {
            get
            {
                return this.isPreviousDayAvailableFieldSpecified;
            }
            set
            {
                this.isPreviousDayAvailableFieldSpecified = value;
            }
        }

        /// <remarks/>
        public bool IsPreviousImageAvailable
        {
            get
            {
                return this.isPreviousImageAvailableField;
            }
            set
            {
                this.isPreviousImageAvailableField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IsPreviousImageAvailableSpecified
        {
            get
            {
                return this.isPreviousImageAvailableFieldSpecified;
            }
            set
            {
                this.isPreviousImageAvailableFieldSpecified = value;
            }
        }

        /// <remarks/>
        public System.DateTime LastImageDate
        {
            get
            {
                return this.lastImageDateField;
            }
            set
            {
                this.lastImageDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool LastImageDateSpecified
        {
            get
            {
                return this.lastImageDateFieldSpecified;
            }
            set
            {
                this.lastImageDateFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)]
        public string ProjectName
        {
            get
            {
                return this.projectNameField;
            }
            set
            {
                this.projectNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)]
        public string WeatherUrl
        {
            get
            {
                return this.weatherUrlField;
            }
            set
            {
                this.weatherUrlField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)]
        public string WebCamName
        {
            get
            {
                return this.webCamNameField;
            }
            set
            {
                this.webCamNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)]
        public string ZoomUrl
        {
            get
            {
                return this.zoomUrlField;
            }
            set
            {
                this.zoomUrlField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.33440")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://schemas.datacontract.org/2004/07/Christman")]
    public partial class ImageTimes
    {
        private string customerNameField;

        private System.DateTime imageDateField;

        private bool imageDateFieldSpecified;

        private string[] imageTimes1Field;

        private string projectNameField;

        private string webCamNameField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)]
        public string CustomerName
        {
            get
            {
                return this.customerNameField;
            }
            set
            {
                this.customerNameField = value;
            }
        }

        /// <remarks/>
        public System.DateTime ImageDate
        {
            get
            {
                return this.imageDateField;
            }
            set
            {
                this.imageDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ImageDateSpecified
        {
            get
            {
                return this.imageDateFieldSpecified;
            }
            set
            {
                this.imageDateFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute("ImageTimes", IsNullable = true)]
        [System.Xml.Serialization.XmlArrayItemAttribute(Namespace = "http://schemas.microsoft.com/2003/10/Serialization/Arrays", DataType = "duration", IsNullable = false)]
        public string[] ImageTimes1
        {
            get
            {
                return this.imageTimes1Field;
            }
            set
            {
                this.imageTimes1Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)]
        public string ProjectName
        {
            get
            {
                return this.projectNameField;
            }
            set
            {
                this.projectNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)]
        public string WebCamName
        {
            get
            {
                return this.webCamNameField;
            }
            set
            {
                this.webCamNameField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.33440")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://schemas.datacontract.org/2004/07/Christman")]
    public partial class DaysWithImages
    {
        private string customerNameField;

        private System.DateTime[] daysField;

        private string projectNameField;

        private string webCamNameField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)]
        public string CustomerName
        {
            get
            {
                return this.customerNameField;
            }
            set
            {
                this.customerNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(IsNullable = true)]
        [System.Xml.Serialization.XmlArrayItemAttribute(Namespace = "http://schemas.microsoft.com/2003/10/Serialization/Arrays", IsNullable = false)]
        public System.DateTime[] Days
        {
            get
            {
                return this.daysField;
            }
            set
            {
                this.daysField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)]
        public string ProjectName
        {
            get
            {
                return this.projectNameField;
            }
            set
            {
                this.projectNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)]
        public string WebCamName
        {
            get
            {
                return this.webCamNameField;
            }
            set
            {
                this.webCamNameField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.33440")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://schemas.datacontract.org/2004/07/Christman")]
    public partial class WebCamInfo
    {
        private string customerNameField;

        private string displayNameField;

        private string nameField;

        private string projectNameField;

        private string streamingUrlField;

        private string timeLapseUrlField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)]
        public string CustomerName
        {
            get
            {
                return this.customerNameField;
            }
            set
            {
                this.customerNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)]
        public string DisplayName
        {
            get
            {
                return this.displayNameField;
            }
            set
            {
                this.displayNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)]
        public string Name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)]
        public string ProjectName
        {
            get
            {
                return this.projectNameField;
            }
            set
            {
                this.projectNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)]
        public string StreamingUrl
        {
            get
            {
                return this.streamingUrlField;
            }
            set
            {
                this.streamingUrlField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)]
        public string TimeLapseUrl
        {
            get
            {
                return this.timeLapseUrlField;
            }
            set
            {
                this.timeLapseUrlField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.33440")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://schemas.datacontract.org/2004/07/Christman")]
    public partial class ProjectInfo
    {
        private string customerNameField;

        private string displayNameField;

        private string logoAltTextField;

        private string logoTargetUrlField;

        private string logoUrlField;

        private string nameField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)]
        public string CustomerName
        {
            get
            {
                return this.customerNameField;
            }
            set
            {
                this.customerNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)]
        public string DisplayName
        {
            get
            {
                return this.displayNameField;
            }
            set
            {
                this.displayNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)]
        public string LogoAltText
        {
            get
            {
                return this.logoAltTextField;
            }
            set
            {
                this.logoAltTextField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)]
        public string LogoTargetUrl
        {
            get
            {
                return this.logoTargetUrlField;
            }
            set
            {
                this.logoTargetUrlField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)]
        public string LogoUrl
        {
            get
            {
                return this.logoUrlField;
            }
            set
            {
                this.logoUrlField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)]
        public string Name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.33440")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://schemas.datacontract.org/2004/07/Christman")]
    public partial class CustomerInfo
    {
        private string displayNameField;

        private string logoAltTextField;

        private string logoTargetUrlField;

        private string logoUrlField;

        private string nameField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)]
        public string DisplayName
        {
            get
            {
                return this.displayNameField;
            }
            set
            {
                this.displayNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)]
        public string LogoAltText
        {
            get
            {
                return this.logoAltTextField;
            }
            set
            {
                this.logoAltTextField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)]
        public string LogoTargetUrl
        {
            get
            {
                return this.logoTargetUrlField;
            }
            set
            {
                this.logoTargetUrlField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)]
        public string LogoUrl
        {
            get
            {
                return this.logoUrlField;
            }
            set
            {
                this.logoUrlField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)]
        public string Name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.33440")]
    public delegate void MostRecentImageCompletedEventHandler(object sender, MostRecentImageCompletedEventArgs e);

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.33440")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class MostRecentImageCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
    {
        private object[] results;

        internal MostRecentImageCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) :
            base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <remarks/>
        public ImageInfo Result
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((ImageInfo)(this.results[0]));
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.33440")]
    public delegate void PreviousImageCompletedEventHandler(object sender, PreviousImageCompletedEventArgs e);

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.33440")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class PreviousImageCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
    {
        private object[] results;

        internal PreviousImageCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) :
            base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <remarks/>
        public ImageInfo Result
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((ImageInfo)(this.results[0]));
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.33440")]
    public delegate void NextImageCompletedEventHandler(object sender, NextImageCompletedEventArgs e);

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.33440")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class NextImageCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
    {
        private object[] results;

        internal NextImageCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) :
            base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <remarks/>
        public ImageInfo Result
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((ImageInfo)(this.results[0]));
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.33440")]
    public delegate void NextDayCompletedEventHandler(object sender, NextDayCompletedEventArgs e);

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.33440")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class NextDayCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
    {
        private object[] results;

        internal NextDayCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) :
            base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <remarks/>
        public ImageInfo Result
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((ImageInfo)(this.results[0]));
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.33440")]
    public delegate void PreviousDayCompletedEventHandler(object sender, PreviousDayCompletedEventArgs e);

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.33440")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class PreviousDayCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
    {
        private object[] results;

        internal PreviousDayCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) :
            base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <remarks/>
        public ImageInfo Result
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((ImageInfo)(this.results[0]));
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.33440")]
    public delegate void GetAllCustomerInfoCompletedEventHandler(object sender, GetAllCustomerInfoCompletedEventArgs e);

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.33440")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class GetAllCustomerInfoCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
    {
        private object[] results;

        internal GetAllCustomerInfoCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) :
            base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <remarks/>
        public CustomerInfo[] Result
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((CustomerInfo[])(this.results[0]));
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.33440")]
    public delegate void GetCustomerInfoCompletedEventHandler(object sender, GetCustomerInfoCompletedEventArgs e);

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.33440")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class GetCustomerInfoCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
    {
        private object[] results;

        internal GetCustomerInfoCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) :
            base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <remarks/>
        public CustomerInfo Result
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((CustomerInfo)(this.results[0]));
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.33440")]
    public delegate void GetAllProjectInfoCompletedEventHandler(object sender, GetAllProjectInfoCompletedEventArgs e);

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.33440")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class GetAllProjectInfoCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
    {
        private object[] results;

        internal GetAllProjectInfoCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) :
            base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <remarks/>
        public ProjectInfo[] Result
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((ProjectInfo[])(this.results[0]));
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.33440")]
    public delegate void GetProjectInfoCompletedEventHandler(object sender, GetProjectInfoCompletedEventArgs e);

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.33440")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class GetProjectInfoCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
    {
        private object[] results;

        internal GetProjectInfoCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) :
            base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <remarks/>
        public ProjectInfo Result
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((ProjectInfo)(this.results[0]));
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.33440")]
    public delegate void GetAllWebCamInfoCompletedEventHandler(object sender, GetAllWebCamInfoCompletedEventArgs e);

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.33440")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class GetAllWebCamInfoCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
    {
        private object[] results;

        internal GetAllWebCamInfoCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) :
            base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <remarks/>
        public WebCamInfo[] Result
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((WebCamInfo[])(this.results[0]));
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.33440")]
    public delegate void GetDaysWithImagesCompletedEventHandler(object sender, GetDaysWithImagesCompletedEventArgs e);

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.33440")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class GetDaysWithImagesCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
    {
        private object[] results;

        internal GetDaysWithImagesCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) :
            base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <remarks/>
        public DaysWithImages Result
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((DaysWithImages)(this.results[0]));
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.33440")]
    public delegate void GetImageNearestCompletedEventHandler(object sender, GetImageNearestCompletedEventArgs e);

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.33440")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class GetImageNearestCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
    {
        private object[] results;

        internal GetImageNearestCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) :
            base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <remarks/>
        public ImageInfo Result
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((ImageInfo)(this.results[0]));
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.33440")]
    public delegate void GetImageTimesCompletedEventHandler(object sender, GetImageTimesCompletedEventArgs e);

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.33440")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class GetImageTimesCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
    {
        private object[] results;

        internal GetImageTimesCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) :
            base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <remarks/>
        public ImageTimes Result
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((ImageTimes)(this.results[0]));
            }
        }
    }
}

#pragma warning restore 1591