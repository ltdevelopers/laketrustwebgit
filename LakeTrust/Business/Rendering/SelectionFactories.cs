﻿using EPiServer.Shell.ObjectEditing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LakeTrust.Business.Rendering
{
    public class HeadingColorSelectionFactory : ISelectionFactory
    {
        public IEnumerable<ISelectItem> GetSelections(ExtendedMetadata metadata)
        {
            var listItems = new List<ISelectItem>();
            listItems.Add(new SelectItem());
            listItems.Add(new SelectItem() { Text = "Blue", Value = "ht-blue" });
            listItems.Add(new SelectItem() { Text = "Green", Value = "ht-green" });
            listItems.Add(new SelectItem() { Text = "Grey", Value = "ht-grey" });
            listItems.Add(new SelectItem() { Text = "Brown", Value = "ht-brown" });
            listItems.Add(new SelectItem() { Text = "Purple", Value = "ht-purple" });
            listItems.Add(new SelectItem() { Text = "Lightblue", Value = "ht-lightblue" });
            listItems.Add(new SelectItem() { Text = "Orange", Value = "ht-orange" });
            listItems.Add(new SelectItem() { Text = "Lightgreen", Value = "ht-lightgreen" });
            listItems.Add(new SelectItem() { Text = "Lightpurple", Value = "ht-lightpurple" });
            return listItems;
        }
    }

    public class HeadingSelectionFactory : ISelectionFactory
    {
        public IEnumerable<ISelectItem> GetSelections(ExtendedMetadata metadata)
        {
            var listItems = new List<ISelectItem>();
            listItems.Add(new SelectItem());
            for (int i = 2; i <= 6; i++)
                listItems.Add(new SelectItem()
                {
                    Value = string.Format("H{0}", i),
                    Text = string.Format("h{0}", i)
                });
            return listItems;
        }
    }
}