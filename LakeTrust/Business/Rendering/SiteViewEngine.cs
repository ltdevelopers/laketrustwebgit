using System.Linq;
using System.Web.Mvc;

namespace LakeTrust.Business.Rendering
{
    /// <summary>
    /// Extends the Razor view engine to include the folders ~/Views/Shared/Blocks/ and
    /// ~/Views/Shared/PagePartials/ when looking for partial views.
    /// </summary>
    public class SiteViewEngine : RazorViewEngine
    {
        private static readonly string[] AdditionalPartialViewFormats = new[]
        {
            TemplateCoordinator.BlockFolder + "{1}.cshtml",
            TemplateCoordinator.BlockFolder + "{0}.cshtml",
            TemplateCoordinator.PagePartialsFolder + "{0}.cshtml",
            TemplateCoordinator.PagePartialsFolder + "{1}.cshtml",
            TemplateCoordinator.MediaFolder + "{0}.cshtml"
        };

        private static readonly string[] AdditionViewPageTemplateFormats = new[]
        {
            TemplateCoordinator.PageFolder + "{1}.cshtml"
        };

        public SiteViewEngine()
        {
            this.ViewLocationFormats = ViewLocationFormats.Union(AdditionViewPageTemplateFormats).ToArray();
            this.PartialViewLocationFormats = PartialViewLocationFormats.Union(AdditionalPartialViewFormats).ToArray();
        }
    }
}