﻿using EPiServer.Shell;
using LakeTrust.Models.Pages;

namespace LakeTrust.Business.Rendering
{
    [UIDescriptorRegistration]
    public class ContainerPageUIDescriptor : UIDescriptor<Container>
    {
        public ContainerPageUIDescriptor()
            : base(EPiServer.Shell.ContentTypeCssClassNames.Container)
        {
            DefaultView = CmsViewNames.AllPropertiesView;
        }
    }
}