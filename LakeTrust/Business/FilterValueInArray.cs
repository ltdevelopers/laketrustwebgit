﻿using EPiServer.Core;
using EPiServer.Filters;
using System;
using System.Linq;

namespace LakeTrust.Business
{
    public class FilterValueInArray : IPageFilter
    {
        public string PropertyArrayName { get; set; }

        public string Value { get; set; }

        public FilterValueInArray(string propertyArrayName, string value)
        {
            this.PropertyArrayName = propertyArrayName;
            this.Value = value;
        }

        public void Filter(PageDataCollection pages)
        {
            for (int i = pages.Count - 1; i >= 0; i--)
            {
                if (pages[i][this.PropertyArrayName] == null)
                {
                    pages.RemoveAt(i);
                    continue;
                }

                var array = pages[i][this.PropertyArrayName].ToString().Split(',')
                    .Select(x => x.ToLower());
                if (!array.Contains(this.Value.ToLower()))
                    pages.RemoveAt(i);
            }
        }

        public void Filter(object sender, FilterEventArgs e)
        {
            this.Filter(e.Pages);
        }

        public bool ShouldFilter(PageData page)
        {
            throw new NotImplementedException();
        }
    }
}