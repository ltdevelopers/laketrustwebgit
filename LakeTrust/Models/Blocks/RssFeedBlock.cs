﻿using EPiServer;
using EPiServer.DataAbstraction;
using System.ComponentModel.DataAnnotations;

namespace LakeTrust.Models.Blocks
{
    [SiteContentType(DisplayName = "Rss Feed", GUID = "76348ecd-964c-400c-9c18-11b804d50a51", Description = "Displays a list of Rss items based on the Rss url")]
    [SiteImageUrl]
    public class RssFeedBlock : SiteBlockData
    {
        public override void SetDefaultValues(ContentType contentType)
        {
            base.SetDefaultValues(contentType);
            this.NumberOfItemsToShow = 5;
        }

        [Display(Name = "Rss Url", Order = 10, Description = "The url for the Rss Feed")]
        public virtual Url RssUrl { get; set; }

        [Display(Name = "Number of Items to show", Description = "The number of items to show in this listing.", GroupName = SystemTabNames.Content, Order = 30)]
        [Editable(true)]
        public virtual int NumberOfItemsToShow { get; set; }
    }
}