﻿using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.Filters;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace LakeTrust.Models.Blocks
{
    /// <summary>
    /// Used to insert a list of pages, for example a news list
    /// </summary>
    [SiteContentType(GUID = "57879212-3d0f-4c73-9128-896605fcf1a5", DisplayName = "Page List")]
    [SiteImageUrl]
    public class PageListBlock : SiteBlockData
    {
        #region IInitializableContent

        /// <summary>
        /// Sets the default property values on the content data.
        /// </summary>
        /// <param name="contentType">Type of the content.</param>
        public override void SetDefaultValues(ContentType contentType)
        {
            base.SetDefaultValues(contentType);

            Count = 3;
            IncludeIntroduction = true;
            IncludePublishDate = false;
            SortOrder = FilterSortOrder.PublishedDescending;
            this.DisplayPhoto = true;
        }

        #endregion IInitializableContent

        [Display(Name = "Include Publish Date", GroupName = SystemTabNames.Content, Order = 20)]
        [DefaultValue(false)]
        public virtual bool IncludePublishDate { get; set; }

        /// <summary>
        /// Gets or sets whether a page introduction/description should be included in the list
        /// </summary>
        [Display(Name = "Include Summary", GroupName = SystemTabNames.Content, Order = 30)]
        [DefaultValue(true)]
        public virtual bool IncludeIntroduction { get; set; }

        [Display(Name = "Display Image", GroupName = SystemTabNames.Content, Order = 35)]
        [DefaultValue(true)]
        public virtual bool DisplayPhoto { get; set; }

        [Display(Name = "Number of Items", GroupName = SystemTabNames.Content, Order = 40)]
        [DefaultValue(3)]
        [Required]
        public virtual int Count { get; set; }

        [Display(Name = "Order By", GroupName = SystemTabNames.Content, Order = 50)]
        [DefaultValue(FilterSortOrder.PublishedDescending)]
        [UIHint("SortOrder")]
        [BackingType(typeof(PropertyNumber))]
        public virtual FilterSortOrder SortOrder { get; set; }

        [Display(Name = "Select Root Page", GroupName = SystemTabNames.Content, Order = 60)]
        [Required]
        public virtual PageReference Root { get; set; }

        [Display(Name = "Filter By Page Type", GroupName = SystemTabNames.Content, Order = 70)]
        public virtual PageType PageTypeFilter { get; set; }

        [Display(Name = "Only In These Categories", GroupName = SystemTabNames.Content, Order = 7)]
        public virtual CategoryList CategoryFilter { get; set; }

        [Display(GroupName = SystemTabNames.Content, Order = 80)]
        public virtual bool Recursive { get; set; }
    }
}