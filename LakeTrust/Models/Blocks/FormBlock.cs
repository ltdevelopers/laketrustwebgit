﻿using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.XForms;
using System.ComponentModel.DataAnnotations;

namespace LakeTrust.Models.Blocks
{
    [SiteContentType(GroupName = Global.GroupNames.Forms, DisplayName = "Form", GUID = "49fcf8c3-0710-49ff-ade1-81b26e3723bc", Description = ""), SiteImageUrl]
    public class FormBlock : SiteBlockData
    {
        [Display(GroupName = SystemTabNames.Content, Order = 1)]
        public virtual string Heading { get; set; }

        [Display(GroupName = SystemTabNames.Content, Order = 2)]
        public virtual XForm BlockForm { get; set; }

        [Ignore]
        public virtual string ActionUri { get; set; }
    }
}