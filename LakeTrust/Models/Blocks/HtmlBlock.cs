﻿using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.Shell.ObjectEditing;
using EPiServer.Shell.ObjectEditing.EditorDescriptors;
using EPiServer.Web;
using LakeTrust.Business.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace LakeTrust.Models.Blocks
{
    [SiteContentType(DisplayName = "Html Block", GUID = "fc0e3eda-131c-4877-b779-69da927782fd", Description = "A block that allows you to add html, images, etc...")]
    [SiteImageUrl]
    public class HtmlBlock : SiteBlockData, ICustomCssInContentArea
    {
        // Allows us to define our own Dropdown options from code.(UIHint)
        [Display(Name = "Background Position", Order = 20, Description = "The background placement for the backgrou image of this block")]
        [UIHint(Global.SiteUIHints.BackgroundPlacementMultiple)]
        public virtual string BackgroundPlacement { get; set; }

        [Display(Name = "Background Image", Order = 30, Description = "The background image for this block")]
        [UIHint(UIHint.Image)]
        public virtual Url BackgroundImage { get; set; }

        [Display(Name = "Make Whole Block Clickable", GroupName = Global.GroupNames.LinkSettings, Order = 33, Description = "If Checked, the whole block will be clickable")]
        public virtual bool IsBlockLink { get; set; }

        [Display(Name = "Open In New Window", GroupName = Global.GroupNames.LinkSettings, Order = 33, Description = "If Checked, the link will open in a new window")]
        public virtual bool OpenInNewWindow { get; set; }

        [Display(Name = "Block Url", Order = 36, GroupName = Global.GroupNames.LinkSettings, Description = "Sets the url for the whole block being clickable")]
        public virtual Url BlockUrl { get; set; }

        [Editable(true)]
        [Display(Name = "Content", Description = "Html Content", GroupName = SystemTabNames.Content, Order = 40)]
        public virtual XhtmlString Content { get; set; }

        public string ContentAreaCssClass
        {
            get { return string.Empty; }
        }
    }

    public class BackgroundSelectionFactory : ISelectionFactory
    {
        public IEnumerable<ISelectItem> GetSelections(ExtendedMetadata metadata)
        {
            var backgroundOptions = new List<SelectItem>();
            backgroundOptions.Add(new SelectItem());
            backgroundOptions.Add(new SelectItem() { Text = "Top Left", Value = "tl" });
            backgroundOptions.Add(new SelectItem() { Text = "Top Center", Value = "tc" });
            backgroundOptions.Add(new SelectItem() { Text = "Top Right", Value = "tr" });
            backgroundOptions.Add(new SelectItem() { Text = "Center Left", Value = "cl" });
            backgroundOptions.Add(new SelectItem() { Text = "Center Center", Value = "cc" });
            backgroundOptions.Add(new SelectItem() { Text = "Center Right", Value = "cr" });
            backgroundOptions.Add(new SelectItem() { Text = "Bottom Left", Value = "bl" });
            backgroundOptions.Add(new SelectItem() { Text = "Bottom Center", Value = "bc" });
            backgroundOptions.Add(new SelectItem() { Text = "Bottom Right", Value = "br" });

            return backgroundOptions;
        }
    }

    [EditorDescriptorRegistration(TargetType = typeof(string), UIHint = Global.SiteUIHints.BackgroundPlacementMultiple)]
    public class BackgroundMultipleEditorDescriptor : EditorDescriptor
    {
        public override void ModifyMetadata(ExtendedMetadata metadata, IEnumerable<Attribute> attributes)
        {
            SelectionFactoryType = typeof(BackgroundSelectionFactory);

            ClientEditingClass = "epi.cms.contentediting.editors.SelectionEditor";

            base.ModifyMetadata(metadata, attributes);
        }
    }
}