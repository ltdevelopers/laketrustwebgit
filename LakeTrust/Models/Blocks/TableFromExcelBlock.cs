﻿using EPiServer.Core;
using EPiServer.Web;
using System;
using System.ComponentModel.DataAnnotations;

namespace LakeTrust.Models.Blocks
{
    [SiteContentType(DisplayName = "Table From Excel", GUID = "487f394e-7c80-4f3a-aba0-2793e9828c43", Description = "A block used to display data in table format from an excel file")]
    [SiteImageUrl]
    public class TableFromExcelBlock : SiteBlockData
    {
        [Display(Name = "Table Caption", Order = 2, Description = "The title of the excel document")]
        public virtual string TableCaption { get; set; }

        [Display(Name = "Excel File", Order = 1)]
        [UIHint(UIHint.MediaFile)]
        public virtual ContentReference ExcelDocument { get; set; }
    }
}