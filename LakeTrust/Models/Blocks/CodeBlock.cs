﻿using System;
using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.Web;

namespace LakeTrust.Models.Blocks
{
    [ContentType(DisplayName = "Code Block", GUID = "ba8aa427-5128-4317-b97d-0dc5c67d0696", Description = "")]
    public class CodeBlock : SiteBlockData
    {
        [Display(Name = "Code", Order = 1)]
        [UIHint(UIHint.Textarea)]
        public virtual string Code { get; set; }

        [ScaffoldColumn(false)]
        public override bool DisplayWithBorder { get; set; }

        [ScaffoldColumn(false)]
        public override bool DisplayWithFadeBackground { get; set; }

        [ScaffoldColumn(false)]
        public override string BlockTitle { get; set; }

        [ScaffoldColumn(false)]
        public override string BlockTitleCustomTag { get; set; }

        [ScaffoldColumn(false)]
        public override string BlockTitleColor { get; set; }
    }
}