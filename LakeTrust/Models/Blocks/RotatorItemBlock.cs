﻿using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.Web;
using System;
using System.ComponentModel.DataAnnotations;

namespace LakeTrust.Models.Blocks
{
    [SiteContentType(DisplayName = "Rotator Item Block", GUID = "d273e357-d56e-4a3b-a426-3d04ac2fcb8b", Description = "")]
    [SiteImageUrl]
    public class RotatorItemBlock : BlockData
    {
        public override void SetDefaultValues(ContentType contentType)
        {
            base.SetDefaultValues(contentType);
            this.SlideButtonText = "Learn More";
        }

        [Display(Name = "Slide Title", Order = 10, Description = "The title for the slide.", GroupName = SystemTabNames.Content)]
        public virtual String SlideTitle { get; set; }

        [Display(Name = "Slide Sub Title", Order = 20, Description = "The sub title for the slide.", GroupName = SystemTabNames.Content)]
        public virtual String SlideSubTitle { get; set; }

        [Display(Name = "Slide Image", Order = 30, Description = "The sub title for the slide.", GroupName = SystemTabNames.Content)]
        [UIHint(UIHint.Image)]
        public virtual Url SlideImage { get; set; }

        [Display(Name = "Slide Alt Text", Order = 35, Description = "The alternate text for this slide.", GroupName = SystemTabNames.Content)]
        public virtual string SlideAltText { get; set; }

        [Display(Name = "Slide Link", Order = 40, Description = "The page where the learn more link is linked to.", GroupName = SystemTabNames.Content)]
        public virtual ContentReference SlideLink { get; set; }

        [Display(Name = "Slide Button Text", Order = 45, Description = "The Text used on the slider button.", GroupName = SystemTabNames.Content)]
        public virtual string SlideButtonText { get; set; }

        [Display(Name = "Align Right", Order = 50, Description = "When checked, the caption will be aligned to the right of the image.", GroupName = SystemTabNames.Content)]
        public virtual bool AlignRight { get; set; }
    }
}