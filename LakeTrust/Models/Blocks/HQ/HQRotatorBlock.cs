﻿using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.Shell.ObjectEditing;
using LakeTrust.Business.Rendering;
using System;
using System.ComponentModel.DataAnnotations;

namespace LakeTrust.Models.Blocks.HQ
{
    [HQSiteContentType(DisplayName = "Rotator", GUID = "2e5b876d-b81d-43a9-8007-f0c230275c7b", Description = "A rotator block Item")]
    [SiteImageUrl("~/Content/Images/GroupImages/HQ.gif")]
    public class HQRotatorBlock : HQBaseBlockData, ICustomCssInContentArea
    {
        public override void SetDefaultValues(ContentType contentType)
        {
            base.SetDefaultValues(contentType);
            this.ContentAreaCssClass = "white";
        }

        [Display(Name = "Rotator Items", Order = 1)]
        [AllowedTypes(new Type[] { typeof(HQRotatorItemBlock) })]
        public virtual ContentArea RotatorContentArea { get; set; }
    }
}