﻿using EPiServer;
using System.ComponentModel.DataAnnotations;

namespace LakeTrust.Models.Blocks.HQ
{
    [HQSiteContentType(DisplayName = "Link", GUID = "e5fccf70-2366-473c-ad29-d7bab974a039", Description = "Will display a link to media, Http, or image")]
    [SiteImageUrl("~/Content/Images/GroupImages/HQ.gif")]
    public class HQLinkBlock : HQBaseBlockData
    {
        [Display(Name = "Link Text", Description = "Will display a link", Order = 1)]
        [Required]
        public virtual string LinkText { get; set; }

        [Display(Name = "Link", Description = "Will display a link", Order = 2)]
        [Required]
        public virtual Url Link { get; set; }
    }
}