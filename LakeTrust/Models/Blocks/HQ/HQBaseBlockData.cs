﻿using EPiServer.Core;
using EPiServer.Shell.ObjectEditing;
using LakeTrust.Business.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace LakeTrust.Models.Blocks.HQ
{
    public class HQBaseBlockData : BlockData, IHQDataItem
    {
        [Display(Name = "Background Color", Description = "Will display the background color of the block based on your selection", Order = 600)]
        [SelectOne(SelectionFactoryType = typeof(ColorSelectionFactory))]
        public virtual string ContentAreaCssClass { get; set; }

        [Display(Name = "Navigation Anchor", Description = "If checked, this will add a new navigation item to the menu.", Order = 550)]
        public virtual bool IsNavigationPoint { get; set; }

        [Display(Name = "Navigation Anchor Title", Description = "The title of the navigational Anchor.", Order = 549)]
        public virtual string NavigationAnchorTitle { get; set; }
    }

    public class ColorSelectionFactory : ISelectionFactory
    {
        public IEnumerable<ISelectItem> GetSelections(ExtendedMetadata metadata)
        {
            return new ISelectItem[] {
                new SelectItem() { Text = "Green", Value = "green" },
                new SelectItem() { Text = "Purple", Value = "purple" },
                new SelectItem() { Text = "Gray", Value = "gray" },
                new SelectItem() { Text = "Red", Value = "red" },
                new SelectItem() { Text = "Blue", Value = "blue" },
                new SelectItem() { Text = "Brown", Value = "brown" },
                new SelectItem() { Text = "Light Blue", Value = "lightblue" },
                new SelectItem() { Text = "White", Value = "white" }
            };
        }
    }
}