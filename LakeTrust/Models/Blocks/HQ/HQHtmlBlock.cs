﻿using EPiServer.Core;
using EPiServer.DataAbstraction;
using LakeTrust.Business.Rendering;
using System.ComponentModel.DataAnnotations;

namespace LakeTrust.Models.Blocks.HQ
{
    [HQSiteContentType(DisplayName = "Html Content", GUID = "e87c5ba5-abff-4984-9b28-bfffabd73d92", Description = "")]
    [SiteImageUrl("~/Content/Images/GroupImages/HQ.gif")]
    public class HQHtmlBlock : HQBaseBlockData, ICustomCssInContentArea
    {
        [Display(Name = "Title", Order = 10, Description = "The title of this item")]
        public virtual string BlockTitle { get; set; }

        [Display(Name = "Content", Description = "Html Content", GroupName = SystemTabNames.Content, Order = 40)]
        [Editable(true)]
        public virtual XhtmlString Content { get; set; }

        [Display(Name = "Make Standard Html Block", Description = "Will be blue text, gray text with white background", GroupName = SystemTabNames.Content, Order = 50)]
        [Editable(true)]
        public virtual bool StandardHtmlBlock { get; set; }
    }
}