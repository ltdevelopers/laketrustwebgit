﻿using EPiServer.DataAbstraction;
using EPiServer.Web;
using LakeTrust.Business.Rendering;
using System.ComponentModel.DataAnnotations;

namespace LakeTrust.Models.Blocks.HQ
{
    [HQSiteContentType(DisplayName = "Question Answer", GUID = "01a282da-b44c-4315-8dba-fae61fd8ccec", Description = "")]
    [SiteImageUrl("~/Content/Images/GroupImages/HQ.gif")]
    public class HQQuestionAnswerBlock : HQBaseBlockData, ICustomCssInContentArea
    {
        public override void SetDefaultValues(ContentType contentType)
        {
            base.SetDefaultValues(contentType);
            this.ContentAreaCssClass = "blue";
        }

        [Display(Name = "Question", Order = 1, Description = "The question for this block")]
        [UIHint(UIHint.Textarea)]
        public virtual string Question { get; set; }

        [Display(Name = "Answer", Order = 2, Description = "The answer for this block")]
        [UIHint(UIHint.Textarea)]
        public virtual string Answer { get; set; }
    }
}