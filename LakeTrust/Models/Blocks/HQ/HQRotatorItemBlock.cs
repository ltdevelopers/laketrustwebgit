﻿using EPiServer.Core;
using EPiServer.DataAnnotations;
using EPiServer.Web;
using System.ComponentModel.DataAnnotations;

namespace LakeTrust.Models.Blocks.HQ
{
    [HQSiteContentType(DisplayName = "Rotator Item", GUID = "3a695fb7-52aa-4a91-98ff-fc573f5ed987", Description = "A single representation of a rotator slide")]
    [SiteImageUrl("~/Content/Images/GroupImages/HQ.gif")]
    public class HQRotatorItemBlock : BlockData
    {
        [Display(Name = "Slide Image", Order = 1)]
        [UIHint(UIHint.Image)]
        public virtual ContentReference SlideImage { get; set; }

        [Display(Name = "Slide Text", Order = 2)]
        [UIHint(UIHint.Textarea)]
        public virtual string SlideCopy { get; set; }

        [Ignore]
        public string ActiveClass { get; set; }
    }
}