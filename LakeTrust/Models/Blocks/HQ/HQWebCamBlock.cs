﻿using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using LakeTrust.Business.Rendering;
using System;
using System.ComponentModel.DataAnnotations;

namespace LakeTrust.Models.Blocks.HQ
{
    [HQSiteContentType(DisplayName = "Web Cam", GUID = "24361957-e4a4-4d7b-9147-ebff1c905dc9", Description = "")]
    [SiteImageUrl("~/Content/Images/GroupImages/HQ.gif")]
    public class HQWebCamBlock : HQBaseBlockData, ICustomCssInContentArea
    {
    }
}