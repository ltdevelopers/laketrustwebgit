﻿using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.XForms;
using LakeTrust.Business.Rendering;
using System.ComponentModel.DataAnnotations;

namespace LakeTrust.Models.Blocks.HQ
{
    [HQSiteContentType(DisplayName = "Form", GUID = "6c281523-e1a9-4abd-96c2-df7f8e1a56ad", Description = "")]
    [SiteImageUrl("~/Content/Images/GroupImages/HQ.gif")]
    public class HQFormBlock : HQBaseBlockData, ICustomCssInContentArea
    {
        public override void SetDefaultValues(ContentType contentType)
        {
            base.SetDefaultValues(contentType);
            this.ContentAreaCssClass = "white";
        }

        [Display(Name = "Form", Order = 31, Description = "The form for this block")]
        public virtual XForm Form { get; set; }

        [Display(Name = "Success Message", Order = 30, Description = "The successful message for this form")]
        public virtual XhtmlString SuccessMessage { get; set; }

        [Ignore]
        public virtual string ActionUri { get; set; }
    }
}