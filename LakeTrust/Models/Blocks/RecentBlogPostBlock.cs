﻿using EPiServer.Core;
using EPiServer.DataAbstraction;
using System;
using System.ComponentModel.DataAnnotations;

namespace LakeTrust.Models.Blocks
{
    [SiteContentType(DisplayName = "Recent Blog Post", AvailableInEditMode = false, GroupName = Global.GroupNames.Blog, GUID = "1482f706-c978-4276-a451-116f7877db1c", Description = "Displays the most recent blog posts based on the number set.  Default is 1.")]
    [SiteImageUrl]
    public class RecentBlogPostBlock : SiteBlockData
    {
        public override void SetDefaultValues(ContentType contentType)
        {
            base.SetDefaultValues(contentType);
            this.NumberOfItemsToShow = 1;
        }

        [Display(Name = "Blog Root", Description = "Displays the recent posts from below this blog page.", GroupName = SystemTabNames.Content, Order = 20)]
        [Editable(true)]
        public virtual ContentReference BlogRootPage { get; set; }

        [Display(Name = "Number of Items to show", Description = "The number of items to show in this listing.", GroupName = SystemTabNames.Content, Order = 30)]
        [Editable(true)]
        public virtual int NumberOfItemsToShow { get; set; }
    }
}