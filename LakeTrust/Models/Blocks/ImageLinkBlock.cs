﻿using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.Web;
using System;
using System.ComponentModel.DataAnnotations;

namespace LakeTrust.Models.Blocks
{
    [SiteContentType(DisplayName = "Image Link", GUID = "46515beb-ed39-4415-8054-7b68b12346ca", Description = "Surrounds an image with a link")]
    [SiteImageUrl]
    public class ImageLinkBlock : SiteBlockData
    {
        [Display(Order = 1)]
        [UIHint(UIHint.Image)]
        public virtual Url Image { get; set; }

        [Display(Name = "Url Link", Order = 2)]
        public virtual Url UrlLink { get; set; }

        /*Overrides*/

        [ScaffoldColumn(false)]
        public override string BlockTitle { get; set; }

        [ScaffoldColumn(false)]
        public override string BlockTitleCustomTag { get; set; }

        [ScaffoldColumn(false)]
        public override string BlockTitleColor { get; set; }

        [ScaffoldColumn(false)]
        public override bool DisplayWithFadeBackground { get; set; }

        [ScaffoldColumn(false)]
        public override bool DisplayWithBorder { get; set; }
    }
}