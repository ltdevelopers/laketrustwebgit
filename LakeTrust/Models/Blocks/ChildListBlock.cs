﻿using EPiServer.Core;
using EPiServer.DataAbstraction;
using System.ComponentModel.DataAnnotations;

namespace LakeTrust.Models.Blocks
{
    [SiteContentType(DisplayName = "Child Listing", GUID = "e77fac1a-9791-403a-a239-77e4fe69efe5", Description = "Will display a list of child pages based on the page you select")]
    [SiteImageUrl]
    public class ChildListBlock : SiteBlockData
    {
        public override void SetDefaultValues(ContentType contentType)
        {
            base.SetDefaultValues(contentType);
            this.DescriptionLength = 100;
            this.DisplayDescription = false;
        }

        [Display(Name = "Parent Page", Description = "The page used for displaying child pages", GroupName = SystemTabNames.Content, Order = 20)]
        public virtual ContentReference ParentPageReference { get; set; }

        [Display(Name = "Display Description", Order = 30, GroupName = SystemTabNames.Content, Description = "If checked, this will display the description of the page")]
        public virtual bool DisplayDescription { get; set; }

        [Display(Name = "Description Length", Order = 30, GroupName = SystemTabNames.Content, Description = "The length of the description in character count.")]
        public virtual int DescriptionLength { get; set; }
    }
}