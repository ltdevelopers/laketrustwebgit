﻿using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.Shell.ObjectEditing;
using LakeTrust.Business.Rendering;
using System.ComponentModel.DataAnnotations;

namespace LakeTrust.Models.Blocks
{
    public abstract class SiteBlockData : BlockData
    {
        public override void SetDefaultValues(ContentType contentType)
        {
            base.SetDefaultValues(contentType);

            this.DisplayWithBorder = false;
            this.DisplayWithFadeBackground = false;
            this.BlockTitleCustomTag = "h3";
        }

        [Display(Name = "Display Border", Order = 1, GroupName = Global.GroupNames.DisplaySettings, Description = "Will display this block with a border")]
        public virtual bool DisplayWithBorder { get; set; }

        [Display(Name = "Display Background", Order = 2, GroupName = Global.GroupNames.DisplaySettings, Description = "Will display this block with faded background")]
        public virtual bool DisplayWithFadeBackground { get; set; }

        [Display(Name = "Title", Order = 1, Description = "The title of this item")]
        public virtual string BlockTitle { get; set; }

        [Display(Name = "Title Tag", Order = 2, Description = "The tag the title will be rendered as")]
        [SelectOne(SelectionFactoryType = typeof(HeadingSelectionFactory))]
        public virtual string BlockTitleCustomTag { get; set; }

        [Display(Name = "Title Color", Order = 3, Description = "The color of this block title")]
        [SelectOne(SelectionFactoryType = typeof(HeadingColorSelectionFactory))]
        public virtual string BlockTitleColor { get; set; }

        [Ignore]
        public string WidgetClasses { get; set; }
    }
}