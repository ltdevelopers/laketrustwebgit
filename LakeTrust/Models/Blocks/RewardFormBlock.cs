﻿using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.Shell.ObjectEditing;
using EPiServer.Web;
using System;
using System.ComponentModel.DataAnnotations;

namespace LakeTrust.Models.Blocks
{
    [SiteContentType(DisplayName = "Reward Form Block", GUID = "cae5820f-02f0-4534-b9c6-2a897ff8b1d5", Description = "", GroupName = Global.GroupNames.Specialized)]
    [SiteImageUrl]
    public class RewardFormBlock : SiteBlockData
    {
        [Display(Name = "Services Title", Order = 1)]
        public virtual string ServicesTitle { get; set; }

        [Display(Name = "Services Form Items", Order = 2)]
        [AllowedTypes(new[] { typeof(RewardFormItemBlock) })]
        public virtual ContentArea ServicesContentArea { get; set; }

        [Display(Name = "More Services Title", Order = 3)]
        public virtual string MoreServicesTitle { get; set; }

        [Display(Name = "More Services Form Items", Order = 4)]
        [AllowedTypes(new[] { typeof(RewardFormItemBlock) })]
        public virtual ContentArea MoreServicesContentArea { get; set; }

        [Display(Name = "Questions Title", Order = 5)]
        public virtual string QuestionsTitle { get; set; }

        [Display(Name = "Questions Form Items", Order = 6)]
        [AllowedTypes(new[] { typeof(RewardFormItemBlock) })]
        public virtual ContentArea QuestionsContentArea { get; set; }

        [Display(Name = "Results Title", Order = 7)]
        public virtual string ResultsTitle { get; set; }

        [Display(Name = "No Rewards Text", Order = 8)]
        [UIHint(UIHint.Textarea)]
        public virtual string NoRewardsText { get; set; }

        [Display(Name = "Default Results Text", Order = 9)]
        [UIHint(UIHint.Textarea)]
        public virtual string DefaultResultsText
        {
            get
            {
                var noResults = this.GetPropertyValue(x => x.DefaultResultsText);
                return !string.IsNullOrWhiteSpace(noResults) ? noResults : "Sorry, but you will not receive any rewards.";
            }
            set { this.SetPropertyValue(x => x.DefaultResultsText, value); }
        }

        [ScaffoldColumn(false)]
        public override string BlockTitle { get; set; }

        [ScaffoldColumn(false)]
        public override string BlockTitleColor { get; set; }

        [ScaffoldColumn(false)]
        public override string BlockTitleCustomTag { get; set; }

        [ScaffoldColumn(false)]
        public override bool DisplayWithBorder { get; set; }

        [ScaffoldColumn(false)]
        public override bool DisplayWithFadeBackground { get; set; }
    }
}