﻿using EPiServer.DataAbstraction;
using System.ComponentModel.DataAnnotations;

namespace LakeTrust.Models.Blocks
{
    [SiteContentType(DisplayName = "Event List", GUID = "01827b23-4b73-4e72-9306-371a7706a6d7", Description = "")]
    [SiteImageUrl]
    public class EventListBlock : SiteBlockData
    {
        public override void SetDefaultValues(ContentType contentType)
        {
            base.SetDefaultValues(contentType);
            this.NumberOfItemsToShow = 5;
        }

        [Display(Name = "Number of Items to show", Description = "The number of items to show in this listing.", GroupName = SystemTabNames.Content, Order = 30)]
        [Editable(true)]
        public virtual int NumberOfItemsToShow { get; set; }
    }
}