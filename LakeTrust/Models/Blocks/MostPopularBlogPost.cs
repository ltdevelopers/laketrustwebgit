﻿using EPiServer.Core;
using EPiServer.DataAbstraction;
using System;
using System.ComponentModel.DataAnnotations;

namespace LakeTrust.Models.Blocks
{
    [SiteContentType(DisplayName = "Most Popular Blog Post", GroupName = Global.GroupNames.Blog, GUID = "3a1449c3-3aaf-4672-b0af-1366a99f8d2c", Description = "")]
    [SiteImageUrl]
    public class MostPopularBlogPost : SiteBlockData
    {
        [Display(Name = "Parent Page", Description = "The blog that you want to query most popular from", GroupName = SystemTabNames.Content, Order = 20)]
        public virtual ContentReference BlogPageReference { get; set; }

        [Display(Name = "Number To Take", Description = "The number of popular posts to take", GroupName = SystemTabNames.Content, Order = 30)]
        public virtual int NumberToTake { get; set; }
    }
}