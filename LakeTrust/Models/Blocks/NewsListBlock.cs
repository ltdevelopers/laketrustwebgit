﻿using EPiServer.DataAbstraction;
using System.ComponentModel.DataAnnotations;

namespace LakeTrust.Models.Blocks
{
    [SiteContentType(DisplayName = "News List", GUID = "056a6980-3c48-464b-83e1-9a1dba11482c", Description = "Displays a listing of news items.  Default view is 5 items")]
    [SiteImageUrl]
    public class NewsListBlock : SiteBlockData
    {
        public override void SetDefaultValues(ContentType contentType)
        {
            base.SetDefaultValues(contentType);
            this.NumberOfItemsToShow = 5;
        }

        [Display(Name = "Number of Items to show", Description = "The number of items to show in this listing.", GroupName = SystemTabNames.Content, Order = 30)]
        [Editable(true)]
        public virtual int NumberOfItemsToShow { get; set; }
    }
}