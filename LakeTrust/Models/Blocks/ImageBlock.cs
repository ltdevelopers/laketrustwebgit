﻿using EPiServer;
using EPiServer.Web;
using System.ComponentModel.DataAnnotations;

namespace LakeTrust.Models.Blocks
{
    [SiteContentType(DisplayName = "Image Block", GUID = "025c9527-e89e-4596-9ec6-9a68e2f43bd9", Description = "")]
    [SiteImageUrl]
    public class ImageBlock : SiteBlockData
    {
        [Display(Name = "Image", Order = 30, Description = "The image for this block")]
        [UIHint(UIHint.Image)]
        public virtual Url Image { get; set; }

        [Display(Name = "Alt Text", Order = 30, Description = "The alt text for this image")]
        public virtual string ImageAlt { get; set; }

        [Display(Name = "Is Responsive Image", Order = 40, Description = "Will allow this image to be responsive")]
        public virtual bool ImageIsResponsive { get; set; }
    }
}