﻿using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using System;
using System.ComponentModel.DataAnnotations;

namespace LakeTrust.Models.Blocks
{
    [SiteContentType(DisplayName = "Rewards Form Item", GUID = "bec93cd5-7393-4111-8e2d-dcd01d2f7b32", Description = "A single form item for calculating rewards", GroupName = Global.GroupNames.Specialized)]
    [SiteImageUrl]
    public class RewardFormItemBlock : SiteBlockData
    {
        [Display(Name = "Question", Order = 1)]
        public virtual string Question { get; set; }

        [Display(Name = "Description", Order = 2)]
        public virtual string Description { get; set; }

        [Display(Name = "Question Link", Order = 3)]
        public virtual Url QuestionLink { get; set; }

        [Display(Name = "Points", Order = 4)]
        public virtual Double Points { get; set; }

        [Display(Name = "Weighted Points", Order = 5)]
        public virtual Double WeightedPionts { get; set; }

        [ScaffoldColumn(false)]
        public override string BlockTitle { get; set; }

        [ScaffoldColumn(false)]
        public override string BlockTitleColor { get; set; }

        [ScaffoldColumn(false)]
        public override string BlockTitleCustomTag { get; set; }

        [ScaffoldColumn(false)]
        public override bool DisplayWithBorder { get; set; }

        [ScaffoldColumn(false)]
        public override bool DisplayWithFadeBackground { get; set; }
    }
}