﻿using EPiServer.DataAbstraction;
using System.ComponentModel.DataAnnotations;

namespace LakeTrust.Models.Blocks
{
    [SiteContentType(DisplayName = "Twitter Feed", GUID = "d12588d8-7799-4bb8-8d72-ddc2ec3e435c", Description = "")]
    [SiteImageUrl]
    public class TwitterFeedBlock : SiteBlockData
    {
        public override void SetDefaultValues(ContentType contentType)
        {
            base.SetDefaultValues(contentType);
            this.NumberOfTwitterItems = 3;
        }

        [Display(Name = "Twitter Username", Order = 60, Description = "Twitter Username", GroupName = Global.GroupNames.TwitterSettings)]
        public virtual string TwitterUserName { get; set; }

        [Display(Name = "Number of Twitter Items", Description = "The number of twitter items shown in this feed", GroupName = Global.GroupNames.TwitterSettings, Order = 10)]
        [Range(1, 10)]
        [Required]
        public virtual int NumberOfTwitterItems { get; set; }

        [Editable(false)]
        [ScaffoldColumn(false)]
        public override string BlockTitleCustomTag { get; set; }
    }
}