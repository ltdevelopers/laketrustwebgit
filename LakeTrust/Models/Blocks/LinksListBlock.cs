﻿using EPiServer.DataAbstraction;
using EPiServer.SpecializedProperties;
using System.ComponentModel.DataAnnotations;

namespace LakeTrust.Models.Blocks
{
    [SiteContentType(DisplayName = "Links List", Description = "A block that allows you to select pages or urls")]
    [SiteImageUrl]
    public class LinksListBlock : SiteBlockData
    {
        [Display(Name = "Links", Description = "Select The Pages or urls you would like displayed.", GroupName = SystemTabNames.Content, Order = 30)]
        [Editable(true)]
        public virtual LinkItemCollection Links { get; set; }
    }
}