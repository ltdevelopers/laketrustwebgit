﻿using EPiServer.DataAnnotations;
using EPiServer.XForms;
using System;
using System.ComponentModel.DataAnnotations;

namespace LakeTrust.Models.Blocks
{
    [SiteContentType(DisplayName = "Poll Result", GroupName = Global.GroupNames.Forms, GUID = "34693c1e-cdec-4ec9-b34c-5afa6e83e1c4", Description = "Displays result of poll data")]
    [SiteImageUrl]
    public class PollResultBlock : SiteBlockData
    {
        [Display(Name = "Results for form", Order = 1, GroupName = Global.GroupNames.Forms)]
        public virtual XForm FormPage { get; set; }

        [Display(Name = "Poll Results Title", Order = 2)]
        public virtual string PollResultsTitle { get; set; }

        [Ignore]
        public virtual string ActionUri { get; set; }
    }
}