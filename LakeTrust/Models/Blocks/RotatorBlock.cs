﻿using EPiServer.Core;
using System.ComponentModel.DataAnnotations;

namespace LakeTrust.Models.Blocks
{
    [SiteContentType(DisplayName = "Rotator Block", GUID = "8f1c4f9e-7b25-4067-9dbb-fdd41bb3a4d1", Description = "")]
    [SiteImageUrl]
    public class RotatorBlock : BlockData
    {
        [Display(Name = "Rotator Block Items", Description = "This content area will display slides based on block items drop here.", GroupName = Global.GroupNames.Blocks, Order = 50)]
        public virtual ContentArea RotatorBlockItems { get; set; }
    }
}