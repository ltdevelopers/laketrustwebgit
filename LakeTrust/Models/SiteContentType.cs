﻿using EPiServer.DataAnnotations;

namespace LakeTrust.Models
{
    /// <summary>
    /// Attribute used for site content types to set default attribute values
    /// </summary>
    public class SiteContentType : ContentTypeAttribute
    {
        public SiteContentType()
        {
            GroupName = Global.GroupNames.Default;
        }
    }

    public class HQSiteContentType : ContentTypeAttribute
    {
        public HQSiteContentType()
        {
            GroupName = Global.GroupNames.HQ;
        }
    }

    public class BlogContentType : ContentTypeAttribute
    {
        public BlogContentType()
        {
            GroupName = Global.GroupNames.Blog;
        }
    }
}