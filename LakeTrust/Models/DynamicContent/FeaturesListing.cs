﻿using EPiServer.DynamicContent;
using LakeTrust.Models.Properties;
using System.Linq;
using System.Web.UI;

namespace LakeTrust.Models.DynamicContent
{
    [DynamicContentPlugIn(DisplayName = "Features Listing", Description = "Allows you to create unlimited items in a two column table.")]
    public class FeaturesListing : DynamicContentBase, IDynamicContentView
    {
        public PropertyFeatures PropFeatures;

        public FeaturesListing()
        {
            this.PropFeatures = new PropertyFeatures();
            PropFeatures.Name = "Feature Items";
            this.Properties.Add(this.PropFeatures);
        }

        public void Render(System.IO.TextWriter textWriter)
        {
            if (this.PropFeatures != null && this.PropFeatures.FeatureCollection.Count > 0)
            {
                HtmlTextWriter writer = new HtmlTextWriter(textWriter);
                writer.AddAttribute(HtmlTextWriterAttribute.Class, "featured-list table");
                writer.RenderBeginTag(HtmlTextWriterTag.Table);

                var heading = this.PropFeatures.FeatureCollection.Where(p => p.IsHeading).FirstOrDefault();
                if (heading != null)
                {
                    this.RenderRow(writer, HtmlTextWriterTag.Th, heading);
                    this.PropFeatures.FeatureCollection.Remove(heading);
                }

                foreach (Feature feature in this.PropFeatures.FeatureCollection)
                {
                    this.RenderRow(writer, HtmlTextWriterTag.Td, feature);
                }
                writer.RenderEndTag();
            }
        }

        private void RenderRow(HtmlTextWriter writer, HtmlTextWriterTag tag, Feature feature)
        {
            writer.RenderBeginTag(HtmlTextWriterTag.Tr);

            writer.RenderBeginTag(tag);
            writer.Write(feature.Name);
            writer.RenderEndTag();

            writer.RenderBeginTag(tag);
            writer.Write(feature.Value);
            writer.RenderEndTag();

            writer.RenderEndTag();
        }
    }
}