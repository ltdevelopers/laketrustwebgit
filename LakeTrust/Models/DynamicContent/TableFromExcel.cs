﻿using EPiServer;
using EPiServer.Core;
using EPiServer.Data;
using EPiServer.DynamicContent;
using EPiServer.Framework.Blobs;
using EPiServer.ServiceLocation;
using EPiServer.SpecializedProperties;
using EPiServer.Web;
using EPiServer.Web.Routing;
using SpreadsheetGear;
using SpreadsheetGear.Data;
using System;
using System.Data;
using System.IO;
using System.Web.UI;

namespace LakeTrust.Models.DynamicContent
{
    [DynamicContentPlugIn(DisplayName = "Table From Excel", Description = "Displays a table that is based on an Excel document")]
    public class TableFromExcel : DynamicContentBase, IDynamicContentView
    {
        public PropertyString TableCaption { get; set; }

        public PropertyDocumentUrl ExcelDocument;

        private const string CacheKeyPrefix = "VppToContentRedirect-";
        private const string MigrationToolLookupSql = "SELECT ContentLink FROM [dbo].[_MigratedVPPFiles] WHERE VirtualPath=@p0";

        public TableFromExcel()
        {
            this.TableCaption = new PropertyString();
            this.TableCaption.Name = "Table Caption";
            this.Properties.Add(this.TableCaption);

            this.ExcelDocument = new PropertyDocumentUrl();
            this.ExcelDocument.Name = "Select Excel File";
            this.Properties.Add(this.ExcelDocument);
        }

        public void Render(TextWriter textWriter)
        {
            if (this.ExcelDocument.Value == null)
            {
                return;
            }

            HtmlTextWriter writer = new HtmlTextWriter(textWriter);
            IWorkbook workbook;
            bool isOldVPPFile = this.IsVirtual(this.ExcelDocument.Value.ToString());
            string url = string.Empty;
            if (isOldVPPFile)
            {
                url = GetNewPath(this.ExcelDocument.Value.ToString());
                if (!string.IsNullOrEmpty(url))
                {
                    workbook = Factory.GetWorkbook(url);
                    DataSet dataSet = workbook.GetDataSet(GetDataFlags.FormattedText);
                    if (dataSet.Tables.Count > 0)
                    {
                        var dt = dataSet.Tables[0];
                        writer.AddAttribute(HtmlTextWriterAttribute.Class, "table table-striped table-bordered");
                        writer.RenderBeginTag(HtmlTextWriterTag.Table);
                        if (this.TableCaption.Value != null)
                        {
                            if (!string.IsNullOrEmpty(this.TableCaption.Value.ToString()))
                            {
                                writer.RenderBeginTag(HtmlTextWriterTag.Caption);
                                writer.Write(this.TableCaption.Value.ToString());
                                writer.RenderEndTag();
                            }
                        }
                        writer.RenderBeginTag(HtmlTextWriterTag.Thead);
                        writer.RenderBeginTag(HtmlTextWriterTag.Tr);
                        foreach (DataColumn column in dt.Columns)
                        {
                            writer.RenderBeginTag(HtmlTextWriterTag.Th);
                            writer.Write(column.ColumnName);
                            writer.RenderEndTag();
                        }
                        writer.RenderEndTag();
                        writer.RenderEndTag();
                        if (dt.Rows.Count > 0)
                        {
                            writer.RenderBeginTag(HtmlTextWriterTag.Tbody);
                            foreach (DataRow row in dt.Rows)
                            {
                                this.WriteRow(writer, row);
                            }
                            writer.RenderEndTag();
                            writer.RenderEndTag();
                        }
                    }
                }
            }
        }

        private void WriteRow(HtmlTextWriter writer, DataRow row)
        {
            int spanCount = 0;

            writer.RenderBeginTag(HtmlTextWriterTag.Tr);
            if (IsSpannedRow(row, out spanCount))
            {
                writer.AddAttribute(HtmlTextWriterAttribute.Colspan, (spanCount + 1).ToString());
                writer.RenderBeginTag(HtmlTextWriterTag.Td);
                writer.Write(row[0].ToString());
                writer.RenderEndTag();
            }
            else
            {
                foreach (DataColumn column in row.Table.Columns)
                {
                    writer.RenderBeginTag(HtmlTextWriterTag.Td);
                    writer.Write(row[column.ColumnName].ToString());
                    writer.RenderEndTag();
                }
            }
            writer.RenderEndTag();
        }

        private bool IsSpannedRow(DataRow row, out int spanCount)
        {
            int columnCount = row.Table.Columns.Count;
            spanCount = 0;
            for (int i = 0; i <= columnCount - 1; i++)
            {
                var value = row[i].ToString();
                if (!string.IsNullOrEmpty(value))
                {
                    if (i != 0)
                    {
                        return false;
                    }
                }
                else
                {
                    if (i > 0)
                    {
                        spanCount++;
                    }
                }
            }
            return spanCount == columnCount - 1;
        }

        private bool IsVirtual(string path)
        {
            return VirtualPathUtilityEx.IsValidVirtualPath(path);
        }

        private string GetNewPath(string vppPath)
        {
            IDatabaseHandler db = ServiceLocator.Current.GetInstance<IDatabaseHandler>();
            UrlResolver urlResolver = ServiceLocator.Current.GetInstance<UrlResolver>();
            IContentRepository contentRepository = ServiceLocator.Current.GetInstance<IContentRepository>();
            string newUrl = null;
            if (newUrl == null)
            {
                var contentLinkString = db.Execute(() =>
                {
                    using (var cmd = db.CreateCommand(MigrationToolLookupSql, System.Data.CommandType.Text, vppPath))
                    {
                        return cmd.ExecuteScalar() as string;
                    }
                });
                ContentReference contentLink = new ContentReference(contentLinkString);

                try
                {
                    var file = contentRepository.Get<MediaData>(contentLink).BinaryData;
                    if (file is FileBlob)
                        newUrl = ((EPiServer.Framework.Blobs.FileBlob)file).FilePath;
                }
                catch (ContentNotFoundException)
                {
                }
            }
            return newUrl;
        }

        //public static string GetAbsolutePath(string assetKey)
        //{
        //    var contentReference = PermanentLinkUtility.FindContentReference(new Guid(assetKey));
        //    if (contentReference != null)
        //    {
        //        var blobFactory = ServiceLocator.Current.GetInstance<BlobFactory>();
        //        var fileID = new ContentReference(contentReference.ID);
        //        //Get the file
        //        var file = contentRepository.Get<MediaData>(fileID).BinaryData;
        //        return file.ID.AbsolutePath;
        //    }
        //    return string.Empty;
        //}
    }
}