﻿using EPiServer.DynamicContent;
using EPiServer.SpecializedProperties;
using System.Web.UI;

namespace LakeTrust.Models.DynamicContent
{
    [DynamicContentPlugIn(DisplayName = "Related Links", Description = "Allows you to select any number of pages or urls to display a list of links on the site.")]
    public class RelatedLinks : DynamicContentBase, IDynamicContentView
    {
        public PropertyLinkCollection Links;

        public RelatedLinks()
        {
            this.Links = new PropertyLinkCollection();
            Links.Name = "Related Links";
            this.Properties.Add(this.Links);
        }

        public void Render(System.IO.TextWriter textWriter)
        {
            if (this.Links != null && this.Links.Links.Count > 0)
            {
                HtmlTextWriter writer = new HtmlTextWriter(textWriter);
                writer.AddAttribute(HtmlTextWriterAttribute.Class, "related-links");
                writer.RenderBeginTag(HtmlTextWriterTag.Ul);
                foreach (LinkItem link in this.Links.Links)
                {
                    writer.RenderBeginTag(HtmlTextWriterTag.Li);

                    writer.AddAttribute(HtmlTextWriterAttribute.Href, link.Href);
                    writer.RenderBeginTag(HtmlTextWriterTag.A);
                    writer.Write(link.Text);
                    writer.RenderEndTag();

                    writer.RenderEndTag();
                }
                writer.RenderEndTag();
            }
        }
    }
}