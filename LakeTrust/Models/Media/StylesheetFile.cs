﻿using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.Framework.DataAnnotations;
using System;

namespace LakeTrust.Models.Media
{
    [ContentType(DisplayName = "Stylesheet File", GUID = "c7b8af61-212c-4fcf-b47e-073f9edd5c55")]
    [MediaDescriptor(ExtensionString = "css")]
    public class StylesheetFile : MediaData
    {
    }
}