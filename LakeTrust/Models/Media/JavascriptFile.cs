﻿using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.Framework.DataAnnotations;
using System;

namespace LakeTrust.Models.Media
{
    [ContentType(DisplayName = "Javascript File", GUID = "dfc07aa2-3e8d-488c-8d0a-c8f231037a30", Description = "")]
    [MediaDescriptor(ExtensionString = "js")]
    public class JavascriptFile : MediaData
    {
    }
}