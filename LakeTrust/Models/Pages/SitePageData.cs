﻿using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.Shell.ObjectEditing;
using EPiServer.SpecializedProperties;
using EPiServer.Web;
using EPiServer.XForms;
using LakeTrust.Business;
using LakeTrust.Models.Media;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace LakeTrust.Models.Pages
{
    public abstract class SitePageData : LakeTrustBaseModel
    {
        public SitePageData()
        {
            this.PageForm = new XForm();
        }

        public override void SetDefaultValues(ContentType contentType)
        {
            base.SetDefaultValues(contentType);
            this.NavigationColumns = "1";
            this.HideCrumbtrail = false;
        }

        #region Blocks

        [Display(Name = "Left Block Area", Description = "A content area where you can place blocks", GroupName = Global.GroupNames.Blocks, Order = 70)]
        public virtual ContentArea LeftBlockArea { get; set; }

        [Display(Name = "Top Block Area", Description = "A content area where you can place blocks", GroupName = Global.GroupNames.Blocks, Order = 80)]
        public virtual ContentArea TopBlockArea { get; set; }

        [Display(Name = "Right Block Area", Description = "A content area where you can place blocks", GroupName = Global.GroupNames.Blocks, Order = 80)]
        public virtual ContentArea RightBlockArea { get; set; }

        [Display(Name = "Right Fixed Block Area", Description = "A content area where you can place blocks", GroupName = Global.GroupNames.Blocks, Order = 90)]
        public virtual ContentArea RightFixedBlockArea { get; set; }

        [Display(Name = "Center Middle Block Area", Description = "A content area where you can place blocks that are fluid and sizable based on editor input", GroupName = Global.GroupNames.Blocks, Order = 95)]
        [UIHint("LakeTrustContentArea")]
        public virtual ContentArea CenterBlockArea { get; set; }

        [Display(Name = "Bottom Block Area", Description = "A content area where you can place blocks", GroupName = Global.GroupNames.Blocks, Order = 110)]
        public virtual ContentArea BottomBlockArea { get; set; }

        #endregion Blocks

        #region Content

        [Display(Name = "Page Title", Order = 10, Description = "The Title for the page.", GroupName = SystemTabNames.Content)]
        [Required(ErrorMessage = "You must enter a title")]
        public virtual string Title { get; set; }

        [Display(Order = 10000, Name = "Main Body", Description = "The main body will be shown in the main content area of the page, using the XHTML-editor you can insert for example text, images and tables.")]
        public virtual XhtmlString MainBody { get; set; }

        #endregion Content

        #region Images

        [Display(Name = "Page Thumbnail", Description = "Used for listings", GroupName = Global.GroupNames.Images)]
        [UIHint(UIHint.Image)]
        public virtual ContentReference PageImage { get; set; }

        #endregion Images

        #region Forms

        [Display(Name = "Page Form", Description = "This will display a form on the page.", GroupName = Global.GroupNames.Forms, Order = 10)]
        public virtual XForm PageForm { get; set; }

        [Display(Order = 9089, GroupName = SystemTabNames.Settings, Name = "Hide Page Form", Description = "When check, the form on the page will be hidden.")]
        [Searchable(false)]
        public virtual bool HidePageForm { get; set; }

        #endregion Forms

        #region SEO

        [Display(Name = "META Keywords", Order = 9998, Description = "The seo keywords for meta data.", GroupName = Global.GroupNames.SEO)]
        [UIHint(UIHint.Textarea)]
        public virtual string MetaKeywords { get; set; }

        [Display(Name = "META Description", Order = 9999, Description = "The seo description for meta data.", GroupName = Global.GroupNames.SEO)]
        [UIHint(UIHint.Textarea)]
        public virtual string SEODescription { get; set; }

        [Display(Name = "Exlude From Index", Description = "If checked, the documemnt will not be indexed", GroupName = SystemTabNames.Settings, Order = 100)]
        public virtual bool ExcludeFromIndex { get; set; }

        #endregion SEO

        #region Navigation

        [Display(Name = "Sub Navigation Start Page", Order = 95, Description = "Will override the page under homepage as the start to display menu from.  This will allow you to choose where the navigation starts from", GroupName = Global.GroupNames.Navigation)]
        public virtual ContentReference SubNavigationStart { get; set; }

        [Display(Name = "Navigation Columns", Description = "Sets the number of columns for the main navigation dropdown list", GroupName = Global.GroupNames.Navigation, Order = 500)]
        [UIHint("NavigationColumnsMultiple")]
        public virtual string NavigationColumns { get; set; }

        [Display(Name = "Navigation Flyout Left", Description = "If checked, the navigation will flyout to the left instead of the default right direction", GroupName = Global.GroupNames.Navigation, Order = 501)]
        public virtual bool NavigationFlyoutDirection { get; set; }

        [Display(Name = "Hide Crumbtrail", Description = "If checked, the crumbtrail will be hidden on the page", GroupName = Global.GroupNames.Navigation, Order = 1)]
        public virtual bool HideCrumbtrail { get; set; }

        #endregion Navigation

        #region Settings

        [Display(Name = "Footer Javscript Includes", GroupName = SystemTabNames.Settings, Order = 2000)]
        [AllowedTypes(new[] { typeof(JavascriptFile) })]
        public virtual LinkItemCollection JavascriptIncludes { get; set; }

        [Display(Name = "Header Javscript Includes", GroupName = SystemTabNames.Settings, Order = 1999)]
        [AllowedTypes(new[] { typeof(JavascriptFile) })]
        public virtual LinkItemCollection HeaderJavascriptIncludes { get; set; }

        [Display(GroupName = SystemTabNames.Settings, Order = 200)]
        public virtual bool HideSiteHeader { get; set; }

        [Display(GroupName = SystemTabNames.Settings, Order = 300)]
        public virtual bool HideSiteFooter { get; set; }

        [Ignore]
        public bool IsMobile { get; set; }

        [Ignore]
        public virtual bool FullWidth
        {
            get
            {
                return !this.RightFixedBlockArea.HasItems();
            }
        }

        #endregion Settings
    }
}