﻿using EPiServer.Core;
using EPiServer.DataAnnotations;
using EPiServer.XForms;
using System.ComponentModel.DataAnnotations;

namespace LakeTrust.Models.Pages
{
    [SiteContentType(DisplayName = "Site Map", GUID = "f8d95c86-4412-44c2-9a54-76dcf128d3da", GroupName = Global.GroupNames.Specialized, Description = "This is used as a singleton and should only be used once.")]
    [SiteImageUrl]
    public class SiteMapPage : SitePageData
    {
        public SiteMapPage()
        {
            this.LevelOnePage = new PageDataCollection();
        }

        /* Properties used that are not part of this pagetype*/

        [Ignore]
        public PageDataCollection LevelOnePage { get; set; }

        /* PageProperties not used for this page type*/

        [Display(Name = "Page Form", Description = "This will display a form on the page.", GroupName = Global.GroupNames.Forms, Order = 10)]
        [ScaffoldColumn(false)]
        [Editable(false)]
        public override XForm PageForm { get; set; }

        [Display(Order = 10000, Name = "Main Body", Description = "The main body will be shown in the main content area of the page, using the XHTML-editor you can insert for example text, images and tables.")]
        [Editable(false)]
        [ScaffoldColumn(false)]
        public override XhtmlString MainBody { get; set; }

        [Display(Name = "Left Block Area", Description = "A content area where you can place blocks", GroupName = Global.GroupNames.Blocks, Order = 70)]
        [Editable(false)]
        public override ContentArea LeftBlockArea { get; set; }

        [ScaffoldColumn(false)]
        public override ContentArea CenterBlockArea { get; set; }
    }
}