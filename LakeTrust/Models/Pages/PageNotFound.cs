﻿using EPiServer.Core;
using System.ComponentModel.DataAnnotations;

namespace LakeTrust.Models.Pages
{
    [SiteContentType(DisplayName = "Page Not Found", GroupName = Global.GroupNames.Specialized, GUID = "8b930729-3352-41ac-a4e3-9339600cd7f3", Description = "A singleton page that should only be used ONCE!!!!!")]
    [SiteImageUrl]
    public class PageNotFound : SitePageData
    {
        [ScaffoldColumn(false)]
        public override ContentArea CenterBlockArea { get; set; }
    }
}