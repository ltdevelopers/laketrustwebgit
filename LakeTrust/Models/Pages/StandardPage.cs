﻿using EPiServer.Core;
using EPiServer.DataAnnotations;
using System;
using System.ComponentModel.DataAnnotations;

namespace LakeTrust.Models.Pages
{
    //[AutoImageThumbnail(typeof(StandardModel))]
    [SiteContentType(DisplayName = "Standard Page", GUID = "2b86817f-c382-438c-a98b-673b36b0cb4f", Description = "Standard test page used for content")]
    [SiteImageUrl(Global.StringConstants.StaticGraphicsFolderPath + "page-type-thumbnail-standard.png")]
    [AvailableContentTypes(Exclude = new Type[] { typeof(StartPage) })]
    public class StandardPage : SitePageData
    {
        [Display(Name = "Bottom Fluid Block Area", Description = "A content area where you can place blocks", GroupName = Global.GroupNames.Blocks, Order = 100)]
        public virtual ContentArea BottomFluidContentArea { get; set; }
    }
}