﻿using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.Shell.ObjectEditing;
using LakeTrust.Models.Blocks.HQ;
using LakeTrust.Models.Media;
using System.ComponentModel.DataAnnotations;

namespace LakeTrust.Models.Pages.HQ
{
    [HQSiteContentType(DisplayName = "Start Page", GUID = "eae0b6db-971e-48ed-b71e-196b18589c04", Description = "")]
    [AvailableContentTypes(IncludeOn = new[] { typeof(StartPage) }, Include = new[] { typeof(HQUpdatePage) }, Availability = Availability.Specific)]
    [SiteImageUrl("~/Content/Images/GroupImages/HQ.gif")]
    public class HQStartPage : HQBasePage
    {
        [Display(Name = "HQ Logo", Description = "The logo for the site.", GroupName = SystemTabNames.Content, Order = 5)]
        [AllowedTypes(new[] { typeof(ImageFile) })]
        public virtual ContentReference Logo { get; set; }

        [Display(Name = "Lake Trust Logo", Description = "The logo for the Lake Trust.", GroupName = SystemTabNames.Content, Order = 6)]
        [AllowedTypes(new[] { typeof(ImageFile) })]
        public virtual ContentReference LakeTrustLogo { get; set; }

        [Display(Name = "Main Content Area", Description = "A content area where you can place blocks", GroupName = Global.GroupNames.Blocks, Order = 10)]
        [AllowedTypes(new[] { typeof(HQUpdatePage), typeof(HQHtmlBlock), typeof(HQFormBlock), typeof(HQWebCamBlock), typeof(HQQuestionAnswerBlock), typeof(HQLinkBlock), typeof(GenericMedia), typeof(HQRotatorBlock) })]
        public virtual ContentArea MainContent { get; set; }

        [Display(Name = "Twitter Url", Description = "Url for twitter.", GroupName = SystemTabNames.Content, Order = 15)]
        public virtual Url TwitterUrl { get; set; }

        [Display(Name = "Facebook Url", Description = "Url for facebook.", GroupName = SystemTabNames.Content, Order = 20)]
        public virtual Url FacebookUrl { get; set; }

        [Display(Name = "Youtube Url", Description = "Url for youtube.", GroupName = SystemTabNames.Content, Order = 25)]
        public virtual Url YouTubeUrl { get; set; }

        [Display(Name = "Instagram Url", Description = "Url for instagram.", GroupName = SystemTabNames.Content, Order = 30)]
        public virtual Url InstagramUrl { get; set; }

        [Display(Name = "Email Address", Description = "Email Address.", GroupName = SystemTabNames.Content, Order = 35)]
        public virtual string EmailAddress { get; set; }

        [ScaffoldColumn(false)]
        public override bool IsNavigationPoint { get; set; }

        [ScaffoldColumn(false)]
        public override string ContentAreaCssClass { get; set; }
    }
}