﻿using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAbstraction.Migration;
using EPiServer.DataAnnotations;
using EPiServer.Web;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace LakeTrust.Models.Pages.HQ
{
    [HQSiteContentType(DisplayName = "Update Page", GUID = "10d13067-9f8f-472d-905a-2a3e973e703a", Description = "Update Page")]
    [AvailableContentTypes(IncludeOn = new[] { typeof(HQStartPage) })]
    [SiteImageUrl("~/Content/Images/GroupImages/HQ.gif")]
    public class HQUpdatePage : HQBasePage
    {
        [Required(ErrorMessage = "You must enter a title")]
        [Display(Name = "Page Title", Order = 10, Description = "The Title for the page.", GroupName = SystemTabNames.Content)]
        public virtual string Title { get; set; }

        [Display(Name = "Image", Order = 20, Description = "The image for this block")]
        [UIHint(UIHint.Image)]
        public virtual ContentReference Image { get; set; }

        [Display(Name = "Content", Order = 20, Description = "The content for the page.  This will NOT be shown in a block.")]
        public virtual XhtmlString MainContent { get; set; }

        [Ignore]
        public IEnumerable<HQUpdatePage> UpdatePages { get; set; }
    }

    //public class HQUpdateMigration : MigrationStep
    //{
    //    public override void AddChanges()
    //    {
    //        RenameProperty();
    //    }

    //    private void RenameProperty()
    //    {
    //        ContentType("HQUpdatePage")                  //On the content type "Bicycle"
    //            .Property("PageImage")          //There is a property called "PneumaticTire"
    //                .UsedToBeNamed("Image");   //That used to be called "WoodenTire"
    //    }
    //}
}