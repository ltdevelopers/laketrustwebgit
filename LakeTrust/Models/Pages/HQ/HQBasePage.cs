﻿using EPiServer.Core;
using LakeTrust.Business.Interfaces;
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace LakeTrust.Models.Pages.HQ
{
    public class HQBasePage : PageData, IHQDataItem
    {
        [Display(Name = "Background Color", Description = "Will display the background color of the block based on your selection", Order = 500)]
        [UIHint(Global.SiteUIHints.HQColorClass)]
        public virtual string ContentAreaCssClass { get; set; }

        [Display(Name = "Navigation Anchor", Description = "If checked, this will add a new navigation item to the menu.", Order = 550)]
        public virtual bool IsNavigationPoint { get; set; }

        [Display(Name = "Navigation Anchor Title", Description = "The title of the navigational Anchor.", Order = 549)]
        public virtual string NavigationAnchorTitle { get; set; }
    }
}