﻿namespace LakeTrust.Models.Pages
{
    [SiteContentType(DisplayName = "Product", GUID = "b29e4eec-536c-4117-a7ea-978c600127d7", Description = "A single product", GroupName = Global.GroupNames.Product)]
    [SiteImageUrl]
    public class ProductPage : SitePageData
    {
    }
}