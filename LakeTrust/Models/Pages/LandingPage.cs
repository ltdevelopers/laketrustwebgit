﻿using EPiServer.Core;
using System.ComponentModel.DataAnnotations;

namespace LakeTrust.Models.Pages
{
    [SiteContentType(DisplayName = "Landing Page", GUID = "f377161c-7731-43fd-932c-513ca1eb5374", Description = "This is a landing page that contains NO navigational elements and should not live withing the tree")]
    [SiteImageUrl]
    public class LandingPage : SitePageData
    {
        [Display(Name = "Hide Page Title", Order = 11)]
        public virtual bool HidePageTitle { get; set; }

        // Hidden

        [ScaffoldColumn(false)]
        public override bool HideCrumbtrail { get; set; }

        [ScaffoldColumn(false)]
        public override bool NavigationFlyoutDirection { get; set; }

        [ScaffoldColumn(false)]
        public override string NavigationColumns { get; set; }

        [ScaffoldColumn(false)]
        public override ContentReference SubNavigationStart { get; set; }
    }
}