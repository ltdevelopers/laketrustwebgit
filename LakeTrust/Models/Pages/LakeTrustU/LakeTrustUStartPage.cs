﻿using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using System;
using System.ComponentModel.DataAnnotations;

namespace LakeTrust.Models.Pages.LakeTrustU
{
    [SiteContentType(DisplayName = "Start Page", GUID = "0aaa4688-8fa0-4814-af4c-58662424c17e", Description = "", GroupName = Global.GroupNames.LakeTrustU)]
    [AvailableContentTypes(IncludeOn = new[] { typeof(StartPage) })]
    [SiteImageUrl]
    public class LakeTrustUStartPage : SitePageData
    {
        [Display(Name = "Number of Classes To Show", Order = 3, GroupName = SystemTabNames.Content)]
        public virtual int NumberOfClassesToShow { get; set; }
    }
}