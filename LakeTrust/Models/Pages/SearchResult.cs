﻿using EPiServer.Core;
using EPiServer.DataAbstraction;
using System.ComponentModel.DataAnnotations;

namespace LakeTrust.Models.Pages
{
    [SiteContentType(DisplayName = "Search Results", GroupName = Global.GroupNames.Specialized, GUID = "db75fe6d-67f3-4875-8603-273c3e96cb8b", Description = "This page type is used to display search results")]
    [SiteImageUrl]
    public class SearchResult : SitePageData
    {
        public override void SetDefaultValues(ContentType contentType)
        {
            base.SetDefaultValues(contentType);

            this.NumberOfItemsToShow = 10;
            this.SearchTermTitle = "Search Results For ";
        }

        [Editable(true)]
        [Display(Name = "Items To Show Per Page", Description = "The number of items to show per page in the results.", GroupName = SystemTabNames.Content, Order = 20)]
        public virtual int NumberOfItemsToShow { get; set; }

        [Editable(true)]
        [Display(Name = "Search Term Title", Description = "The text displayed before the search term.", GroupName = SystemTabNames.Content, Order = 30)]
        public virtual string SearchTermTitle { get; set; }

        [ScaffoldColumn(false)]
        public override ContentArea CenterBlockArea { get; set; }
    }
}