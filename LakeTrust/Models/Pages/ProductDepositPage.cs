﻿using System;
using System.ComponentModel.DataAnnotations;

namespace LakeTrust.Models.Pages
{
    [SiteContentType(DisplayName = "Product-Deposit", GroupName = Global.GroupNames.Product, GUID = "cb33c27b-fbfc-4e69-8ce5-26f9ac02dd9e", Description = "")]
    public class ProductDepositPage : ProductPage
    {
        [Display(Name = "High Dividend", Order = 1)]
        public virtual string HighRateDividend { get; set; }

        [Display(Name = "Low Dividend", Order = 2)]
        public virtual string LowRateDividend { get; set; }

        [Display(Name = "Low APY", Order = 3)]
        public virtual string LowRateAPY { get; set; }

        [Display(Name = "High APY", Order = 4)]
        public virtual string HighRateAPY { get; set; }

        [Display(Name = "Monthly Service Fee", Order = 5)]
        public virtual string FeeService { get; set; }

        [Display(Name = "Minimum Balance", Order = 6)]
        public virtual string Minimum { get; set; }

        [Display(Name = "Balance For Interest", Order = 7)]
        public virtual string BalanceInterest { get; set; }

        [Display(Name = "Terms", Order = 8)]
        public virtual string TermsCD { get; set; }
    }
}