﻿using System;
using System.ComponentModel.DataAnnotations;

namespace LakeTrust.Models.Pages
{
    [SiteContentType(DisplayName = "Product-CreditCard", GroupName = Global.GroupNames.Product, GUID = "79b96522-46ab-4718-9b58-3d75da084392", Description = "")]
    public class ProductCreditCardPage : ProductPage
    {
        [Display(Name = "Intro Rate", Order = 1)]
        public virtual string IntroRate { get; set; }

        [Display(Name = "Low Rate", Order = 2)]
        public virtual string LowRate { get; set; }

        [Display(Name = "High Rate", Order = 3)]
        public virtual string HighRate { get; set; }

        [Display(Name = "Transfer Low Rate", Order = 4)]
        public virtual string LowRateTransfer { get; set; }

        [Display(Name = "Transfer High Rate", Order = 5)]
        public virtual string HighTransferRate { get; set; }

        [Display(Name = "Annual Fee", Order = 6)]
        public virtual string AnnualFee { get; set; }

        [Display(Name = "Late Fee", Order = 7)]
        public virtual string LateFee { get; set; }

        [Display(Name = "Returned Payment Fee", Order = 8)]
        public virtual string ReturnedPaymentFee { get; set; }

        [Display(Name = "Grace Period", Order = 9)]
        public virtual string GracePeriod { get; set; }

        [Display(Name = "Travel and Accident Insurance", Order = 10)]
        public virtual string Insurance { get; set; }

        [Display(Name = "Minimum Credit", Order = 11)]
        public virtual string Minimum { get; set; }
    }
}