﻿using EPiServer.Core;
using EPiServer.DataAnnotations;
using EPiServer.XForms;
using System;
using System.ComponentModel.DataAnnotations;

namespace LakeTrust.Models.Pages
{
    [SiteContentType(DisplayName = "News List", GroupName = Global.GroupNames.News, GUID = "9379c4b2-7841-477f-837f-99f495c3e693", Description = "A listing of news items.  This is also used as a container for month, year, etc...")]
    [SiteImageUrl]
    [AvailableContentTypes(Exclude = new Type[] { typeof(StartPage), typeof(StandardPage) }, Include = new Type[] { typeof(NewsPage), typeof(NewsArticlePage) })]
    public class NewsPage : SitePageData
    {
        [Display(Name = "Page Form", Description = "This will display a form on the page.", GroupName = Global.GroupNames.Forms, Order = 10)]
        [Editable(false)]
        [ScaffoldColumn(false)]
        public override XForm PageForm { get; set; }

        [ScaffoldColumn(false)]
        public override ContentArea CenterBlockArea { get; set; }
    }
}