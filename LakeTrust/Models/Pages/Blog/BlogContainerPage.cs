﻿using EPiServer.Core;
using EPiServer.DataAbstraction;
using System.ComponentModel.DataAnnotations;

namespace LakeTrust.Models.Pages
{
    [BlogContentType(DisplayName = "Blog Container", GroupName = Global.GroupNames.Blog, AvailableInEditMode = false, GUID = "9d8a24f9-5f24-461d-88af-e27f8c6c3b50", Description = "Represents the year and month container for posts.")]
    [SiteImageUrl]
    public class BlogContainerPage : LakeTrustBaseModel
    {
        [Display(Name = "Page Title", Order = 10, Description = "The Title for the page.", GroupName = SystemTabNames.Content)]
        [Required(ErrorMessage = "You must enter a title")]
        public virtual string Title { get; set; }

        [Display(Name = "Left Block Area", Description = "A content area where you can place blocks", GroupName = Global.GroupNames.Blocks, Order = 70)]
        public virtual ContentArea LeftBlockArea { get; set; }

        [Display(Name = "Right Block Area", Description = "A content area where you can place blocks", GroupName = Global.GroupNames.Blocks, Order = 80)]
        public virtual ContentArea RightBlockArea { get; set; }

        [Display(Name = "Right Fixed Block Area", Description = "A content area where you can place blocks", GroupName = Global.GroupNames.Blocks, Order = 90)]
        public virtual ContentArea RightFixedBlockArea { get; set; }

        [Display(Name = "Top Block Area", Description = "A content area where you can place blocks", GroupName = Global.GroupNames.Blocks, Order = 100)]
        public virtual ContentArea TopBlockArea { get; set; }

        [Display(Name = "Bottom Block Area", Description = "A content area where you can place blocks", GroupName = Global.GroupNames.Blocks, Order = 110)]
        public virtual ContentArea BottomBlockArea { get; set; }
    }
}