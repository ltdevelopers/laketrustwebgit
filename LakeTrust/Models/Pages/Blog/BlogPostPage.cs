﻿using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.Web;
using LakeTrust.Business;
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace LakeTrust.Models.Pages
{
    [BlogContentType(DisplayName = "Blog Post", GroupName = Global.GroupNames.Blog, GUID = "069aa884-2661-422a-9022-fedc65428670", Description = ""),
    SiteImageUrl(Global.StringConstants.StaticGraphicsFolderPath + "page-type-thumbnail-article.png")]
    public class BlogPostPage : SitePageData
    {
        [Display(Name = "Blog Post Listing Thumbnail", Order = 888, Description = "The image for this blog post.", GroupName = SystemTabNames.Content)]
        [UIHint(UIHint.Image)]
        [ScaffoldColumn(true)]
        public virtual Url BlogPostImage { get; set; }

        [Display(Name = "Blog Post Listing Thumbnail Alt Text", Order = 888, Description = "The image for this blog post.", GroupName = SystemTabNames.Content)]
        [ScaffoldColumn(true)]
        public virtual string BlogPostImageAltText { get; set; }

        [Display(Name = "Blog Post Teaser", Order = 9999, Description = "The teaser for the blog post.", GroupName = SystemTabNames.Content)]
        public virtual XhtmlString Teaser { get; set; }

        [Display(Order = 10000, Name = "Blog Content", Description = "The content of the blog will be shown in the main content area of the page, using the XHTML-editor you can insert for example text, images and tables.")]
        public override XhtmlString MainBody { get; set; }

        [Display(Name = "Disable Comments", Order = 8000, Description = "If checked, comments will not be allowed on the blog post.", GroupName = SystemTabNames.Content)]
        public virtual bool CommentsClosed { get; set; }

        public override string SEODescription
        {
            get
            {
                var description = this.GetPropertyValue(x => x.SEODescription);
                if (!string.IsNullOrEmpty(description))
                    return description;
                return this.GetPropertyValue(x => x.Teaser, new XhtmlString()).ToHtmlString().StripTags();
            }
            set
            {
                this.SetPropertyValue(x => x.SEODescription, value);
            }
        }

        [Display(Name = "Import PostId")]
        [Editable(false)]
        [ScaffoldColumn(false)]
        public virtual int ImportPostId { get; set; }

        [Display(Name = "Number of Views")]
        [Editable(false)]
        [ScaffoldColumn(false)]
        public virtual int Views { get; set; }

        [Display(Name = "Blog Post Catgories", Order = 800)]
        [UIHint(LakeTrust.Global.SiteUIHints.Tags)]
        public virtual string Tags { get; set; }
    }
}