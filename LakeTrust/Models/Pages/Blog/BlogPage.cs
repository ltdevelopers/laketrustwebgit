﻿using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace LakeTrust.Models.Pages
{
    [BlogContentType(DisplayName = "Blog", GroupName = Global.GroupNames.Blog, GUID = "acd7bce7-a849-493f-a2d7-10676825fea3", Description = "")]
    [AvailableContentTypes(Include = new[] { typeof(BlogPostPage), typeof(BlogContainerPage) }, IncludeOn = new[] { typeof(StandardPage) })]
    [SiteImageUrl]
    public class BlogPage : LakeTrustBaseModel
    {
        public override void SetDefaultValues(ContentType contentType)
        {
            base.SetDefaultValues(contentType);
            this.PostPerPage = 10;
            this.PopularPostCount = 3;
        }

        [Display(Name = "Page Title", Order = 10, Description = "The Title for the page.", GroupName = SystemTabNames.Content)]
        [Required(ErrorMessage = "You must enter a title")]
        public virtual string Title { get; set; }

        [Display(Name = "Posts Per Page", Order = 15, GroupName = SystemTabNames.Content)]
        public virtual int PostPerPage { get; set; }

        [Display(Name = "Popular Posts Count", Order = 16, GroupName = SystemTabNames.Content)]
        public virtual int PopularPostCount { get; set; }

        [Display(Name = "Left Block Area", Description = "A content area where you can place blocks", GroupName = Global.GroupNames.Blocks, Order = 70)]
        public virtual ContentArea LeftBlockArea { get; set; }

        [Display(Name = "Right Block Area", Description = "A content area where you can place blocks", GroupName = Global.GroupNames.Blocks, Order = 80)]
        public virtual ContentArea RightBlockArea { get; set; }

        [Display(Name = "Right Fixed Block Area", Description = "A content area where you can place blocks", GroupName = Global.GroupNames.Blocks, Order = 90)]
        public virtual ContentArea RightFixedBlockArea { get; set; }

        [Display(Name = "Top Block Area", Description = "A content area where you can place blocks", GroupName = Global.GroupNames.Blocks, Order = 100)]
        public virtual ContentArea TopBlockArea { get; set; }

        [Display(Name = "Bottom Block Area", Description = "A content area where you can place blocks", GroupName = Global.GroupNames.Blocks, Order = 110)]
        public virtual ContentArea BottomBlockArea { get; set; }

        [Display(Name = "ExludeFromIndex", Description = "If checked, the documemnt will not be indexed", GroupName = SystemTabNames.Settings, Order = 100)]
        public virtual bool ExcludeFromIndex { get; set; }
    }
}