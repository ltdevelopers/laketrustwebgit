﻿using EPiServer.Core;
using EPiServer.DataAnnotations;
using EPiServer.XForms;
using LakeTrust.Business.Rendering;
using System.ComponentModel.DataAnnotations;

namespace LakeTrust.Models.Pages
{
    [SiteContentType(DisplayName = "Blank Page", GroupName = Global.GroupNames.Specialized, GUID = "9ff148bd-959b-42c6-b4f6-8adac7208feb", Description = "")]
    [SiteImageUrl]
    public class BlankPage : LakeTrustBaseModel, IContainerPage
    {
        [Display(Order = 10000, Name = "Main Body", Description = "The main body will be shown in the main content area of the page, using the XHTML-editor you can insert for example text, images and tables.")]
        public virtual XhtmlString MainBody { get; set; }

        [Display(Name = "Page Form", Description = "This will display a form on the page.", GroupName = Global.GroupNames.Forms, Order = 10)]
        public virtual XForm PageForm { get; set; }

        [Display(Order = 9089, GroupName = Global.GroupNames.Forms, Name = "Hide Page Form", Description = "When check, the form on the page will be hidden.")]
        [Searchable(false)]
        public virtual bool HidePageForm { get; set; }

        [Display(Name = "Top Block Area", Description = "A content area where you can place blocks", GroupName = Global.GroupNames.Blocks, Order = 90)]
        public virtual ContentArea TopBlockArea { get; set; }

        [Display(Name = "Bottom Block Area", Description = "A content area where you can place blocks", GroupName = Global.GroupNames.Blocks, Order = 100)]
        public virtual ContentArea BottomBlockArea { get; set; }
    }
}