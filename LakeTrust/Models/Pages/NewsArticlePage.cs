﻿using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.Web;
using EPiServer.XForms;
using LakeTrust.Business;
using System;
using System.ComponentModel.DataAnnotations;

namespace LakeTrust.Models.Pages
{
    [ContentType(DisplayName = "News Article", GroupName = Global.GroupNames.News, GUID = "8b543f53-cd50-4914-bc31-317a464d4a0c", Description = "A single news article")]
    [SiteImageUrl(Global.StringConstants.StaticGraphicsFolderPath + "page-type-thumbnail-article.png")]
    public class NewsArticlePage : SitePageData
    {
        public override void SetDefaultValues(ContentType contentType)
        {
            base.SetDefaultValues(contentType);
            this.IsAlert = false;
        }

        [UIHint(UIHint.Textarea)]
        [Display(Name = "Listing Teaser", Order = 1000, Description = "If filled in, this property will display in listings, else, we will truncate the text at 150 characters.")]
        public virtual String Teaser
        {
            get
            {
                string teaser = this.GetPropertyValue(p => p.Teaser);
                if (!string.IsNullOrEmpty(teaser))
                    return teaser;
                else
                {
                    return this.GetTeaser("SEODescription", 200, true);
                }
            }
            set { this.SetPropertyValue(p => p.Teaser, value); }
        }

        [Display(Name = "Is An Alert", Order = 30, Description = "If checked, this News Article will be marked as an alert.")]
        public virtual bool IsAlert { get; set; }

        [Display(Name = "Page Form", Description = "This will display a form on the page.", GroupName = Global.GroupNames.Forms, Order = 10)]
        [Editable(false)]
        [ScaffoldColumn(false)]
        public override XForm PageForm { get; set; }
    }
}