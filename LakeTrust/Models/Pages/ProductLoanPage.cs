﻿using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.Web;
using System;
using System.ComponentModel.DataAnnotations;

namespace LakeTrust.Models.Pages
{
    [ContentType(DisplayName = "Product-Loan", GroupName = Global.GroupNames.Product, GUID = "40cb3c15-05a3-42d0-b426-e91e92a52794", Description = "")]
    public class ProductLoanPage : ProductPage
    {
        [Display(Name = "Type", Order = 1)]
        public virtual string Type { get; set; }

        [Display(Name = "Low Rate", Order = 2)]
        public virtual string LowRate { get; set; }

        [Display(Name = "High Rate", Order = 3)]
        public virtual string HighRate { get; set; }

        [Display(Order = 4)]
        [UIHint(UIHint.Textarea)]
        public virtual string Terms { get; set; }

        [Display(Name = "Annual Fee", Order = 5)]
        public virtual string AnnualFee { get; set; }

        [Display(Name = "Application Fee", Order = 6)]
        public virtual string ApplicationFee { get; set; }

        [Display(Name = "Late Fee", Order = 7)]
        public virtual string LateFee { get; set; }

        [Display(Name = "Access Funds", Order = 8)]
        public virtual string AccessFunds { get; set; }

        [Display(Name = "Monthly Payments", Order = 9)]
        public virtual string MonthlyPayments { get; set; }

        [Display(Name = "Prepay Penalty", Order = 10)]
        public virtual string PrepayPenalty { get; set; }

        [Display(Order = 11)]
        public virtual string Collateral { get; set; }

        [Display(Order = 12)]
        public virtual string Minimum { get; set; }
    }
}