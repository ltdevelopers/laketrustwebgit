﻿using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.Shell.ObjectEditing;
using EPiServer.SpecializedProperties;
using EPiServer.XForms;
using LakeTrust.Models.Blocks;
using LakeTrust.Models.Media;
using LakeTrust.Models.Pages.HQ;
using LakeTrust.Models.Pages.LakeTrustU;
using System.ComponentModel.DataAnnotations;

namespace LakeTrust.Models.Pages
{
    [SiteContentType(DisplayName = "Start Page", Description = "Start Page", Order = 10000)]
    [SiteImageUrl]
    [AvailableContentTypes(ExcludeOn = new[] { typeof(CalendarEventPage), typeof(LakeTrustUStartPage), typeof(CalendarPage), typeof(LandingPage), typeof(NewsArticlePage), typeof(NewsPage), typeof(Container), typeof(SearchResult), typeof(StandardPage), typeof(StartPage), typeof(HQStartPage) })]
    public class StartPage : SitePageData
    {
        public override void SetDefaultValues(ContentType contentType)
        {
            base.SetDefaultValues(contentType);
            this.MyCreditCardText = "My Credit Card";
            this.NumberOfTwitterItems = 2;
            this.TwitterUserName = "lake_trustcu";
        }

        [Display(Name = "Home Page Header Content Area", Description = "The header area for the start page.", GroupName = SystemTabNames.Content, Order = 21)]
        public virtual ContentArea HeaderBlockArea { get; set; }

        [Editable(true)]
        [Display(Name = "Header Navigation", Order = 200, Description = "The navigation located in the page header", GroupName = Global.GroupNames.Navigation)]
        public virtual LinkItemCollection HeaderNavigation { get; set; }

        [Editable(true)]
        [Display(Name = "Footer Navigation", Order = 201, Description = "The navigation located in the page footer", GroupName = Global.GroupNames.Navigation)]
        public virtual LinkItemCollection FooterNavigation { get; set; }

        [Editable(true)]
        [Display(Name = "Social Navigation", Order = 202, Description = "The navigation located in the page footer for social media", GroupName = Global.GroupNames.Navigation)]
        [AllowedTypes(new[] { typeof(ImageLinkBlock) })]
        public virtual ContentArea SocialContentArea { get; set; }

        [Editable(true)]
        [Display(Name = "Footer Text", Order = 100, Description = "The text located in the page footer", GroupName = Global.GroupNames.Footer)]
        public virtual XhtmlString FooterText { get; set; }

        [Editable(true)]
        [Display(Name = "Footer Address", Order = 110, Description = "The address text located in the page footer", GroupName = Global.GroupNames.Footer)]
        public virtual XhtmlString FooterAddress { get; set; }

        [Editable(true)]
        [Display(Name = "My Credit Card Text", Order = 49, Description = "The text for the \"My Credit Card\" hyperlink", GroupName = SystemTabNames.Content)]
        public virtual string MyCreditCardText { get; set; }

        [Editable(true)]
        [Display(Name = "My Credit Card Url", Order = 50, Description = "The url to the \"My Credit Card\" site", GroupName = SystemTabNames.Content)]
        public virtual Url MyCreditCard { get; set; }

        [Display(Name = "Login Page", Description = "The page used for logins", GroupName = Global.GroupNames.Navigation, Order = 10)]
        [ScaffoldColumn(false)]
        public virtual ContentReference LoginPageReference { get; set; }

        [Display(Name = "News Start Page", Description = "The news start page in which ALL news is housed", GroupName = Global.GroupNames.Navigation, Order = 20)]
        public virtual ContentReference NewsStartReference { get; set; }

        [Display(Name = "Calendar Start Page", Description = "The caledar start page in which ALL calendar events is housed", GroupName = Global.GroupNames.Navigation, Order = 30)]
        public virtual ContentReference CalendarStartReference { get; set; }

        [Display(Name = "Search Results Page", Description = "The page that is used to return results", GroupName = Global.GroupNames.Navigation, Order = 40)]
        public virtual ContentReference SearchResultsPage { get; set; }

        [Display(Name = "Slides Start Page", Description = "The page that is used to return the children of slides", GroupName = Global.GroupNames.Navigation, Order = 50)]
        public virtual PageReference SlidesStartPage { get; set; }

        [Display(Name = "Number of Twitter Items", Description = "The number of twitter items shown on the home page", GroupName = Global.GroupNames.TwitterSettings, Order = 10)]
        [Range(1, 10)]
        public virtual int NumberOfTwitterItems { get; set; }

        [Display(Name = "Twitter Username", Order = 60, Description = "Twitter Username", GroupName = Global.GroupNames.TwitterSettings)]
        public virtual string TwitterUserName { get; set; }

        [Display(Name = "Mobile Banking Url", Description = "The url to mobile banking locator for phones", GroupName = Global.GroupNames.Navigation, Order = 80)]
        public virtual Url MobileBankingUrl { get; set; }

        [Display(Name = "Tablet Banking Url", Description = "The url to mobile banking locator for tablets", GroupName = Global.GroupNames.Navigation, Order = 90)]
        public virtual Url TabletBankingUrl { get; set; }

        [Display(Name = "Mobile Banking Login Page", Description = "The page in which the mobile login page will be linked too", GroupName = Global.GroupNames.Navigation, Order = 100)]
        public virtual ContentReference MobileBankingLoginPage { get; set; }

        [Display(Name = "Desktop Banking Login Page", Description = "The page in which the desktop login page will be linked too", GroupName = Global.GroupNames.Navigation, Order = 110)]
        public virtual ContentReference DesktopBankingLoginPage { get; set; }

        [Display(Name = "Login Xhtml Content", Description = "html content for the login popup screen", Order = 120, GroupName = Global.GroupNames.Navigation)]
        public virtual XhtmlString LoginContent { get; set; }

        [Display(Name = "Included Stylesheets", Description = "Multiple files can now be added to the site via link collection instead of textarea", GroupName = SystemTabNames.Settings)]
        [AllowedTypes(typeof(StylesheetFile))]
        [ScaffoldColumn(false)]
        public virtual LinkItemCollection IncludedStyleSheets { get; set; }

        [Display(Name = "Included Javscripts", Description = "Multiple files can now be added to the site via link collection instead of textarea", GroupName = SystemTabNames.Settings)]
        [AllowedTypes(typeof(JavascriptFile))]
        [ScaffoldColumn(false)]
        public virtual LinkItemCollection IncludedScripts { get; set; }

        /*Overrides*/

        [Display(Name = "Page Form", Description = "This will display a form on the page.", GroupName = Global.GroupNames.Forms, Order = 10)]
        [Editable(false)]
        [ScaffoldColumn(false)]
        public override XForm PageForm { get; set; }

        [ScaffoldColumn(false)]
        public override ContentArea CenterBlockArea { get; set; }
    }
}