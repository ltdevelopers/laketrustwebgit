﻿namespace LakeTrust.Models.Pages
{
    [SiteContentType(DisplayName = "Container", GroupName = Global.GroupNames.Specialized, GUID = "0a035e4e-a8ee-463a-ab69-8f2d4e3fc769", Description = "A container for holding content")]
    [SiteImageUrl]
    public class Container : LakeTrustBaseModel
    {
    }
}