﻿using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.XForms;
using LakeTrust.Business;
using System;
using System.ComponentModel.DataAnnotations;

namespace LakeTrust.Models.Pages
{
    [SiteContentType(DisplayName = "Calendar Event", GroupName = "Calendar", Order = 550, GUID = "431ef942-f3fc-4749-a4aa-ad1a8c720f1e", Description = "A single event in a calendar listing")]
    [SiteImageUrl]
    public class CalendarEventPage : SitePageData
    {
        public override void SetDefaultValues(ContentType contentType)
        {
            base.SetDefaultValues(contentType);
            this.IsAlert = false;
        }

        // Properties
        [Display(Name = "Event Start Date", Description = "The Date of the event", GroupName = "Information", Order = 20)]
        [Required(ErrorMessage = "You must specify an Event Date")]
        public virtual DateTime EventDateTime { get; set; }

        [Display(Name = "Event End Date", Description = "The Date-Time the event will end", GroupName = "Information", Order = 30)]
        public virtual DateTime EventEndDateTime { get; set; }

        [Display(Name = "Event End Time", Description = "The end time of the event", GroupName = "Information", Order = 40)]
        public virtual string EventEndTime { get; set; }

        [Display(Name = "Event Start Time", Description = "The time of the event", GroupName = "Information", Order = 50)]
        public virtual string EventStartTime { get; set; }

        [Display(Name = "Event Location", Description = "The location of the event", GroupName = "Information", Order = 60)]
        public virtual string EventLocation { get; set; }

        [Display(Name = "Is An Alert", Order = 40, Description = "If checked, this event will be marked as an alert.")]
        public virtual bool IsAlert { get; set; }

        [Display(Name = "Sign Up Url", Order = 50, Description = "If entered, this will display a link to the signup page.")]
        public virtual Url SignUpUrl { get; set; }

        [Display(Name = "Class Info", Order = 60, Description = "If entered, this will display a link to the class info page.")]
        public virtual Url ClassInfoUrl { get; set; }

        [ScaffoldColumn(false)]
        public override XForm PageForm { get; set; }

        public bool HasEndDate
        {
            get
            {
                return (this.EventEndDateTime.Year != 1);
            }
        }

        public string Teaser
        {
            get
            {
                return (!string.IsNullOrEmpty(this.SEODescription) ? this.SEODescription : this.GetTeaser("Teaser", 200, true));
            }
        }

        [ScaffoldColumn(false)]
        public override ContentArea CenterBlockArea { get; set; }
    }
}