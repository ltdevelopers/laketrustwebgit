﻿using EPiServer.Core;
using EPiServer.DataAnnotations;
using System;
using System.ComponentModel.DataAnnotations;

namespace LakeTrust.Models.Pages
{
    [SiteContentType(DisplayName = "Calendar Listing", GUID = "e49197f3-52d8-4a2d-838f-5ea65aee1611", GroupName = LakeTrust.Global.GroupNames.Calendar, Order = 500, Description = "A listing of calendar events")]
    [SiteImageUrl]
    [AvailableContentTypes(Include = new Type[] { typeof(CalendarPage), typeof(CalendarEventPage) })]
    public class CalendarPage : SitePageData
    {
        [ScaffoldColumn(false)]
        public override ContentArea CenterBlockArea { get; set; }
    }
}