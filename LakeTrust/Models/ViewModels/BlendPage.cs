﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LakeTrust.Models.ViewModels
{
    public class BlendPager
    {
        public BlendPager()
        {
            this.TotalPages = 1;
        }

        /// <summary>
        /// Creates a list of page numbers to be enumerated in a paging control.
        /// </summary>
        /// <remarks>Paging is 1-based, meaning that the first page is called page 1.</remarks>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="totalItems">The total items.</param>
        /// <param name="currentPage">The current page.</param>
        /// <param name="pagesShownPerPager">The amount of pages per pager based on items.</param>
        /// <returns></returns>
        public IList<PagerPage> CreatedPages(int pageWeight, int totalItems, int currentPageIndex, int visiblePages = 10)
        {
            List<PagerPage> pages = new List<PagerPage>();
            this.TotalPages = (totalItems + pageWeight - 1) / pageWeight;
            int startIndex = 0;
            int endIndex = this.totalPages;

            if (this.totalPages > visiblePages)
            {
                startIndex = currentPageIndex - (visiblePages / 2);
                endIndex = currentPageIndex + (visiblePages / 2);
                if (startIndex < 0)
                {
                    startIndex = 0;
                    endIndex = startIndex + visiblePages;
                }
                if (endIndex > this.totalPages)
                {
                    endIndex = this.totalPages;
                    startIndex = this.totalPages - visiblePages;
                }
            }

            #region PagerStuff

            pages.Add(new PagerPage("first", 1, false, false, (currentPageIndex == 1)));
            pages.Add(new PagerPage("prev", currentPageIndex == 1 ? currentPageIndex : currentPageIndex - 1, false, false, currentPageIndex == 1));

            for (int i = startIndex; i < endIndex; i++)
            {
                pages.Add(new PagerPage((i + 1).ToString(), i + 1, i == (currentPageIndex - 1), true, false));
            }

            pages.Add(new PagerPage("next", currentPageIndex == this.totalPages ? currentPageIndex : currentPageIndex + 1, false, false, currentPageIndex == this.totalPages));
            pages.Add(new PagerPage("last", this.totalPages, false, false, currentPageIndex == this.totalPages));

            #endregion PagerStuff

            return pages;
        }

        private int totalPages = 1;

        public int TotalPages
        {
            get { return totalPages; }
            private set { totalPages = value; }
        }
    }

    /// <summary>
    /// Page class containing the information used to create a paging control
    /// </summary>
    public class PagerPage
    {
        public PagerPage()
        {
        }

        public PagerPage(string title, int pageNum, bool currentPage, bool isPageNumber = true, bool isDisabled = false)
        {
            this.Title = title;
            this.PageNum = pageNum;
            this.CurrentPage = currentPage;
            this.IsPageNumber = isPageNumber;
            this.IsDisabled = isDisabled;
        }

        /// <summary>
        /// Gets or sets the title.
        /// </summary>
        /// <value>The title.</value>
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets the page number.
        /// </summary>
        /// <value>The page num.</value>
        public int PageNum { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this page is the current page.
        /// </summary>
        /// <value><c>true</c> if [current page]; otherwise, <c>false</c>.</value>
        public bool CurrentPage { get; set; }

        public bool IsPageNumber { get; set; }

        public bool IsDisabled { get; set; }
    }
}