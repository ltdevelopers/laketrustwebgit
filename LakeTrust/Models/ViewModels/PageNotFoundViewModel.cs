﻿using LakeTrust.Models.Pages;
using System;
using System.Linq;

namespace LakeTrust.Models.ViewModels
{
    public class PageNotFoundViewModel : PageViewModel<PageNotFound>
    {
        public PageNotFoundViewModel(PageNotFound currentPage)
            : base(currentPage)
        {
        }

        public virtual Uri UrlNotFound { get; set; }

        public virtual string Referer { get; set; }
    }
}