﻿using EPiServer.SpecializedProperties;
using LakeTrust.Models.Pages.HQ;
using System;
using System.Linq;

namespace LakeTrust.Models.ViewModels
{
    public class HQUpdateViewModel
    {
        public HQUpdateViewModel(HQUpdatePage currentPage)
        {
            this.MainNavigation = new LinkItemCollection();
            this.AllowComments = true;
            this.CurrentPage = currentPage;
        }

        public HQUpdatePage PreviousContent { get; set; }

        public HQUpdatePage NextContent { get; set; }

        public LinkItemCollection MainNavigation { get; set; }

        public bool AllowComments { get; set; }

        public HQUpdatePage CurrentPage { get; private set; }
    }
}