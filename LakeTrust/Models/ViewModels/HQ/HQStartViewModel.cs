﻿using EPiServer;
using EPiServer.Core;
using EPiServer.SpecializedProperties;
using LakeTrust.Models.Pages.HQ;
using System;
using System.Linq;

namespace LakeTrust.Models.ViewModels
{
    public class HQStartViewModel
    {
        public HQStartViewModel(HQStartPage currentPage)
        {
            this.MainNavigation = new LinkItemCollection();
            this.MainContent = currentPage.MainContent;
            this.Logo = currentPage.Logo;
            this.LakeTrustLogo = currentPage.LakeTrustLogo;
            this.TwitterUrl = currentPage.TwitterUrl;
            this.FacebookUrl = currentPage.FacebookUrl;
            this.InstagramUrl = currentPage.InstagramUrl;
            this.YouTubeUrl = currentPage.YouTubeUrl;
            this.EmailAddress = currentPage.EmailAddress;
        }

        public ContentArea MainContent { get; set; }

        public LinkItemCollection MainNavigation { get; set; }

        public ContentReference Logo { get; set; }

        public ContentReference LakeTrustLogo { get; set; }

        public Url TwitterUrl { get; set; }

        public Url FacebookUrl { get; set; }

        public Url InstagramUrl { get; set; }

        public Url YouTubeUrl { get; set; }

        public string EmailAddress { get; set; }
    }
}