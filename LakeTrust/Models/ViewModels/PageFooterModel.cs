﻿using EPiServer.Core;
using EPiServer.SpecializedProperties;
using System;
using System.Linq;

namespace LakeTrust.Models.ViewModels
{
    public class PageFooterModel
    {
        public XhtmlString FooterText { get; set; }

        public XhtmlString FooterAddress { get; set; }

        public LinkItemCollection JavascriptIncludes { get; set; }

        public LinkItemCollection FooterNavigation { get; set; }

        public LinkItemCollection SocialLinks { get; set; }

        public ContentArea SocialContentArea { get; set; }
    }
}