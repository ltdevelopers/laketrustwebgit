﻿using EPiServer.Core;
using LakeTrust.Models.Pages;

namespace LakeTrust.Models.ViewModels
{
    /// <summary>
    /// Defines common characteristics for view models for pages, including properties used by
    /// layout files.
    /// </summary>
    /// <remarks>
    /// Views which should handle several page types (T) can use this interface as model type rather
    /// than the concrete PageViewModel class, utilizing the that this interface is covariant.
    /// </remarks>
    public interface IPageViewModel<out T> where T : LakeTrustBaseModel
    {
        T CurrentPage { get; }

        LayoutModel Layout { get; set; }

        PageHeaderModel PageHeader { get; set; }

        PageFooterModel PageFooter { get; set; }

        IContent Section { get; set; }

        ContentReference ParentBelowStartPage { get; set; }
    }
}