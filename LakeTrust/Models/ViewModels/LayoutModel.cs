﻿using System.Web.Mvc;

namespace LakeTrust.Models.ViewModels
{
    public class LayoutModel
    {
        public MvcHtmlString CustomHeaderScripts { get; set; }

        public string BodyClasses { get; set; }

        public bool HideScreenReaderLink { get; set; }

        public PageHeaderModel PageHeader { get; set; }

        public PageFooterModel PageFooter { get; set; }

        public bool LoggedIn { get; set; }

        public bool HideHeader { get; set; }

        public bool HideFooter { get; set; }

        public MvcHtmlString LoginUrl { get; set; }

        public MvcHtmlString LogOutUrl { get; set; }
    }
}