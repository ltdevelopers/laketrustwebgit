﻿using EPiServer;
using EPiServer.Core;
using EPiServer.SpecializedProperties;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LakeTrust.Models.ViewModels
{
    public class PageHeaderModel
    {
        public LinkItemCollection HeaderNavigation { get; set; }

        public IEnumerable<IContentData> MainNavigation { get; set; }

        public List<string> StylesheetIncludes { get; set; }

        public Url MyCreditCardLink { get; set; }

        public string MyCreditCartText { get; set; }

        public IContent MobileBankingLoginPage { get; set; }

        public IContent DesktopBankingLoginPage { get; set; }

        public ContentReference SearchPageContentLink { get; set; }

        public bool IsLandingPage { get; set; }

        public LinkItemCollection HeaderJavascriptIncludes { get; set; }

        public XhtmlString LoginContent { get; set; }
    }
}