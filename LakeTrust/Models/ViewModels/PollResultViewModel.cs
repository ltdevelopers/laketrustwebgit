﻿using LakeTrust.Models.Blocks;
using System;
using System.Linq;

namespace LakeTrust.Models.ViewModels
{
    public class PollResultViewModel : BlockViewModel<PollResultBlock>
    {
        public PollResultViewModel(PollResultBlock currentBlock)
            : base(currentBlock)
        {
            this.PollResult = new PollResult();
            this.DisplayResults = false;
        }

        public bool DisplayResults { get; set; }

        public PollResult PollResult { get; set; }
    }
}