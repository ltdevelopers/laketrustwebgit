﻿using EPiServer.Core;
using LakeTrust.Models.Pages;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LakeTrust.Models.ViewModels
{
    public class SiteMapViewModel : PageViewModel<SiteMapPage>
    {
        public SiteMapViewModel(SiteMapPage currentPage)
            : base(currentPage)
        {
            this.Children = Enumerable.Empty<PageData>();
        }

        public IEnumerable<PageData> Children { get; set; }
    }
}