﻿using LakeTrust.Models.Pages;
using LinqToTwitter;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LakeTrust.Models.ViewModels
{
    public class StartPageViewModel : PageViewModel<StartPage>
    {
        public StartPageViewModel(StartPage currentPage)
            : base(currentPage)
        {
            this.EventItems = Enumerable.Empty<CalendarEventPage>();
            this.TwitterItems = Enumerable.Empty<Status>();
        }

        public IEnumerable<CalendarEventPage> EventItems { get; set; }

        public IEnumerable<Status> TwitterItems { get; set; }
    }
}