﻿using EPiServer;
using EPiServer.Core;
using System;
using System.Linq;

namespace LakeTrust.Models.ViewModels
{
    public class MenuItem
    {
        public MenuItem(PageData page)
        {
            Page = page;
        }

        public PageData Page { get; set; }

        public bool Selected { get; set; }

        public int Depth
        {
            get { return DataFactory.Instance.GetAncestors(this.Page.ContentLink).Count(); }
        }

        public Lazy<bool> HasChildren { get; set; }
    }
}