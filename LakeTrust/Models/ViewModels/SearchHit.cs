﻿using EPiServer.Core;
using EPiServer.Framework.Web;
using EPiServer.Search;
using EPiServer.ServiceLocation;
using EPiServer.Web;
using EPiServer.Web.Routing;
using LakeTrust.Business;
using LakeTrust.Models.Pages;
using System.Collections.Generic;

namespace LakeTrust.Models.ViewModels
{
    public class SearchHit
    {
        public string Title { get; set; }

        public string Url { get; set; }

        public string SearchText { get; set; }

        public float Score { get; set; }

        public IContent ContentItem { get; set; }

        public static IEnumerable<SearchHit> CreateHitModel(IndexResponseItem responseItem)
        {
            var content = ServiceLocator.Current.GetInstance<ContentSearchHandler>().GetContent<IContent>(responseItem);
            if (content != null && HasTemplate(content) && IsPublished(content as IVersionable))
            {
                yield return CreatePageHit(content, responseItem, responseItem.Score);
            }
        }

        public static IEnumerable<SearchHit> CreateHitModel(IndexResponseItem responseItem, ContentSearchHandler searchHandler)
        {
            var content = searchHandler.GetContent<IContent>(responseItem);
            if (content != null && HasTemplate(content) && IsPublished(content as IVersionable))
            {
                yield return CreatePageHit(content, responseItem, responseItem.Score);
            }
        }

        private static bool HasTemplate(IContent content)
        {
            return ServiceLocator.Current.GetInstance<TemplateResolver>().HasTemplate(content, TemplateTypeCategories.Page);
        }

        private static bool IsPublished(IVersionable content)
        {
            if (content == null)
                return true;
            return content.Status.HasFlag(VersionStatus.Published);
        }

        private static SearchHit CreatePageHit(IContent content, IndexResponseItem item, float score)
        {
            string title = content.Name;
            string summary = content is SitePageData ? ((SitePageData)content).SEODescription : item.DisplayText;

            if (string.IsNullOrWhiteSpace(summary))
            {
                summary = ((SitePageData)content).GetPropertyValue(x => x.MainBody, new XhtmlString()).ToHtmlString().CleanBody(500);
            }

            return new SearchHit
            {
                Title = title,
                Url = UrlResolver.Current.GetUrl(content.ContentLink),
                SearchText = summary,
                Score = score,
                ContentItem = content
            };
        }
    }
}