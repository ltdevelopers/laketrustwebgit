﻿using LakeTrust.Models.Pages;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LakeTrust.Models.ViewModels
{
    public class RotatorBlockViewModel
    {
        public RotatorBlockViewModel()
        {
            this.RotatorItems = new List<SlideItem>();
        }

        public List<SlideItem> RotatorItems { get; set; }
    }
}