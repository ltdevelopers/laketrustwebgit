﻿using LakeTrust.Models.Blocks;
using System;
using System.Linq;

namespace LakeTrust.Models.ViewModels
{
    public class BlockViewModel<T> : IBlockViewModel<T> where T : SiteBlockData
    {
        public BlockViewModel(T currentPage)
        {
            CurrentBlock = currentPage;
        }

        public T CurrentBlock { get; private set; }
    }

    public static class BlockViewModel
    {
        /// <summary>
        /// Returns a PageViewModel of type <typeparam name="T" />.
        /// </summary>
        /// <remarks>
        /// Convenience method for creating PageViewModels without having to specify the type as
        /// methods can use type inference while constructors cannot.
        /// </remarks>
        public static BlockViewModel<T> Create<T>(T block) where T : SiteBlockData
        {
            return new BlockViewModel<T>(block);
        }
    }
}