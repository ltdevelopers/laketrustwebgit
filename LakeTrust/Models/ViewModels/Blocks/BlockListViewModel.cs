﻿using LakeTrust.Models.Blocks;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LakeTrust.Models.ViewModels
{
    public class BlockListViewModel<T, TList> : BlockViewModel<T> where T : SiteBlockData
    {
        public BlockListViewModel(T currentPage)
            : base(currentPage)
        {
            this.Items = Enumerable.Empty<TList>();
        }

        public IEnumerable<TList> Items { get; set; }
    }
}