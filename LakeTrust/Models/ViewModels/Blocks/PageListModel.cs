﻿using EPiServer.Core;
using LakeTrust.Models.Blocks;
using System.Collections.Generic;

namespace LakeTrust.Models.ViewModels
{
    public class PageListModel : BlockViewModel<SiteBlockData>
    {
        public PageListModel(PageListBlock block)
            : base(block)
        {
            this.ShowIntroduction = block.IncludeIntroduction;
            this.ShowPublishDate = block.IncludePublishDate;
            this.DisplayPhoto = block.DisplayPhoto;
        }

        public string Heading { get; set; }

        public IEnumerable<PageData> Pages { get; set; }

        public bool ShowIntroduction { get; set; }

        public bool ShowPublishDate { get; set; }

        public bool DisplayPhoto { get; set; }
    }
}