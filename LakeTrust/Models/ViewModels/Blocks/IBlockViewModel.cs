﻿using LakeTrust.Models.Blocks;
using System;
using System.Linq;

namespace LakeTrust.Models.ViewModels
{
    public interface IBlockViewModel<out T> where T : SiteBlockData
    {
        T CurrentBlock { get; }
    }
}