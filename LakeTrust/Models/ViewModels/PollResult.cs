﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace LakeTrust.Models.ViewModels
{
    public class PollResult
    {
        public PollResult()
        {
            this.TotalItems = 0;
            this.Results = new List<Result>();
        }

        public string Question { get; set; }

        public List<Result> Results { get; set; }

        public int TotalItems { get; set; }
    }

    public class Result
    {
        public string Name { get; set; }

        public string XFormValue { get; set; }

        public int Value { get; set; }

        public double Percentage { get; set; }
    }
}