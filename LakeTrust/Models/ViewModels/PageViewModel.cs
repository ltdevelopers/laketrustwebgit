﻿using EPiServer.Core;
using LakeTrust.Models.Pages;
using System;

namespace LakeTrust.Models.ViewModels
{
    public class PageViewModel<T> : IPageViewModel<T> where T : LakeTrustBaseModel
    {
        public PageViewModel(T currentPage)
        {
            CurrentPage = currentPage;
        }

        public T CurrentPage { get; private set; }

        public LayoutModel Layout { get; set; }

        public PageHeaderModel PageHeader { get; set; }

        public PageFooterModel PageFooter { get; set; }

        public IContent Section { get; set; }

        public ContentReference ParentBelowStartPage { get; set; }
    }

    public static class PageViewModel
    {
        /// <summary>
        /// Returns a PageViewModel of type <typeparam name="T" />.
        /// </summary>
        /// <remarks>
        /// Convenience method for creating PageViewModels without having to specify the type as
        /// methods can use type inference while constructors cannot.
        /// </remarks>
        public static PageViewModel<T> Create<T>(T page) where T : LakeTrustBaseModel
        {
            return new PageViewModel<T>(page);
        }
    }
}