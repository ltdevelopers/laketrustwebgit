﻿using LakeTrust.Models.Pages;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LakeTrust.Models.ViewModels
{
    public class ListingViewModel<T, TList> : PageViewModel<T> where T : LakeTrustBaseModel
    {
        public ListingViewModel(T currentPage)
            : base(currentPage)
        {
            this.Pages = new List<TList>();
        }

        public IEnumerable<TList> Pages { get; set; }

        public int PageWeight { get; set; }

        public int TotalItems { get; set; }

        public int PageIndex { get; set; }

        public string SearchTerm { get; set; }
    }
}