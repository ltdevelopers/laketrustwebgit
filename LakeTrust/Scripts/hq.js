﻿function viewport() {
    var e = window, a = 'inner';
    if (!('innerWidth' in window)) {
        a = 'client';
        e = document.documentElement || document.body;
    }
    return { width: e[a + 'Width'], height: e[a + 'Height'] };
}

(function ($) {
    var $event = $.event,
        $special,
        resizeTimeout;

    $special = $event.special.debouncedresize = {
        setup: function () {
            $(this).on("resize", $special.handler);
        },
        teardown: function () {
            $(this).off("resize", $special.handler);
        },
        handler: function (event, execAsap) {
            // Save the context
            var context = this,
                args = arguments,
                dispatch = function () {
                    // set correct event type
                    event.type = "debouncedresize";
                    $event.dispatch.apply(context, args);
                };

            if (resizeTimeout) {
                clearTimeout(resizeTimeout);
            }

            execAsap ?
                dispatch() :
                resizeTimeout = setTimeout(dispatch, $special.threshold);
        },
        threshold: 150
    };
})(jQuery);

function scrollToTop() {
    $('html, body').animate({ scrollTop: 0 }, 500);
    return false;
}

function goToByScroll(id) {
    $('html,body').animate({ scrollTop: $(id).offset().top - 30 }, 500);
}

function scrollToClass(className) {
    $('html,body').animate({ scrollTop: $("." + className).offset().top - 27 }, 500);
}

function equalHeightElements() {
    $(".height-set").removeAttr("style").removeClass("height-set");
    var windowWidth = viewport().width;
    if (windowWidth > 767) {
        $(".hq-start").each(function () {
            var maxHeight = 0;
            var items = $(".metro", $(this));
            if (!$(this).children().first().hasClass("col-md-12")) {
                items.each(function () {
                    var height = $(this).innerHeight();
                    if (height > maxHeight) { maxHeight = height; }
                });

                items.css('height', maxHeight).addClass("height-set");
            }
        });
    }
}

$(function () {
    $(".hq-start-page #hq-navigation").on("click", "a", function (e) {
        e.preventDefault();
        goToByScroll($(this).attr("href"))
    });

    $(".hq-start-page").on("click", ".logo", function (e) {
        scrollToTop();
    });

    $("div.questionanswer").mouseenter(function () {
        $(".question", this).slideUp('fast');
        $(".answer", this).slideDown('fast');
        $(this).removeClass("blue").addClass("green");
    }).mouseleave(function () {
        $(".answer", this).slideUp('fast');
        $(".question", this).slideDown('fast');
        $(this).removeClass("green").addClass("blue");
    });

    $(".update").mouseenter(function () {
        var a = $(this);
        var item = $(".overlay", a);
        var height = a.height();
        item.animate({
            height: height + 'px'
        }, 250);
    }).mouseleave(function () {
        $(".overlay", $(this)).animate({
            height: '0px'
        }, 250);
    });
    $(".form form table tr td").each(function (i) {
        var td = $(this);
        var input = $("input", td);
        var label = $("label", td);
        var textarea = $("textarea", td);
        if (input != null)
            if (input.attr("type") == "text")
                input.attr("placeholder", label.attr("title"));

        if (textarea != null)
            textarea.attr("placeholder", label.attr("title"));
    })

    if ($(".rslides").length > 0) {
        $(".rslides").responsiveSlides({
            pager: false,
            auto: false,
            nav: true
        });
    }
})