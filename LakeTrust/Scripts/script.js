//var dropdown = new TINY.dropdown.init("dropdown", { id: 'main-menu', active: 'menuhover' });
var interval = null;
var loginItem = $("#main-menu #menu li.online-login");
var isLoginClicked = false;
jQuery.expr[':'].hasAttr = function (elem) {
    return elem.attributes.length;
};
function goToByScroll(id) {
    $('html,body').animate({ scrollTop: $("#" + id).offset().top - 80 }, 500);
}
function resizeText(multiplier) {
    var pagebody = $("#LakeTrustPage")
    pagebody.removeClass("large").removeClass("xlarge");
    if (multiplier != "normal") {
        pagebody.addClass(multiplier);
    }
}

function callFunc() {
    jQuery(".es-nav-next").trigger('click');
}

$(window).load(function () {
    $('.placeholder-swap').isCheck(function () {
        $(this).each(function () {
            var inpt = this, phtext = '';

            $('label[for="' + inpt.id + '"]').isCheck(function () {
                phtext = $(this)[0].innerHTML.toLowerCase();
                $(this).addClass('visuallyhidden');
            });
            $(this).data('orig', phtext);
            $(this).val(phtext);

            $(this).focus(function () {
                if ($(this).val().trim() == $(this).data('orig')) {
                    $(this).val('');
                }
            });

            $(this).blur(function () {
                if ($(this).val().trim() == '') {
                    $(this).val($(this).data('orig'));
                } else {
                    $(this).val($(this).val().trim());
                }
            });
        });
    });

    $('.login-btn').click(function (e) {
        e.preventDefault();
        var menu = $(this).next();
        menu.toggle();
    });

    $('#myTab a').click(function (e) {
        e.preventDefault();
        $(this).tab('show');
    });

    $("#CCLoginBtn").click(function (e) {
        window.location.href = "https://www.epscu.com/servlet/3132/raAppHtml?ServicePageName=com.invisiondev.JServer.xml.xmlProcess&RQID=LOGINMFA&ORGID=3132&USERNAMEL=" + $("#CCLoginName").val();
    });

    $("#main-navigation .dropdown").hover(
        function () {
            $("b", $(this)).removeClass("caret-right").addClass("caret");
        }, function () {
            $("b", $(this)).removeClass("caret").addClass("caret-right");
        });

    if ($(".rslides").length > 0) {
        $(".rslides").responsiveSlides({
            pager: true
        });
    }

    $("#searchtext").keydown(function (ev) {
        var keycode = (ev.keyCode ? ev.keyCode : ev.which);
        if (keycode == '13') {
            window.location.href = $("#searchUrl").val() + "?q=" + $("#searchtext").val();
        }
    });

    $("#CCLoginName").click(function (e) {
        e.preventDefault();
        $(this).parent().parent().parent().addClass("open");
    });

    $("#userid,#password").focus(function (e) {
        $(this).closest(".online-login").addClass("on");
    });

    $(".rslides_tabs").width($(".rslides_tabs li").length * 18);
    $(".rslides_tabs").append("<li class=\"clearfix\"></li>");
});

$(function () {
    if (window.PIE) {
        $('.rounded').each(function () {
            PIE.attach(this);
        });
        $('.page-widget.sb').each(function () {
            PIE.attach(this);
        });
        $('.widget.sb').each(function () {
            PIE.attach(this);
        });
        $('.btn-primary').each(function () {
            PIE.attach(this);
        });
    }
});

$("#main-menu #menu li").not(".online-login").hover(function () {
    loginItem.removeClass("on").removeClass("login-active");
});

$("a.level-one", loginItem).click(function (e) {
    var parentItem = $(this).parent();
    if (parentItem.hasClass("on")) {
        parentItem.removeClass("on");
    }
    else {
        parentItem.addClass("on");
    }
});

$(loginItem).hover(function () {
    $(this).addClass("login-active");
}, function () {
    setTimeout(function () {
        loginItem.removeClass("login-active")
    }, 1500);
});

$("#CalculateRewards").on("click", "input:checkbox", function () {
    var services = $(".services-container", "#CalculateRewards");
    var moreServices = $(".moreservices-container", "#CalculateRewards");
    var questions = $(".questions-container", "#CalculateRewards");

    var totalServices = $(".question-item", services).length;
    var weightedPoints = 0;
    var points = 0;
    var totalSelectedServices = 0;
    var hasATMRefunds = false;
    var hasPoints = false;

    $(".data-item", services).each(function (i) {
        var obj = $(this);
        var checked = obj.find("input").is(":checked");
        if (checked) {
            totalSelectedServices++;
        }
    });

    $(".data-item", moreServices).each(function (i) {
        var obj = $(this);
        var checked = obj.find("input").is(":checked");
        if (checked) {
            var objPoint = obj.data("point");
            var objWeight = obj.data("weightpoint");
            points = points + objPoint;
            weightedPoints = weightedPoints + objWeight;
        }
    });

    $(".data-item", questions).each(function (i) {
        var obj = $(this);
        var checked = obj.find("input").is(":checked");
        if (checked) {
            var objPoint = obj.data("point");
            var objWeight = obj.data("weightpoint");
            points = points + objPoint;
            weightedPoints = weightedPoints + objWeight;
        }
    });

    if (totalServices == totalSelectedServices) {
        hasATMRefunds = true;
        weightedPoints = weightedPoints + 22.50;
    }

    if (weightedPoints > 0) {
        if (weightedPoints > 100)
            weightedPoints = 100;
        $("#TotalRefund").html(CurrencyFormatted(weightedPoints * 0.10)).removeClass("hide");
        hasPoints = true;
    }

    if (hasATMRefunds) {
        $("#NoResults").addClass("hide");
        $("#NoRewards").addClass("hide");
        $("#CalculateResults").removeClass("hide");
        $("#RewardsTitle").removeClass("hide");
        $('#CalculateResults .rounded').matchHeight(true);
    } else {
        $("#NoResults").removeClass("hide");
        $("#NoRewards").removeClass("hide");
        $("#RewardsTitle").addClass("hide");
        $("#CalculateResults").addClass("hide");
    }
});

function CurrencyFormatted(amount) {
    var i = parseFloat(amount);
    if (isNaN(i)) { i = 0.00; }
    var minus = '';
    if (i < 0) { minus = '-'; }
    i = Math.abs(i);
    i = parseInt((i + .005) * 100);
    i = i / 100;
    s = new String(i);
    if (s.indexOf('.') < 0) { s += '.00'; }
    if (s.indexOf('.') == (s.length - 2)) { s += '0'; }
    s = minus + s;
    return "$" + s;
}

function enableMe() {
    document.getElementById("dscheck").value = "0";
}

function isCookieEnabled() {
    var exp = new Date(); exp.setTime(exp.getTime() + 1800000);
    setCookie("testCookie", "cookie", exp, false, false, false);
    if (document.cookie.indexOf('testCookie') == -1) {
        return false;
    }
    exp = new Date();
    exp.setTime(exp.getTime() - 1800000);
    setCookie("testCookie", "cookie", exp, false, false, false);
    return true;
}

function setCookie(name, value, expires, path, domain, secure) {
    var curCookie = name + "=" + value +
    ((expires) ? "; expires=" + expires.toGMTString() : "") + ((path) ? "; path=" + path : "") +
    ((domain) ? "; domain=" + domain : "") + ((secure) ? "; secure" : "");
    document.cookie = curCookie;
}

function isDupSubmit() {
    var dupSbmt = true;
    var e = document.getElementById("dscheck");
    if (e != null && e.value == "0") {
        dupSbmt = false; e.value = "1"; setTimeout(enableMe, 5000);
    }
    return dupSbmt;
}

function setParamStatus() {
    if (!isDupSubmit()) {
        if (isCookieEnabled()) {
            document.getElementById('testcookie').value = 'true';
        }
        document.getElementById('testjs').value = 'true';
        return true;
    }
    return false;
}