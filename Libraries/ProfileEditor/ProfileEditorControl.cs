﻿using EPiServer.Core;
using EPiServer.Personalization;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ProfileEditor.Implementation
{
    public class ProfileEditorControl : UserControl
    {
        private List<EditorConfigPair> _handlers;

        /// <summary>
        /// Finds configured handlers for properties, if any, and loads them
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="data"></param>
        public void LoadSettings(string userName, EPiServerProfile data)
        {
            _handlers = new List<EditorConfigPair>();

            // Walk through the properies of the EPiServerProfile and check if
            // there is a configured handler for this property
            foreach (SettingsPropertyValue property in data.PropertyValues)
            {
                EditorConfigPair handler;
                if (ProfileEditorFactory.Instance.Handlers.TryGetValue(property.Name, out handler))
                {
                    // Found a handler for the property, save it in the list of handlers
                    // and load the property data into it
                    _handlers.Add(handler);
                    handler.Editor.LoadProperty(data[handler.Configuration.PropertyName]);
                }
            }

            // Sort the list of handlers according to the configured sort index
            _handlers.Sort(new EditorConfigPairComparer());

            // Add the controls inside a containing div
            this.Controls.Add(new LiteralControl("<fieldset id=\"profileeditor\">"));
            foreach (EditorConfigPair handler in _handlers)
            {
                // Add the header and rendering control for each property
                string heading = handler.Configuration.EditHeading;
                if (String.IsNullOrEmpty(heading))
                {
                    heading = handler.Configuration.PropertyName;
                }
                if (heading.StartsWith("/"))
                {
                    // Possible to use language paths
                    heading = LanguageManager.Instance.Translate(heading);
                }

                // Make sure editor has an ID
                if (String.IsNullOrEmpty(handler.Editor.RenderingControl.ID))
                {
                    handler.Editor.RenderingControl.ID = handler.Configuration.PropertyName + "_Editor";
                }

                // Create label
                Label label = new Label();
                label.AssociatedControlID = handler.Editor.RenderingControl.ID;
                label.Text = heading;

                // Add label and control in a div
                this.Controls.Add(new LiteralControl("<div>"));
                this.Controls.Add(label);
                this.Controls.Add(handler.Editor.RenderingControl);
                this.Controls.Add(new LiteralControl("</div>"));
            }
            this.Controls.Add(new LiteralControl("</fieldset>"));
        }

        /// <summary>
        /// Returns true if any of the rendering controls needs a refresh
        /// </summary>
        public bool SaveRequiresUIReload
        {
            get
            {
                // Check if necessary for any handler
                foreach (EditorConfigPair handler in _handlers)
                {
                    if (handler.Editor.SaveRequiresUIReload)
                    {
                        // Necessary for at least one handler
                        return true;
                    }
                }

                // Not necessary for any handler
                return false;
            }
        }

        /// <summary>
        /// Calls all rendering controls to get their new property values
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="data"></param>
        public void SaveSettings(string userName, EPiServerProfile data)
        {
            foreach (EditorConfigPair handler in _handlers)
            {
                data[handler.Configuration.PropertyName] = handler.Editor.SaveProperty();
            }
        }

        /// <summary>
        /// Helper to add a control in a div classed "content" to this
        /// </summary>
        /// <param name="control"></param>
        protected void AddContentDiv(Control control)
        {
            this.Controls.Add(new LiteralControl("<div class=\"content\">"));
            this.Controls.Add(control);
            this.Controls.Add(new LiteralControl("</div>"));
        }

        /// <summary>
        /// Helper to add a control in a div classed "heading" to this
        /// </summary>
        /// <param name="control"></param>
        protected void AddHeadingDiv(Control control)
        {
            this.Controls.Add(new LiteralControl("<div class=\"heading\">"));
            this.Controls.Add(control);
            this.Controls.Add(new LiteralControl("</div>"));
        }
    }
}