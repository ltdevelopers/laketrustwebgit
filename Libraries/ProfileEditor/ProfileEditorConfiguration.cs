﻿using System;
using System.Configuration;

namespace ProfileEditor.Implementation
{
    /// <summary>
    /// Represents the profileEditor configuration section of web.config
    /// </summary>
    public class ProfileEditorConfiguration : ConfigurationSection
    {
        /// <summary>
        /// Returns an ProfileEditorConfiguration instance
        /// </summary>
        public static ProfileEditorConfiguration GetConfig()
        {
            try
            {
                return ConfigurationManager.GetSection("profileEditor") as ProfileEditorConfiguration;
            }
            catch (Exception e)
            {
                throw new Exception(String.Format("Error when reading profileEditor section from web.config: {0}", e.Message));
            }
        }

        [ConfigurationProperty("handlers")]
        public ProfileEditorConfigurationCollection Handlers
        {
            get
            {
                try
                {
                    return this["handlers"] as ProfileEditorConfigurationCollection;
                }
                catch (Exception e)
                {
                    throw new Exception(String.Format("Error when reading handlers from profileEditor section in web.config: {0}", e.Message));
                }
            }
        }
    }
}