﻿using System.Collections.Generic;

namespace ProfileEditor.Implementation
{
    /// <summary>
    /// Container class to pair an IProfileEditor object with an ProfileEditorConfigurationElement object
    /// </summary>
    public class EditorConfigPair
    {
        private IProfileEditor _editor;
        private ProfileEditorConfigurationElement _configuration;

        public EditorConfigPair(IProfileEditor editor, ProfileEditorConfigurationElement config)
        {
            this.Editor = editor;
            this.Configuration = config;
        }

        public IProfileEditor Editor
        {
            get { return _editor; }
            set { _editor = value; }
        }

        public ProfileEditorConfigurationElement Configuration
        {
            get { return _configuration; }
            set { _configuration = value; }
        }
    }

    /// <summary>
    /// Comparer for EditorConfigPair, compares the EditorConfigPair.Configuration.SortOrder
    /// </summary>
    public class EditorConfigPairComparer : IComparer<EditorConfigPair>
    {
        #region IComparer<EditorConfigPair> Members

        public int Compare(EditorConfigPair x, EditorConfigPair y)
        {
            return x.Configuration.SortOrder.CompareTo(y.Configuration.SortOrder);
        }

        #endregion IComparer<EditorConfigPair> Members
    }
}