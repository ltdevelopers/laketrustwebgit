﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Reflection;

namespace ProfileEditor.Implementation
{
    /// <summary>
    /// Singleton factory class which loads IProfileEditor instances according to the
    /// ProfileEditorConfiguration
    /// </summary>
    public class ProfileEditorFactory
    {
        #region Private members

        private static object _lockObject = new object();
        private static ProfileEditorFactory _instance;
        private ProfileEditorConfiguration _configuration;
        private Dictionary<string, EditorConfigPair> _handlers;

        #endregion Private members

        /// <summary>
        /// Singleton instance for this class
        /// </summary>
        public static ProfileEditorFactory Instance
        {
            get
            {
                lock (_lockObject)
                {
                    if (_instance == null)
                    {
                        _instance = new ProfileEditorFactory();
                    }
                }
                return _instance;
            }
        }

        /// <summary>
        /// The available handlers with respective configurations as
        /// EditorConfigPair instances
        /// </summary>
        public Dictionary<string, EditorConfigPair> Handlers
        {
            get
            {
                if (_handlers == null)
                {
                    GetHandlers();
                }
                return _handlers;
            }
        }

        /// <summary>
        /// The current configuration
        /// </summary>
        protected ProfileEditorConfiguration Configuration
        {
            get
            {
                if (_configuration == null)
                {
                    _configuration = ProfileEditorConfiguration.GetConfig();
                }
                return _configuration;
            }
        }

        /// <summary>
        /// Helper method which uses reflection to load instances of all configured
        /// IProfileEditor implementations
        /// </summary>
        private void GetHandlers()
        {
            try
            {
                Dictionary<string, EditorConfigPair> handlers = new Dictionary<string, EditorConfigPair>();

                // Group configured classes by assembly in a dictionary
                Dictionary<string, List<ProfileEditorConfigurationElement>> handlerInfo = new Dictionary<string, List<ProfileEditorConfigurationElement>>();
                foreach (ProfileEditorConfigurationElement handlerConfig in Configuration.Handlers)
                {
                    if (!handlerInfo.ContainsKey(handlerConfig.Assembly))
                    {
                        handlerInfo.Add(handlerConfig.Assembly, new List<ProfileEditorConfigurationElement>());
                    }
                    handlerInfo[handlerConfig.Assembly].Add(handlerConfig);
                }

                // Walk through assemblies and load classes
                foreach (string assemblyName in handlerInfo.Keys)
                {
                    string path = Path.Combine(AppDomain.CurrentDomain.RelativeSearchPath, String.Format("{0}.dll", assemblyName));
                    Assembly assembly = Assembly.LoadFrom(path);

                    // Load classes for this assembly
                    foreach (ProfileEditorConfigurationElement handlerConfig in handlerInfo[assemblyName])
                    {
                        Type t = assembly.GetType(handlerConfig.RenderingClass);

                        if (t != null)
                        {
                            // Check if the type is a non-abstract implementation of the IProfileEditor interface
                            if (t.IsClass && !t.IsAbstract && (t.GetInterface(typeof(IProfileEditor).Name) != null))
                            {
                                // Load and init class and add to collection
                                IProfileEditor editor = null;
                                try
                                {
                                    editor = assembly.CreateInstance(t.FullName) as IProfileEditor;
                                }
                                catch (Exception e)
                                {
                                    throw new Exception(String.Format("Error creating instance of {0} from assembly {1}: {2}",
                                        t.FullName, path, e.Message));
                                }

                                try
                                {
                                    editor.Init(handlerConfig.Options);
                                }
                                catch (Exception e)
                                {
                                    throw new Exception(String.Format("Error initializing instance of {0} from assembly {1}: {2}",
                                        t.FullName, path, e.Message));
                                }

                                handlers.Add(handlerConfig.PropertyName, new EditorConfigPair(editor, handlerConfig));
                            }
                        }
                        else
                        {
                            throw new Exception(String.Format("Error finding type {0} in assembly {1}", t.FullName, path));
                        }
                    }
                }
                _handlers = handlers;
            }
            catch (Exception e)
            {
                throw new ConfigurationErrorsException(String.Format("Error executing configuration for ProfileEditor: ", e.Message));
            }
        }
    }
}