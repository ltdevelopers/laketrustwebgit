﻿using System.Web.UI;

namespace ProfileEditor.Implementation
{
    /// <summary>
    /// Interface used by ProfileEditorControl/ProfileEditorFactory.
    /// This interface must be implemented for editor controls that are configured in
    /// the profileEditor section of web.config
    /// </summary>
    public interface IProfileEditor
    {
        /// <summary>
        /// This method is called directly after the constructor. Perform any initialization here.
        /// </summary>
        /// <param name="configOptions"></param>
        void Init(string configOptions);

        /// <summary>
        /// After initialization this should return the control responsible for editing
        /// the property value
        /// </summary>
        Control RenderingControl
        {
            get;
        }

        /// <summary>
        /// Use this method to populate RenderingControl according to the current property
        /// value etc.
        /// </summary>
        /// <param name="propertyValue">The current property value</param>
        void LoadProperty(object propertyValue);

        /// <summary>
        /// True if the UI should be reloaded after SaveProperty is called
        /// </summary>
        bool SaveRequiresUIReload
        {
            get;
        }

        /// <summary>
        /// Should return the new property value cast as object. Return null to erase the
        /// property value.
        /// </summary>
        /// <returns></returns>
        object SaveProperty();
    }
}