﻿using System;
using System.Configuration;

namespace ProfileEditor.Implementation
{
    /// <summary>
    /// Represents the collection of elements in the profileEditor/handlers section of
    /// web.config
    /// </summary>
    public class ProfileEditorConfigurationCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new ProfileEditorConfigurationElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((ProfileEditorConfigurationElement)element).PropertyName;
        }
    }

    /// <summary>
    /// Represents one item in the profileEditor/handlers section of
    /// web.config
    /// </summary>
    public class ProfileEditorConfigurationElement : ConfigurationElement
    {
        /// <summary>
        /// The user profile property handled by the handler
        /// </summary>
        [ConfigurationProperty("propertyName", IsRequired = true)]
        public string PropertyName
        {
            get
            {
                return this["propertyName"] as string;
            }
        }

        /// <summary>
        /// The type (full class path and assembly) used to render
        /// this property, format: Namespace.Class, Assembly
        /// </summary>
        [ConfigurationProperty("type", IsRequired = true)]
        public string Type
        {
            get
            {
                return this["type"] as string;
            }
        }

        /// <summary>
        /// The sort index of the handler
        /// </summary>
        [ConfigurationProperty("sortIndex", IsRequired = true)]
        public int SortOrder
        {
            get
            {
                return (int)this["sortIndex"];
            }
        }

        /// <summary>
        /// The heading placed above the control that renders
        /// the property
        /// </summary>
        [ConfigurationProperty("editHeading", IsRequired = false)]
        public string EditHeading
        {
            get
            {
                return this["editHeading"] as string;
            }
        }

        /// <summary>
        /// Freetext field for any handler-specific options
        /// </summary>
        [ConfigurationProperty("options", IsRequired = false)]
        public string Options
        {
            get
            {
                return this["options"] as string;
            }
        }

        /// <summary>
        /// Shorthand for the first part of Type (class path)
        /// </summary>
        public string RenderingClass
        {
            get
            {
                return GetCommaSeparatedValue(Type, 0);
            }
        }

        /// <summary>
        /// Shorthand for the second part of Type (assembly name)
        /// </summary>
        public string Assembly
        {
            get
            {
                return GetCommaSeparatedValue(Type, 1);
            }
        }

        /// <summary>
        /// Helper method to split a comma separated string and get one of the values
        /// </summary>
        /// <param name="source"></param>
        /// <param name="valueIndex"></param>
        /// <returns></returns>
        protected string GetCommaSeparatedValue(string source, int valueIndex)
        {
            try
            {
                return source.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries)[valueIndex].Trim();
            }
            catch (Exception)
            {
                throw new Exception(String.Format("Error extracting value with index {0} in comma separated string \"{1}\"", valueIndex, source));
            }
        }
    }
}