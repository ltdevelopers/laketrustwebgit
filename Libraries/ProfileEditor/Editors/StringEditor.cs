﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ProfileEditor.Implementation.Editors
{
    /// <summary>
    /// Class for editing string properties
    /// </summary>
    public class StringEditor : EditorBase
    {
        private Control _renderingControl;

        #region IProfileEditor Members

        public override void Init(string configOptions)
        {
            base.Init(configOptions);

            // Use a textbox for user input
            _renderingControl = new TextBox();
        }

        public override Control RenderingControl
        {
            get { return _renderingControl; }
        }

        public override void LoadProperty(object propertyValue)
        {
            // Put the current property value in the textbox
            ((TextBox)RenderingControl).Text = propertyValue as string;
        }

        public override object SaveProperty()
        {
            // Return the contents of the text box, or null if it's empty
            string newValue = ((TextBox)RenderingControl).Text;
            if (String.IsNullOrEmpty(newValue))
            {
                return null;
            }
            else
            {
                return newValue;
            }
        }

        #endregion IProfileEditor Members
    }
}