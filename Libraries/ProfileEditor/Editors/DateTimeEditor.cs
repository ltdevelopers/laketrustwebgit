﻿using EPiServer.Core;
using System;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ProfileEditor.Implementation.Editors
{
    /// <summary>
    /// Class for editing datetime properties
    /// </summary>
    public class DateTimeEditor : EditorBase
    {
        private const string DateControlName = "Date";
        private const string DateValidatorName = "DateValidator";
        private Control _renderingControl;

        #region IProfileEditor Members

        public override void Init(string configOptions)
        {
            base.Init(configOptions);

            // Use a textbox paired with a custom validator to edit the date
            UserControl parent = new UserControl();
            TextBox date = new TextBox();
            date.ID = DateControlName;
            parent.Controls.Add(date);

            CustomValidator dateValidator = new CustomValidator();
            dateValidator.ID = DateValidatorName;
            dateValidator.ControlToValidate = DateControlName;
            dateValidator.Text = "*";
            dateValidator.Display = ValidatorDisplay.Dynamic;
            dateValidator.ValidateEmptyText = true;
            dateValidator.EnableClientScript = false;
            dateValidator.ServerValidate += new ServerValidateEventHandler(dateValidator_ServerValidate);
            parent.Controls.Add(dateValidator);

            _renderingControl = parent;
        }

        protected void dateValidator_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (!CheckDateInput(args.Value))
            {
                args.IsValid = false;

                UserControl parent = _renderingControl as UserControl;
                CustomValidator dateValidator = parent.FindControl(DateValidatorName) as CustomValidator;
                dateValidator.ErrorMessage = String.Format(
                    LanguageManager.Instance.Translate("/edit/editxform/datatypes/isodate/errormessage"), args.Value);
            }
        }

        /// <summary>
        /// Helper to validate if input is a valid date and time
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        protected bool CheckDateInput(string input)
        {
            if (AllowEmpty && String.IsNullOrEmpty(input))
            {
                return true;
            }
            else
            {
                DateTime date;
                return DateTime.TryParse(input, out date);
            }
        }

        /// <summary>
        /// The option AllowEmpty from the configuration, defaults to false
        /// If true validation of an empty box will succeed and the value
        /// will be saved as null (not set), otherwise a correct date input will be
        /// required
        /// </summary>
        protected bool AllowEmpty
        {
            get
            {
                bool value;
                string allowEmpty;
                if (Options.TryGetValue("AllowEmpty", out allowEmpty) && Boolean.TryParse(allowEmpty, out value))
                {
                    return value;
                }
                else
                {
                    // Default to false
                    return false;
                }
            }
        }

        /// <summary>
        /// The option DateFormat from the configuration, defaults to
        /// yyyy-MM-dd. Used to display the current value
        /// </summary>
        protected string DateFormat
        {
            get
            {
                string dateFormat = Options["DateFormat"];
                if (!Options.TryGetValue("DateFormat", out dateFormat))
                {
                    // default
                    dateFormat = "yyyy-MM-dd";
                }
                return dateFormat;
            }
        }

        public override System.Web.UI.Control RenderingControl
        {
            get { return _renderingControl; }
        }

        public override void LoadProperty(object propertyValue)
        {
            UserControl parent = _renderingControl as UserControl;
            TextBox dateControl = parent.FindControl(DateControlName) as TextBox;

            DateTime date = (DateTime)propertyValue;

            // If the date is not set (null), the cast will cause it to be MinValue
            if (date > DateTime.MinValue)
            {
                dateControl.Text = date.ToString(DateFormat);
            }
            else
            {
                dateControl.Text = String.Empty;
            }
        }

        public override object SaveProperty()
        {
            UserControl parent = _renderingControl as UserControl;
            TextBox dateControl = parent.FindControl(DateControlName) as TextBox;

            if (AllowEmpty && String.IsNullOrEmpty(dateControl.Text))
            {
                return null;
            }
            else
            {
                // This is a special solution because the page cycle somehow prevents the validator
                // server event to be fired

                CustomValidator dateValidator = parent.FindControl(DateValidatorName) as CustomValidator;

                // If IsValid is false we can be sure the server validation event has been fired,
                // otherwise we do an extra check
                if (dateValidator.IsValid)
                {
                    if (!CheckDateInput(dateControl.Text))
                    {
                        // It turns out the validator was falsely true, in this case use the alternate way
                        // of throwing an EPiServerException to make the message appear like other validator
                        // messages in the control. Also set IsValid to false to trigger the mark (dateValidator.Text)
                        // on the control. Only one error message will be displayed as long as dateValidator.ErrorMessage
                        // is empty (which it is since it's text is set in the server validate event)
                        dateValidator.IsValid = false;
                        string error = String.Format(
                            LanguageManager.Instance.Translate("/edit/editxform/datatypes/isodate/errormessage"), dateControl.Text);
                        throw new EPiServerException(error);
                    }
                }
                return DateTime.Parse(dateControl.Text);
            }
        }

        #endregion IProfileEditor Members
    }
}