﻿using System;
using System.Collections.Generic;

namespace ProfileEditor.Implementation.Editors
{
    /// <summary>
    /// Abstract base class for implementations of the IProfileEditor interface
    /// Adds the Options property which contains a key-value collection of the parsed
    /// options attribute in the configuration file.
    /// </summary>
    public abstract class EditorBase : IProfileEditor
    {
        private Dictionary<string, string> _options;

        /// <summary>
        /// Key-value collection of the options contained in the options attribute in
        /// the config file
        /// </summary>
        public Dictionary<string, string> Options
        {
            get { return _options; }
            set { _options = value; }
        }

        #region IProfileEditor Members

        /// <summary>
        /// When overriding this method a call to base.Init() should be added if
        /// the Options property should be populated
        /// </summary>
        /// <param name="configOptions"></param>
        public virtual void Init(string configOptions)
        {
            // Extract the options from the configOptions string
            Dictionary<string, string> options = new Dictionary<string, string>();
            if (configOptions != null)
            {
                string[] nameValuePairs = configOptions.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);

                foreach (string nameValuePair in nameValuePairs)
                {
                    string[] nameValue = nameValuePair.Split(new char[] { '=' }, StringSplitOptions.RemoveEmptyEntries);
                    string name = nameValue[0].Trim();
                    string value = nameValue[1].Trim().Trim(new char[] { '\'' });
                    options.Add(name, value);
                }

                _options = options;
            }
        }

        public abstract System.Web.UI.Control RenderingControl
        {
            get;
        }

        public abstract void LoadProperty(object propertyValue);

        /// <summary>
        /// Defaults to false, override if true is needed
        /// </summary>
        public virtual bool SaveRequiresUIReload
        {
            get { return false; }
        }

        public abstract object SaveProperty();

        #endregion IProfileEditor Members
    }
}