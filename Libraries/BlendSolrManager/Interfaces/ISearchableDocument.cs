﻿using System;
using System.Linq;

namespace BlendSolrManager.Interfaces
{
    public interface ISearchableDocument : ISearchableDocument<ISolrDocument>
    { }

    public interface ISearchableDocument<T> where T : ISolrDocument
    {
        T GetSolrDocument();

        bool ExcludeFromIndex { get; set; }
    }
}