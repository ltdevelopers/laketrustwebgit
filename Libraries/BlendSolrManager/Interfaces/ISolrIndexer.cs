﻿using EPiServer.Core;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BlendSolrManager.Interfaces
{
    public interface ISolrIndexer
    {
        void Clear();

        void Index(IContent content, bool indexFiles = true, bool commit = false);

        void Index(IEnumerable<IContent> content, bool indexFiles = true, int commitIncrement = 25);

        void Delete(ISolrDocument content);

        void Delete(PageData content);

        void IndexPageTree(IContent content);
    }
}