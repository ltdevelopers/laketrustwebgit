﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace BlendSolrManager.Interfaces
{
    public interface ISolrDocument
    {
        string Id { get; set; }

        string ContentGuid { get; set; }

        string SiteId { get; set; }

        int ParentId { get; set; }

        int[] AncestorIds { get; set; }

        int[] MappedContentIds { get; set; }

        string ContentType { get; set; }

        string ContentTypeName { get; set; }

        string CreatedBy { get; set; }

        DateTime Created { get; set; }

        DateTime StartPublish { get; set; }

        DateTime StopPublish { get; set; }

        string Title { get; set; }

        string LinkUrl { get; set; }

        string Language { get; set; }

        ICollection<string> ACL { get; set; }

        bool VisibleInMenu { get; set; }

        ICollection<string> Categories { get; set; }

        string FriendlyUrl { get; set; }

        string SiteUrl { get; set; }

        string FullUrl { get; set; }

        List<string> Content { get; set; }
    }
}