﻿using BlendSolrManager.Enums;
using BlendSolrManager.Extensions;
using BlendSolrManager.Interfaces;
using EPiServer;
using EPiServer.Core;
using EPiServer.ServiceLocation;
using EPiServer.Web.Hosting;
using log4net;
using SolrNet;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;

namespace BlendSolrManager.Services
{
    /// <summary>
    /// The default interface that allows you to index episerver IContent
    /// </summary>
    /// <typeparam name="T">The default solr document type used for passing to Solr.</typeparam>
    public class SolrIndexer<T> : ISolrIndexer where T : ISolrDocument
    {
        private readonly IContentRepository contentRepository;
        private static ILog log = LogManager.GetLogger(typeof(SolrIndexer<T>));
        private ISolrOperations<T> solrnetFactory;
        private string solrConnectionStringName = string.Empty;

        public SolrIndexer()
            : this(DataFactory.Instance, ServiceLocator.Current.GetInstance<ISolrOperations<T>>())
        {
        }

        public SolrIndexer(IContentRepository contentRepository, ISolrOperations<T> solrOperations)
        {
            this.contentRepository = DataFactory.Instance;
            this.solrnetFactory = solrOperations;
        }

        /// <summary>
        /// Clears Entire Index
        /// </summary>
        public void Clear()
        {
            this.SolrSearchFactory.Delete(new SolrHasValueQuery("Id"));
            this.SolrSearchFactory.Commit();
        }

        /// <summary>
        /// Index SolrDocument from IContent.
        /// </summary>
        /// <param name="content">EPiServer IContent</param>
        /// <param name="indexFiles">Will index files</param>
        /// <param name="commit">Commit to Solr?</param>
        public void Index(IContent content, bool indexFiles = true, bool commit = false)
        {
            this.Index(content, indexFiles, null, commit);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="content"></param>
        /// <param name="indexFiles"></param>
        /// <param name="commit"></param>
        /// <param name="allowedFiles">Key is the property name, Value is the value of the property</param>
        public void Index(IContent content, bool indexFiles = true, Dictionary<string, string> allowedFiles = null, bool commit = false)
        {
            if (!(content is PageData))
                return;

            if (content.IsDeleted)
            {
                Delete(content as PageData);
                return;
            }

            if (content.ImplementsInterface(typeof(ISearchableDocument<T>)))
            {
                try
                {
                    var document = (ISearchableDocument<T>)content;
                    var solrDocument = document.GetSolrDocument();

                    if (document.ExcludeFromIndex)
                    {
                        this.Delete(solrDocument);
                        return;
                    }

                    // Lets remove the empty strings.
                    solrDocument.Content = solrDocument.Content.Where(p => !string.IsNullOrEmpty(p.Trim())).ToList();

                    this.SolrSearchFactory.Add(solrDocument);
                    if (commit)
                        this.SolrSearchFactory.Commit();

                    if (indexFiles)
                    {
                        //var contentPage = this.contentRepository.Get<PageData>(content.ContentLink);
                        //foreach (UnifiedFile file in contentPage.GetPageFolderFiles(allowedFiles))
                        //    IndexFile(solrDocument, file, commit);
                    }
                }
                catch (Exception)
                {
                }
            }
        }

        public void Index(IEnumerable<IContent> contentItems, bool indexFiles = true, int commitIncrement = 10)
        {
            int counter = 1;
            foreach (IContent content in contentItems)
            {
                Index(content, indexFiles, null, counter % commitIncrement == 0);
                counter++;
            }
            // Finally commit that didn't make it in the increment commit
            this.SolrSearchFactory.Commit();
        }

        private void Delete(string solrId)
        {
            this.SolrSearchFactory.Delete(new SolrQueryByField("Id", solrId));
            this.SolrSearchFactory.Commit();
        }

        public void Delete(ISolrDocument content)
        {
            this.Delete(content.Id);
        }

        public void Delete(PageData content)
        {
            this.Delete(content.ContentSolrIdentifier());
        }

        public void IndexPageTree(IContent content)
        {
            log.Debug(String.Concat("Spawning IndexPageWorker for descendents of ", content.Name, "."));
            var thread = new Thread(IndexPageWorker);
            thread.Start(new List<ContentReference> {
                content.ContentLink
            }.Union(this.contentRepository.GetDescendents(content.ContentLink)).ToList());
        }

        private void IndexPageWorker(object pagesToIndex)
        {
            var contentReferences = pagesToIndex as List<ContentReference>;

            log.Debug(String.Concat("IndexPageWorker Starting. ", contentReferences.Count, " page(s) in queue."));
            var contents = this.contentRepository.GetItems(contentReferences, LanguageSelector.AutoDetect());
            this.Index(contents, true, 25);
            log.Debug("IndexPageWorker Stopping.");
        }

        #region Files

        public void IndexFile(T searchableDocument, UnifiedFile file, bool commit = true)
        {
            var solrFileDocument = searchableDocument;
            solrFileDocument.Id = GetFileGuid(file);
            solrFileDocument.ContentType = SolrContentType.File.ToString();
            solrFileDocument.Title = String.IsNullOrEmpty(Convert.ToString(file.Summary.Dictionary["Title"])) ? file.Name : Convert.ToString(file.Summary.Dictionary["Title"]);
            solrFileDocument.Content.Add(ExtractFileContents(file));
            this.SolrSearchFactory.Add(solrFileDocument);
            if (commit)
                this.SolrSearchFactory.Commit();
        }

        private string ExtractFileContents(UnifiedFile file)
        {
            var address = ConfigurationManager.ConnectionStrings[this.solrConnectionStringName].ConnectionString;
            string extractionAddress = address + "/update/extract?extractOnly=true";

            var client = new WebClient();

            byte[] response;
            try
            {
                response = client.UploadData(extractionAddress, File.ReadAllBytes(file.LocalPath));
            }
            catch (Exception)
            {
                return String.Empty;
            }

            return Encoding.UTF8.GetString(response).Replace("&lt;", "<").Replace("&gt;", ">");
        }

        private string GetFileGuid(UnifiedFile file)
        {
            var fileInfo = new FileInfo(file.LocalPath);
            var extension = fileInfo.Extension;
            return fileInfo.Name.Replace(extension, string.Empty);
        }

        #endregion Files

        public ISolrOperations<T> SolrSearchFactory
        {
            get
            {
                if (solrnetFactory == null)
                {
                    solrnetFactory = ServiceLocator.Current.GetInstance<ISolrOperations<T>>();
                };
                return solrnetFactory;
            }
            internal set
            {
                solrnetFactory = value;
            }
        }
    }
}