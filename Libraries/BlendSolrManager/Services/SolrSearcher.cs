﻿using BlendSolrManager.Enums;
using BlendSolrManager.Interfaces;
using BlendSolrManager.Models;
using EPiServer.ServiceLocation;
using log4net;
using SolrNet;
using SolrNet.Commands.Parameters;
using System;
using System.Linq;

namespace BlendSolrManager.Services
{
    public class SolrSearcher<T> : ISolrSearcher where T : ISolrDocument
    {
        private static ILog log = LogManager.GetLogger(typeof(SolrIndexer<T>));
        private ISolrOperations<T> solrnetFactory;

        public SolrSearcher()
            : this(ServiceLocator.Current.GetInstance<ISolrOperations<T>>())
        {
        }

        public SolrSearcher(ISolrOperations<T> solrFactory)
        {
            this.solrnetFactory = solrFactory;
        }

        public SolrSearchResult<T> Search(SolrSearchCriterion criterion)
        {
            var queryOptions = new QueryOptions();

            if (criterion.FilterForSolrContentType != SolrContentType.All)
                queryOptions.AddFilterQueries(new SolrQueryByField("ContentType", criterion.FilterForSolrContentType == SolrContentType.Page ? "Page" : "File"));

            // Add the incoming filters
            queryOptions.AddFilterQueries(new SolrQueryByRange<DateTime>("StartPublish", DateTime.MinValue, DateTime.Now, true, true));
            queryOptions.AddFilterQueries(new SolrQueryByRange<DateTime>("StopPublish", DateTime.Now, DateTime.MaxValue, true, true));

            queryOptions.AddFilterQueries(new SolrQueryInList("AncestorIds", criterion.SearchStartPage.ToString()));

            foreach (AbstractSolrQuery filterQuery in criterion.QueryParameters)
                queryOptions.FilterQueries.Add(filterQuery);

            // Calculate which results we want
            queryOptions.Start = (criterion.PageNumber - 1) * criterion.PageWeight;
            queryOptions.Rows = criterion.PageWeight;

            // Add the sorts
            foreach (var entry in criterion.Sort)
                queryOptions.AddOrder(new SortOrder(entry.Key, entry.Value == SolrSortDirection.Ascending ? Order.ASC : Order.DESC));

            return new SolrSearchResult<T>(this.SolrSearchFactory.Query(SolrQuery.All, queryOptions), criterion.PageNumber, criterion.PageWeight);
        }

        public ISolrOperations<T> SolrSearchFactory
        {
            get
            {
                if (solrnetFactory == null)
                {
                    solrnetFactory = ServiceLocator.Current.GetInstance<ISolrOperations<T>>();
                };
                return solrnetFactory;
            }
            internal set
            {
                solrnetFactory = value;
            }
        }
    }
}