﻿using BlendSolrManager.Enums;
using BlendSolrManager.Interfaces;
using BlendSolrManager.Models;
using EPiServer;
using EPiServer.Core;
using EPiServer.ServiceLocation;
using EPiServer.Web;
using EPiServer.Web.Hosting;
using EPiServer.Web.Routing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace BlendSolrManager.Extensions
{
    public static class IContentExtensions
    {
        public static bool ImplementsInterface(this IContent content, Type interfaceType)
        {
            var type = content.GetOriginalType();
            if (type != null)
            {
                return interfaceType.IsAssignableFrom(type);
            }

            return false;
        }

        public static string ContentSolrIdentifier(this PageData content)
        {
            return string.Format("{0}-{1}", content.ContentLink.ID, content.Language.ToString());
        }

        public static string ConvertToString(this IEnumerable<IndexableContentItem> indexableContentItems)
        {
            return string.Join(" ", indexableContentItems.Select(p => p.Content));
        }

        public static List<string> ConvertToListOfString(this IEnumerable<IndexableContentItem> indexableContentItems, bool cleanString = true)
        {
            //return indexableContentItems.Where(p => !string.IsNullOrEmpty(p.Content)).Select(p => CleanString(p.Content)).ToList();
            var list = new List<string>();
            foreach (IndexableContentItem item in indexableContentItems)
            {
                if (!string.IsNullOrEmpty(item.Content))
                {
                    if (cleanString)
                        list.Add(item.Content.CleanString());
                    else
                        list.Add(item.Content);
                }
            }
            return list;
        }

        public static T FillDefaultDocument<T>(this PageData content, T document) where T : ISolrDocument
        {
            var contentRepository = ServiceLocator.Current.GetInstance<IContentRepository>();
            var urlResolver = ServiceLocator.Current.GetInstance<UrlResolver>();
            var settings = SiteDefinition.Current;
            var ancestors = DataFactory.Instance.GetAncestors(content.ContentLink)
                .Where(p => !p.ContentLink.CompareToIgnoreWorkID(PageReference.RootPage))
                .Reverse()
                .Select(x => x.ContentLink.ID)
                .ToArray();

            document.Id = content.ContentSolrIdentifier(); // we use id but with different languages, we need to tack on language.
            document.Title = content.Name;
            document.ContentType = SolrContentType.Page.ToString();
            document.Created = content.Created;
            document.CreatedBy = content.CreatedBy;
            document.StartPublish = content.StartPublish;
            document.StopPublish = content.StopPublish;
            document.ParentId = content.ParentLink.ID;
            document.Categories = content.Category.Select(p => EPiServer.DataAbstraction.Category.Find(p).Name).ToList();
            document.ContentGuid = content.ContentGuid.ToString();
            document.VisibleInMenu = content.VisibleInMenu;
            document.SiteUrl = settings.SiteUrl.ToString();
            document.Language = content.Language.Name;
            document.ContentTypeName = content.PageTypeName;
            document.LinkUrl = content.LinkURL.ToString();
            document.SiteId = settings.Name;
            document.FullUrl = string.Concat(settings.SiteUrl.ToString(), content.LinkURL);
            document.ACL = content.ACL.ToRawACEArray().Select(ace => ace.Name).ToArray();
            document.AncestorIds = ancestors;
            document.FriendlyUrl = urlResolver.GetUrl(content.ContentLink);
            document.MappedContentIds = contentRepository.GetReferencesToContent(content.ContentLink, true).Select(p => p.OwnerID.ID).ToArray();

            return document;
        }

        public static IEnumerable<UnifiedFile> GetPageFolderFiles(this PageData currentPage, Dictionary<string, string> indexableProperties = null)
        {
            var list = new List<UnifiedFile>();
            VirtualPathUnifiedProvider provider = (VirtualPathUnifiedProvider)VirtualPathHandler.GetProvider("SitePageFiles");
            var id = (int)(currentPage["PageFolderID"] ?? 0);

            var pageFolderName = VirtualPathUtility.AppendTrailingSlash(id.ToString());

            var path = VirtualPathUtilityEx.Combine(provider.VirtualPathRoot, pageFolderName);

            if (provider.DirectoryExists(path))
            {
                UnifiedDirectory vdir = (UnifiedDirectory)provider.GetDirectory(path);
                list = vdir.GetFiles().ToList();
            }

            if (indexableProperties != null)
            {
                for (int i = list.Count - 1; i >= 0; i--)
                {
                    var file = list[i];
                    foreach (KeyValuePair<string, string> values in indexableProperties)
                    {
                        if (file.Summary.Dictionary[values.Key] == null)
                        {
                            list.RemoveAt(i);
                            continue;
                        }

                        if (file.Summary.Dictionary[values.Key].ToString() != values.Value)
                        {
                            list.RemoveAt(i);
                            continue;
                        }
                    }
                }
            }
            return list;
        }

        public static string CleanString(this string content)
        {
            string inputText = content;

            inputText = inputText.StripTags();
            inputText = inputText.Replace("&nbsp;", " ");
            inputText = inputText.CollapseWhitespace();
            return inputText.Trim();
        }

        public static string StripTags(this string inputstring)
        {
            if (!string.IsNullOrEmpty(inputstring))
            {
                return Regex.Replace(inputstring, "<.*?>", string.Empty);
            }
            return string.Empty;
        }

        public static string CollapseWhitespace(this string inputstring)
        {
            return Regex.Replace(inputstring, " {2-1000}", "");
        }
    }
}