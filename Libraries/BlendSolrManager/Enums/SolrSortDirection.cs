﻿namespace BlendSolrManager.Enums
{
    public enum SolrSortDirection
    {
        Ascending,
        Descending
    }
}