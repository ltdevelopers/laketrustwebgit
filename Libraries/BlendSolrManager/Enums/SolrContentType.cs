﻿namespace BlendSolrManager.Enums
{
    public enum SolrContentType
    {
        All,
        Page,
        File
    }
}