﻿using BlendSolrManager.Interfaces;
using SolrNet.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BlendSolrManager.Models
{
    public abstract class EPiServerBaseSolrDocument : ISolrDocument
    {
        public EPiServerBaseSolrDocument()
        {
            this.Content = new List<string>();
            this.Categories = new HashSet<string>();
            this.ACL = new HashSet<string>();
        }

        [SolrUniqueKey("Id")]
        public virtual string Id { get; set; }

        [SolrUniqueKey("ContentGuid")]
        public virtual string ContentGuid { get; set; }

        [SolrField("SiteId")]
        public virtual string SiteId { get; set; }

        [SolrField("ParentId")]
        public virtual int ParentId { get; set; }

        [SolrField("AncestorIds")]
        public virtual int[] AncestorIds { get; set; }

        [SolrField("MappedContentIds")]
        public virtual int[] MappedContentIds { get; set; }

        [SolrField("ContentType")]
        public virtual string ContentType { get; set; }

        [SolrField("ContentTypeName")]
        public virtual string ContentTypeName { get; set; }

        [SolrField("CreatedBy")]
        public virtual string CreatedBy { get; set; }

        [SolrField("Created")]
        public virtual DateTime Created { get; set; }

        [SolrField("StartPublish")]
        public virtual DateTime StartPublish { get; set; }

        [SolrField("StopPublish")]
        public virtual DateTime StopPublish { get; set; }

        [SolrField("Title")]
        public virtual string Title { get; set; }

        [SolrField("LinkUrl")]
        public virtual string LinkUrl { get; set; }

        [SolrField("Language")]
        public virtual string Language { get; set; }

        [SolrField("ACL")]
        public virtual ICollection<string> ACL { get; set; }

        [SolrField("VisibleInMenu")]
        public virtual bool VisibleInMenu { get; set; }

        [SolrField("Categories")]
        public virtual ICollection<string> Categories { get; set; }

        [SolrField("FriendlyUrl")]
        public virtual string FriendlyUrl { get; set; }

        [SolrField("SiteUrl")]
        public virtual string SiteUrl { get; set; }

        [SolrField("FullUrl")]
        public virtual string FullUrl { get; set; }

        [SolrField("Content")]
        public virtual List<string> Content { get; set; }
    }
}