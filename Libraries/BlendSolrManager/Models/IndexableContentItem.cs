﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BlendSolrManager.Models
{
    public class IndexableContentItem
    {
        public enum IndexableContentItemType
        {
            Page,
            Block
        }

        public IndexableContentItem()
        {
            this.ContentItemType = IndexableContentItemType.Page;
        }

        public string ContentName { get; set; }

        public IndexableContentItemType ContentItemType { get; set; }

        public string PropertyName { get; set; }

        public string Content { get; set; }
    }
}