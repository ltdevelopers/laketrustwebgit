﻿using BlendSolrManager.Enums;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.ServiceLocation;
using SolrNet;
using System;
using System.Collections.Generic;

namespace BlendSolrManager
{
    public class SolrSearchCriterion
    {
        private readonly IContentTypeRepository contentTypeRepository;

        public SolrSearchCriterion(int pageNumber = 1, int pageWeight = 20)
        {
            this.contentTypeRepository = ServiceLocator.Current.GetInstance<IContentTypeRepository>();
            this.PageNumber = pageNumber;
            this.PageWeight = pageWeight;
            this.Sort = new Dictionary<string, SolrSortDirection>();
            this.SearchStartPage = ContentReference.StartPage.ID;
            this.FilterForSolrContentType = SolrContentType.Page;
            this.FilterForDate = DateTime.Now;
            this.QueryParameters = new HashSet<ISolrQuery>();
        }

        public void SearchBelowPage(int contentId)
        {
            this.SearchStartPage = contentId;
        }

        public void SearchBelowPage(ContentReference contentReference)
        {
            this.SearchBelowPage(contentReference.ID);
        }

        public void AddParameter(ISolrQuery query)
        {
            this.QueryParameters.Add(query);
        }

        public void AddSearchParameter(string fieldName, string q)
        {
            this.QueryParameters.Add(new SolrQueryByField(fieldName, q));
        }

        public void AddSearchParameter(string fieldName, string q, bool quoted)
        {
            if (quoted)
                this.QueryParameters.Add(new SolrQueryByField(fieldName, QuoteString(q)));
            else
                this.QueryParameters.Add(new SolrQueryByField(fieldName, q));
        }

        public void AddSearchParameter(string fieldName, string q, double boost)
        {
            this.QueryParameters.Add(new SolrQueryByField(fieldName, q).Boost(boost));
        }

        public void AddSearchParameter(string fieldName, string q, double boost, bool quoted)
        {
            if (quoted)
                this.QueryParameters.Add(new SolrQueryByField(fieldName, QuoteString(q)).Boost(boost));
            else
                this.QueryParameters.Add(new SolrQueryByField(fieldName, q).Boost(boost));
        }

        public void ContentType(SolrContentType contentType)
        {
            this.FilterForSolrContentType = contentType;
        }

        public void ContentTypeName(Type type)
        {
            this.ContentTypeName(contentTypeRepository.Load(type).Name);
        }

        public void ContentTypeName(string contentTypeName)
        {
            this.QueryParameters.Add(new SolrQueryByField("ContentTypeName", contentTypeName));
        }

        public void AddSort(string fieldName, SolrSortDirection direction)
        {
            this.Sort.Add(fieldName, direction);
        }

        public string QuoteString(string value)
        {
            return string.Format("\"{0}\"", value);
        }

        #region Properties

        public int PageNumber { get; set; }

        public int PageWeight { get; set; }

        public int SearchStartPage { get; private set; }

        public SolrContentType FilterForSolrContentType { get; private set; }

        public DateTime FilterForDate { get; private set; }

        public ICollection<ISolrQuery> QueryParameters { get; private set; }

        public Dictionary<string, SolrSortDirection> Sort { get; private set; }

        #endregion Properties
    }
}