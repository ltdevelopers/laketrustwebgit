﻿using BlendSolrManager.Interfaces;
using SolrNet;

namespace BlendSolrManager.Models
{
    /// <summary>
    /// Return object that SolrSearcher returns.
    /// </summary>
    /// <typeparam name="T">The default solr document type.</typeparam>
    public class SolrSearchResult<T> where T : ISolrDocument
    {
        private int PageNumber { get; set; }

        private int PageWeight { get; set; }

        public SolrSearchResult(SolrQueryResults<T> results, int pageNumber, int pageWeight)
        {
            this.Results = results;
            this.PageNumber = pageNumber;
            this.PageWeight = pageWeight;
        }

        public SolrQueryResults<T> Results { get; set; }

        /// <summary>
        /// Returns the total number of records in the returned results SolrQueryResults
        /// </summary>
        public int TotalResults
        {
            get
            {
                return Results.NumFound;
            }
        }

        /// <summary>
        /// The total number of pages based on pageweight
        /// </summary>
        public int PageCount
        {
            get
            {
                if (this.TotalResults == 0)
                    return 1;

                return ((this.TotalResults - 1) / this.PageWeight) + 1;
            }
        }
    }
}